using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using BusinessObject;
using PickAccessLayer.DataAccessObjects;
using PickData;
using BizProject = BusinessObject.Project;
using PickDataProject = PickData.Project;

namespace PickAccessLayer
{
    /// <summary>
    /// TODO: EliminateIdenticalIds copy
    /// </summary>
    [Serializable]
    public class IBatisPickCopier
    {
        private BindingListEx<PickIdDAO> _pickIdList;
        private IProject _pickProject;
        private ProjectHeader _projectHeader;
        private IDLoadCollection _idLoadCollection;
        private BizProject _project;
        private ProductCollection _productCollection;

        public static void Copy(string sourceProjectName, string sourceConnection,
            string destinationProjectName, string destinationConnection)
        {
            IBatisPickCopier copier = new IBatisPickCopier();

            copier.ReadAllPickInfoPlusFirmware(sourceConnection, sourceProjectName);
            ProjectHeader projectHeader = copier.MakeProjectHeader(destinationConnection, destinationProjectName);
            copier.WriteIdLoad(destinationConnection, projectHeader);
            copier.WriteAllPickInfoPlusFirmware(destinationConnection, destinationProjectName);
        }

        public static void CopyPartialWithStoredProcedure(
            string sourceProjectName, string sourceDbOption,
            string destinationProjectName, string DestDbOption)
        {
            ProductDBFunctions.CopyPick2Partially(sourceProjectName, sourceDbOption,
                destinationProjectName, DestDbOption);
        }

        public IBatisPickCopier()
        {
        }

        public void ReadAllPickInfoPlusFirmware(string connStr, string projectName)
        {
            ReadAllPickInfo(connStr, projectName);
            _project = DAOFactory.Project().LoadPick2Related(projectName);
            _productCollection = ProductDBFunctions.GetProjectProducts(_projectHeader.Code);
        }

        public void WriteAllPickInfoPlusFirmware(
            string connStr, string projectName)
        {
            ProjectHeader projectHeader = MakeProjectHeader(connStr, projectName);

            _project.Header.Code = projectHeader.Code;
            _project.Header.Name = projectName;

            try
            {
                DAOFactory.BeginTransaction();
                DAOFactory.Project().AddPick2Related(_project);
                DAOFactory.Project().Save(_productCollection, _project.Header.Code);
                DAOFactory.CommitTransaction();
            }
            catch (Exception)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }
            WriteAllPickInfo(connStr, projectName);
        }

        public void ReadAllPickInfo(string connStr, string projectName)
        {
            DAOFactory.ResetDBConnection(connStr);

            _projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

            if (_projectHeader == null)
                throw new DBPickNotFoundException("Could not find in database " + projectName);

            _pickProject = PickRetrieveFunctions.GetPickConfigAsProject(projectName);

            if (_pickProject == null)
                throw new DBPickNotFoundException("Could not find pick project data in database " + projectName);
            try
            {
                _idLoadCollection = ProductDBFunctions.GetIDLoadList(projectName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            IList<IDVersionPair> idVersionList = PickRetrieveFunctions.GetPickIdNames(projectName);

            _pickIdList = new BindingListEx<PickIdDAO>();
            foreach (IDVersionPair idVersion in idVersionList)
            {
                PickIdDAO pickId = PickIdDAO.GetFromDB(_projectHeader.Code, idVersion.Id, idVersion.Version);

                _pickIdList.Add(pickId);
            }
        }

        private void WriteIdLoad(string connStr, ProjectHeader projectHeader)
        {
            DAOFactory.ResetDBConnection(connStr);

            try
            {
                DAOFactory.BeginTransaction();

                foreach (IDLoad idLoad in _idLoadCollection)
                {
                    idLoad.Project_Code = projectHeader.Code;
                }
                ProductDBFunctions.InsertIDLoadList(_idLoadCollection);

                DAOFactory.CommitTransaction();
            }
            catch (Exception)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }
        }

        public int WriteAllPickInfo(string connStr, string projectName)
        {
            ProjectHeader projectHeader = MakeProjectHeader(connStr, projectName);

            _pickProject.ProjectName = projectName;

            PickDBFunctions.AddOrUpdatePickProjectInfo(projectName, _pickProject);

            try
            {
                DAOFactory.BeginTransaction();

                foreach (PickIdDAO pickId in _pickIdList)
                {
                    pickId.FK_Project_Code = projectHeader.Code;
                    pickId.WriteDB();
                }
                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                throw ex;
            }

            return projectHeader.Code;
        }

        private ProjectHeader MakeProjectHeader(string connStr, string projectName)
        {
            DAOFactory.ResetDBConnection(connStr);

            PickDBFunctions.AddOrUpdateProjectInfo(projectName, _projectHeader.MemoryType,
                                                   _projectHeader.SectorSize, _projectHeader.Manufacturer_Code,
                                                   _projectHeader.Description);
            ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

            if (projectHeader == null)
                throw new DBPickNotFoundException("Could not find in database " + projectName);
            return projectHeader;
        }

        public void ToXml(Stream s)
        {
            XmlSerializer x = new XmlSerializer(GetType());
            x.Serialize(s, this);
            using (MemoryStream ms = (_pickProject as PickData.Project).ToXml())
            {
                ms.Position = 0;
                for (int count = 0; count < ms.Length; count++)
                {
                    s.WriteByte(Convert.ToByte(ms.ReadByte()));
                }
            }
            s.Close();
        }

        public BindingListEx<PickIdDAO> PickIdList
        {
            get { return _pickIdList; }
        }
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { _projectHeader = value; }
        }
        public IDLoadCollection IdLoadCollection
        {
            get { return _idLoadCollection; }
        }

        public BizProject BusinessObjectProject
        {
            get { return _project; }
            set { _project = value; }
        }
        public ProductCollection ProductCollection
        {
            get { return _productCollection; }
        }
    }
}
