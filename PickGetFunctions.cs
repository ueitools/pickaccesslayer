using System;
using System.Collections;
using System.Collections.Generic;
using BusinessObject;
using PickData;
using Device = BusinessObject.Device;

namespace PickAccessLayer
{
    public static class PickRetrieveFunctions
    {
        public static bool ProjectExists(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                return ProductDBFunctions.GetProjectHeader(projectName) != null;
            }
        }

        public static string GetPickConfig(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                if (projectHeader == null)
                    throw new DBPickNotFoundException("Project name not found.");

                return Pick2DAOFactory.Pick2DAO().GetPickConfig(projectHeader.Code);
            }
        }

        public static PickData.IProject GetPickConfigAsProject(string projectName)
        {
            string xml = GetPickConfig(projectName);

            if (String.IsNullOrEmpty(xml))
                throw new DBPickNotFoundException("Pick project data for " + projectName +
                    " not found. Perhaps a pick needs to be configured project.");

            return PALUtil.FromProjectXml(xml);
        }

        public static PickData.IProject GetPickConfigAsProject(string projectName, out bool isValid)
        {
            string xml = GetPickConfig(projectName);

            PickData.IProject result = null;
            isValid = !String.IsNullOrEmpty(xml);

            if (isValid)
            {
                result = PALUtil.FromProjectXml(xml);
            }
            return result;
        }	
	
        public static IList<IDVersionPair> GetPickIdNames(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                if (projectHeader == null)
                    throw new DBPickNotFoundException("Pick data for " + projectName + 
                        " not found. Perhaps a pick needs to be run for that project.");

                int projectCode = projectHeader.Code;
                Pick2DAO dao = new Pick2DAO();
                IList<Hashtable> idList = dao.GetPickIdNames(projectCode);
                List<IDVersionPair> result = new List<IDVersionPair>();

                foreach (Hashtable table in idList)
                {
                    result.Add(new IDVersionPair((string)table["FK_ID"], (int)table["Version"], (DateTime)table["CreationDate"]));
                }
                return result;
            }
        }

        public static IList<PickPrefix> GetPickPrefixWithVersion(string projectName, string id, int version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                IList<PickPrefix> prefixList = dao.GetPickPrefixWithVersion(projectName, id, version);

                return prefixList;
            }
        }

        public static IList<Hashtable> GetEmptyPickedIds(string projectname, ArrayList idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                IList<Hashtable> idList = dao.GetEmptyPickedIds(projectname, idlist);

                return idList;
            }
        }

        public static IList<Hashtable> GetAllVersionsOfId(string projectname, string id)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                IList<Hashtable> idList = dao.GetAllVersionsOfId(projectname, id);

                return idList;
            }
        }


        /// <summary>
        /// partners with setpickdatamap
        /// </summary>
        /// <param name="project"></param>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static IList<PickResultKey> GetEditPickDataMapWithVersion(IProject project, string id, int version)
        {
            using (new ConnectionSwitcher())
            {
                IList<PickResultKey> resultList = new List<PickResultKey>();
                Pick2DAO dao = new Pick2DAO();
                IList<Hashtable> functionList = dao.GetPickDataMapWithVersion(project.ProjectName, id, version);

                foreach (Hashtable dataMap in functionList)
                {
                    PickResultKey resultKey = SetEditedPickDataMap(
                        (string)dataMap["Intron"],
                        (string)dataMap["IntronPriority"],
                        (string)dataMap["Comment"],
                        (string)dataMap["IDLabel"],
                        (string)dataMap["FirmwareLabel"],
                        (int)dataMap["Number"],
                        (string)dataMap["Data"]);
                    UpdateDeviceKey(project, id, resultKey);
                    resultList.Add(resultKey);
                }
                return resultList;
            }
        }

        private static void UpdateDeviceKey(IProject project, string id, PickResultKey resultKey)
        {
            PickData.Device dev = project.FindDevice(id[0]);
            DeviceKey key = resultKey.DeviceKey;
            if (dev != null)
                key = dev.FindDeviceKey(resultKey.FirmwareLabel);
            if (key != null)
                resultKey.DeviceKey = key;
        }

        /// <summary>
        /// partners with getpickdatamapwithversion
        /// </summary>
        /// <returns></returns>
        public static PickResultKey SetEditedPickDataMap(string intron, string intronPriority,
            string comment, string label, string firmwareLabel, int keyNumber, string data)
        {
            PickFunction pickFunction = new PickFunction();

            pickFunction.Label = label;
            pickFunction.Comment = comment;
            pickFunction.Intron = intron;
            pickFunction.IntronPriority = intronPriority;
            pickFunction.Data = data;

            DeviceKey deviceKey = new DeviceKey();
            deviceKey.FirmwareLabel = firmwareLabel;
            deviceKey.KeyIndex = keyNumber;

            PickResultKey resultKey = new PickResultKey(deviceKey);
            resultKey.SetPickFunction((pickFunction));

            return resultKey;
        }

        public static PickIdHeader GetPickIdHeader(string projectName, string id, int version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                PickIdHeader pickIdHeader = dao.GetPickIdHeader(projectName, id, version);

                ArrayList idList = new ArrayList();
                idList.Add(id);

                IList<Hashtable> loadTable = dao.GetPickIdWithAlias(projectName, idList);

                if (loadTable != null && loadTable.Count > 0)
                {
                    pickIdHeader.ParentID = (string)loadTable[0]["FromID"];
                }
                return pickIdHeader;
            }
        }

        public static ProjectHeaderCollection GetProjectHeaderCollection()
        {
            return ProductDBFunctions.GetAllProjects();
        }

        public static List<IdListCollection> GetProjectIdAliasList(string projectName, IList<string> idList)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                List<IdListCollection> idAliasList = new List<IdListCollection>();
                ArrayList arrayList = new ArrayList();

                for (int i = 0; i < idList.Count; i++)
                    arrayList.Add(idList[i]);

                foreach (Hashtable table in dao.GetPickIdWithAlias(projectName, arrayList))
                {
                    IdListCollection idListCollection = new IdListCollection();

                    idListCollection.FromId = (string)table["FromID"];
                    idListCollection.Id = (string)table["ID"];

                    idAliasList.Add(idListCollection);
                }

                return idAliasList;
            }
        }

        public static IList<Hashtable> GetExecIdMap(string ProjectName, ArrayList idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetExecIdMap(ProjectName, idlist);
            }
        }

        public static IList<Hashtable> GetFunctionSearchCount(string projectname, ArrayList idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetFunctionSearchCount(projectname, idlist);
            }
        }

        public static IList<Hashtable> SearchFunction(string projectname, ArrayList idlist, ArrayList functionlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.SearchFunction(projectname, idlist, functionlist);
            }
        }

        public static IList<Hashtable> SearchUnPickedfunctions(string projectname, ArrayList idlist, ArrayList functionlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.SearchUnPickedfunctions(projectname, idlist, functionlist);
            }
        }

        public static IList<Hashtable> GetPickProjectNames()
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetPickProjectNames();
            }
        }

        public static IList<Hashtable> GetPickIdList(string projectname)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetPickIdList(projectname);
            }
        }

        public static IList<Hashtable> GetPickIdWithAlias(string projectname, ArrayList IDList)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetPickIdWithAlias(projectname, IDList);
            }
        }

        public static IList<Hashtable> GetAllPickedKeyInfo(string projectname, ArrayList idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetAllPickedKeyInfo(projectname, idlist);
            }
        }

        public static IList<Hashtable> GetPickedKeyInfoWithoutPrefix(string projectname, ArrayList idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetPickedKeyInfoWithoutPrefix(projectname, idlist);
            }
        }

        public static IList<Hashtable> GetPickedKeyInfoWithoutPrefixV1(string projectname, ArrayList idlist,string version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                return dao.GetPickedKeyInfoWithoutPrefixV1(projectname, idlist,version);
            }
        }

        public static IList<OptomizedKeyInfo> GetOptimizedKeyInfoList(string projectName, string modes)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);
                return dao.GetOptimizedKeyInfoList(projectHeader.Code, modes);
            }
        }
		  
        public static int CountProjectLocks(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                PickProjectDAO pickProjectDao = new PickProjectDAO();
                ProjectHeader projectHeader = pickProjectDao.SelectHeader(projectName);

                if (projectHeader == null)
                    throw new DBPickNotFoundException();

                return pickProjectDao.CountProjectLocks(projectHeader.Code);
            }
        }

        public static IList<ProjectLockUserInfo> GetWhoHasLocks(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                PickProjectDAO pickProjectDao = new PickProjectDAO();
                ProjectHeader projectHeader = pickProjectDao.SelectHeader(projectName);

                if (projectHeader == null)
                    throw new DBPickNotFoundException();

                return pickProjectDao.GetWhoHasLocks(projectHeader.Code);
            }            
        }

        public static IList<Hashtable> GetPickIdInfo(string ProjectName, string Id, int Version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetPickIdInfo(ProjectName,Id,Version);
                
            }
        }

        public static IList<Hashtable> GetPickItems_Test(string projectName, string id, int version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetPickItems_Test(projectName, id, version);

            }
        }

        public static IList<Hashtable> GetPickPrefixItems_Test(string ProjectName, string Id, int Version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetPickPrefixItems_Test(ProjectName, Id, Version);

            }
        }

        public static int GetMaxVersion(string ProjectName, string Id)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetMaxVersion(ProjectName, Id);

            }
        }

        public static string GetInversePrefixFlag(string ProjectName, string Id, int Version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetInversePrefixFlag(ProjectName, Id, Version);

            }
        }

        public static IList<Hashtable> GetAllIdsWithDigitGroup(string projectname, string Firmwarelabel, int labelcount, int datalength, string idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetAllIdsWithDigitGroup(projectname,Firmwarelabel,labelcount,datalength,idlist);

            }
        }

        public static IList<Hashtable> GetIdsWithAllDigitKeysWithData(string firmwareLabel, string keyData, string projectName, int keyCount,char isInversedData,string idlist)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetIdsWithAllDigitKeysWithData(firmwareLabel, keyData, projectName, keyCount, isInversedData,idlist);
            }            
        }

        public static PickId GetIdSnapshot(string projectName, string id, int version)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                string xml = dao.GetIdSnapshot(projectName, id, version);

                if (String.IsNullOrEmpty(xml))
                    return null;

                return PALUtil.FromPickIdXml(xml);
            }
        }

        public static IList<Hashtable> GetIdenticalIdReport(string projectName, string deviceTypes, string modes)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.GetIdenticalIdReport(projectName, deviceTypes, modes);
    		}
        }
        public static IList<Hashtable> StoreAndEliminateIdenticalIds(string ProjectName, string RetainedIDs, string EliminatedIds, string aliasids, string LineNumbers, int saveStatus)
        {
            using(new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.StoreAndEliminateIdenticalIds(ProjectName, RetainedIDs, EliminatedIds, aliasids, LineNumbers, saveStatus);
            }
        }

        //public static IList<Hashtable> StoreAndEliminateIdenticalIds ( string ProjectName, string RetainedIDs, string EliminatedIds,string LineNumbers, int saveStatus )
        //{
        //    using ( new ConnectionSwitcher ( ) )
        //    {
        //        Pick2DAO dao = new Pick2DAO ( );
        //        return dao.StoreAndEliminateIdenticalIds ( ProjectName, RetainedIDs, EliminatedIds, LineNumbers, saveStatus );
        //    }
        //}

		public static IList<Hashtable> DeletePickIds(string ProjectName, ArrayList idModeList)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.DeletePickIds(ProjectName, idModeList);
            }
        }

        public static int DeletePickIds(string ProjectName, ArrayList idModeList, int systemflag)
        {
            using(new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.DeletePickIds(ProjectName, idModeList, systemflag);
            }
        }

        public static IList<Hashtable> ViewEliminatedIdList(string projectname)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.ViewEliminatedIdList(projectname);
            }
        }

        public static void DeleteIdenticalIdReport ( string projectname )
        {
            using ( new ConnectionSwitcher ( ) )
            {
                Pick2DAO dao = new Pick2DAO ( );
                dao.DeleteIdenticalIdReport( projectname );
            }
        }

        public static string IsDataInverted(string id, string projectname)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                return dao.IsDataInversed(id, projectname);
            }
        }

 		public static void DeleteDatabaseLoadIDs(string projectName, string id)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                dao.DeleteDatabaseLoadIDs(projectName, id);
            }
        }

        public static IList<Hashtable> GetAllProjectLocks ()
        {
            using ( new ConnectionSwitcher ( ) )
            {
                Pick2DAO dao = new Pick2DAO ( );
                return dao.GetAllProjectLocks ( );
            }
        }

        public static void DeleteProjectLock ( string projectname )
        {
            using ( new ConnectionSwitcher ( ) )
            {
                Pick2DAO dao = new Pick2DAO ( );
                dao.DeleteProjectLock ( projectname );
            }
        }

        public static IList<Hashtable> GetAllProjectsInfo ( )
        {
            using ( new ConnectionSwitcher ( ) )
            {
                Pick2DAO dao = new Pick2DAO ( );
                return dao.GetAllProjectsInfo (  );
            }
        }

        public static IList<Hashtable> GetIdLoad ( string projectname )
        {
            using ( new ConnectionSwitcher ( ) )
            {
                Pick2DAO dao = new Pick2DAO ( );
                return dao.GetIdLoad ( projectname );
            }
        }
    }
}