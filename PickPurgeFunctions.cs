using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;


namespace PickAccessLayer
{
    static public class PickPurgeFunctions
    {
        public static IList<Hashtable> DeletePickIdAfterN(string projectName, string id, int n)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeletePickIdAfterN(projectCode, id, n);
            }
        }

        public static IList<Hashtable> DeleteAfterDate(string projectName, string id, DateTime dateTime)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeleteAfterDate(projectCode, id, dateTime);
            }
        }

        public static IList<Hashtable> DeleteBeforeDate(string projectName, string id, DateTime dateTime)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeleteBeforeDate(projectCode, id, dateTime);
            }
        }

        public static IList<Hashtable> DeleteBeforeN(string projectName, string id, int n)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeleteBeforeN(projectCode, id, n);
            }
        }

        public static IList<Hashtable> DeleteExceptFirstAndLast(string projectName, string id)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeleteExceptFirstAndLast(projectCode, id);
            }
        }

        public static IList<Hashtable> DeleteExceptFirstN(string projectName, string id, int n)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeleteExceptFirstN(projectCode, id, n);
            }
        }

        public static IList<Hashtable> DeleteExceptLastN(string projectName, string id, int n)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                int projectCode = projectHeader.Code;

                return PickPurgeFunctions.DeleteExceptLastN(projectCode, id, n);
            }
        }

        public static IList<Hashtable> DeletePickIdAfterN(int projectCode, string id, int n)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeletePickIdAfterN(projectCode, id, n);
        }

        public static IList<Hashtable> DeleteAfterDate(int projectCode, string id, DateTime dateTime)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeleteAfterDate(projectCode, id, dateTime);
        }

        public static IList<Hashtable> DeleteBeforeDate(int projectCode, string id, DateTime dateTime)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeleteBeforeDate(projectCode, id, dateTime);
        }

        public static IList<Hashtable> DeleteBeforeN(int projectCode, string id, int n)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeleteBeforeN(projectCode, id, n);
        }

        public static IList<Hashtable> DeleteExceptFirstAndLast(int projectCode, string id)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeleteExceptFirstAndLast(projectCode, id);
        }

        public static IList<Hashtable> DeleteExceptFirstN(int projectCode, string id, int n)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeleteExceptFirstN(projectCode, id, n);
        }


        public static IList<Hashtable> DeleteExceptLastN(int projectCode, string id, int n)
        {
            Pick2DAO dao = new Pick2DAO();

            return dao.DeleteExceptLastN(projectCode, id, n);
        }

        public static IList<Hashtable> DeleteAllLatestVersionIds(string projectName, string id, int version)
        {

            Pick2DAO dao = new Pick2DAO();
            PickProjectDAO pickProjectDao = new PickProjectDAO();
            ProjectHeader projectHeader = pickProjectDao.SelectHeader(projectName);
            return dao.DeletePickIdVersion(projectHeader.Code, id, version);
                        
        }

        public static void ClearRegularProjectLock(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                PickProjectDAO pickProjectDao = new PickProjectDAO();
                ProjectHeader projectHeader = pickProjectDao.SelectHeader(projectName);
                
                if (projectHeader == null)
                    throw new DBPickNotFoundException();

                DAOFactory.BeginTransaction();
                pickProjectDao.ClearRegularProjectLock(projectHeader.Code);
                DAOFactory.CommitTransaction();
            }
        }
		 /// <summary>
        /// Delete ALL original ID snapshot versions for a given PickID
        /// </summary>
        /// <param name="projectName">Project Name</param>
        /// <param name="id">Pick ID Name</param>
        /// <returns></returns>
        public static bool DeleteOriginalIDSnapshots(string projectName, string id)
        {
            //Due to some unknown reason this function was being pointed to UEI2 DB
            //when deleting ids from pick edit view
            //need to investigate, untill then this connectionswitcher will do the needful for now
            using ( new ConnectionSwitcher ( ) )
            {
                DAOFactory.ResetDBConnection ( DBConnectionString.WORKPRODUCT );
                Pick2DAO dao = new Pick2DAO ( );
                return dao.DeleteOriginalIDSnapshots ( projectName, id ).Count > 0;
            }
            
        }

        /// <summary>
        /// after removal of pick id's, remove snapshots no longer used by pick id's.
        /// </summary>
        /// <returns></returns>
        public static void DeleteUnusedOriginalIDSnapshots()
        {
            Pick2DAO dao = new Pick2DAO(); 
            dao.DeleteSomeUnusedOriginalIDSnapshots();
        }

        public static bool DeletePickIdVersion(string projectName, string id, int version)
        {
            using (new ConnectionSwitcher())
            {
                PickProjectDAO pickProjectDao = new PickProjectDAO();
                ProjectHeader projectHeader = pickProjectDao.SelectHeader(projectName);
                
                if (projectHeader == null)
                    throw new DBPickNotFoundException();

                Pick2DAO dao = new Pick2DAO();

                return dao.DeletePickIdVersion(projectHeader.Code, id, version).Count > 0;
            }
        }

        

        public static int CheckIdEliminationReport (string projectname, List<string> _idlist )
        {
            try
            {
                //_goDelete=1, Then Delete Identical Id Report
                //_goDelete=0, Then Do not delete the report
                //_goDelete=2, Then No Identical Id Report
                
                int _goDelete = 2;
                IList<Hashtable> _identicalIdReport = PickAccessLayer.PickRetrieveFunctions.ViewEliminatedIdList(projectname);
                if ( _identicalIdReport.Count > 0 )
                {
                    foreach ( Hashtable ht in _identicalIdReport )
                    {
                        if ( _idlist.Contains ( ht ["FK_ID"].ToString ( ) ) || _idlist.Contains ( ht ["ReplacementID"].ToString ( ) ) )
                        {
                            if ( MessageBox.Show ( "Identical Id Report contains Ids marked for deletion, If you delete this Id then you have to re-generate Identical Id report since this report would be permanently deleted now. Would you like to Continue?", "Alert!", MessageBoxButtons.YesNo ) == DialogResult.Yes )
                            {
                                _goDelete = 1;
                                return _goDelete;
                            }
                            else
                            {
                                _goDelete = 0;
                                return _goDelete;
                            }


                        }
                    }
                }
                else
                {
                    _goDelete = 2;
                }
                return _goDelete;
            }
            catch ( Exception ex )
            {
                MessageBox.Show ( ex.Message );
                return 0;
            }
        }

        
    }
    
}
