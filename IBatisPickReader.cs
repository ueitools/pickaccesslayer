using System;
using System.Collections.Generic;
using System.Text;
using PickData;

namespace PickAccessLayer
{
    class IBatisPickReader
    {
        public IProject ReadPickConfig(string projectName)
        {
            return PickRetrieveFunctions.GetPickConfigAsProject(projectName);
        }
        public void Read(string projectName, PickId.List pickIdList, PickResultId.List pickResultId)
        {
            
        }
    }
}
