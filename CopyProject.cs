﻿using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using PickData;
using Project = PickData.Project;

namespace PickAccessLayer
{
    public class CopyProject
    {
        public bool CreateProjectCopy(string OldProject, string newProject)
        {
            try
            {

                CopyProjectDAO daoCopy = new CopyProjectDAO();
                ProjectHeader oldProjectObj = ProductDBFunctions.GetProjectHeader(OldProject);
                ProjectHeader newProjectObj = ProductDBFunctions.GetProjectHeader(newProject);
                int oldProjectCode = oldProjectObj.Code;
                int newProjectCode = newProjectObj.Code;

                // Insert in to IDLoad Table
                daoCopy.InsertIDLoad_Copy(oldProjectCode, newProjectCode);

                // Insert in to OriginalIDSnapshot Table
                daoCopy.InsertOriginalIDSnapshot_Copy(oldProjectCode, newProjectCode);

                // Insert in to PickID2 Table
                daoCopy.InsertPickID2_Copy(oldProjectCode, newProjectCode);


                daoCopy.InsertPickPrefix2(oldProjectCode, newProjectCode);

                daoCopy.InsertPickDataMap2(oldProjectCode, newProjectCode);

                daoCopy.InsertOptionalTables(oldProjectCode, newProjectCode);
                return true;
            }
            
            catch(Exception)
            {
                return false;
            }

        }
    }
}
