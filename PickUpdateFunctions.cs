using System;
using System.Collections.Generic;
using System.Text;
using PickData;
using BusinessObject;
using Key = BusinessObject.Key;

namespace PickAccessLayer
{
    public static class PickUpdateFunctions
    {
        public static void UpdateWithoutSnapshotChange(string projectName, PickResultId pickResultId)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();
                int snapshotKey = dao.GetIdLastSnapshotKey(projectName, pickResultId.Id);
                PickAddFunctions.AddPickId(projectName, pickResultId, snapshotKey);
                PickResultId.List loadList = new PickResultId.List();
                loadList.Add(pickResultId);
                UpdatePickProjectLoad(projectName, loadList);
            }
        }

        internal static int UpdatePickProjectInfo(string projectName,
            string memoryType, int sectorSize, string manufacturerCode,
            string description)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                if (projectHeader == null)
                    throw new ArgumentException("Project does not exist. Perhaps project name is incorrect");

                projectHeader.Name = projectName;
                projectHeader.Description = description;
                projectHeader.Manufacturer_Code = manufacturerCode;
                projectHeader.MemoryType = memoryType;
                projectHeader.SectorSize = sectorSize;

                try
                {
                    DAOFactory.BeginTransaction();
                    DAOFactory.Project().Update(projectHeader);
                    DAOFactory.CommitTransaction();

                    if (ProductDBFunctions.GetProjectHeader(projectName).Code != projectHeader.Code)
                        throw new ArgumentException("Inconsistent write. Please try again.");
                }
                catch
                {
                    DAOFactory.RollBackTransaction();
                    throw;
                }
                return projectHeader.Code;
            }
        }

        internal static void UpdatePickProjectLoad(string projectName, PickResultId.List idList)
        {
            if (!PickRetrieveFunctions.ProjectExists(projectName))
                throw new ArgumentException("Project does not exist yet. Please add project first.");
            if (!AreAllInLoad(projectName, idList))
                throw new ArgumentException("Id list contains id not in database yet. Perhaps should use addorupdate.");

            using (new ConnectionSwitcher())
            {
                try
                {
                    IDLoadCollection idLoadCollection = PickAddFunctions.CreateLoadCollection(projectName, idList);
                    DAOFactory.BeginTransaction();
                    foreach (IDLoad idLoad in idLoadCollection)
                    {
                        ProductDBFunctions.UpdateIDLoad(idLoad);
                    }
                    DAOFactory.CommitTransaction();
                }
                catch
                {
                    DAOFactory.RollBackTransaction();
                    throw;
                }
            }
        }

        public static void UpdateFromId(string projectName, string id, string fromId)
        {
            using (new ConnectionSwitcher())
            {
                Pick2DAO dao = new Pick2DAO();

                dao.UpdateFromId(projectName, id, fromId);
            }
        }

        private static bool AreAllInLoad(string projectName, PickResultId.List idList)
        {
            Dictionary<string, bool> loadLookup = PickDBFunctions.CreateLoadLookup(projectName);

            bool allInLoad = true;

            idList.ForEach(delegate(PickResultId id) { if (!loadLookup.ContainsKey(id.Id)) allInLoad = false; });

            return allInLoad;
        }

        public static void RenameOrInsertIdLoad(string projectName, string oldIdName, string newIdName, string fromId, string doUpdate)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                if (projectHeader == null)
                    throw new ArgumentException("Project does not exist. Perhaps project name is incorrect");

                Pick2DAO dao = new Pick2DAO();

                dao.RenameOrInsertIdLoad(projectHeader.Code, oldIdName, newIdName, fromId, doUpdate);
            }            
        }
    }
}