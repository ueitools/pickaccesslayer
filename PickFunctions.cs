using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using PickData;
using BusinessObject;
using Project = BusinessObject.Project;

namespace PickAccessLayer
{
    /// <summary>
    /// combination functions that don't go under add, update, purge, retrieve
    /// </summary>
    public static class PickDBFunctions
    {
        public static int AddOrUpdateProjectInfo(string projectName, 
            string memoryType, int sectorSize, string manufacturerCode, 
            string description)
        {
            int rid = -1;

            if (PickRetrieveFunctions.ProjectExists(projectName))
                rid = PickUpdateFunctions.UpdatePickProjectInfo(projectName, memoryType, sectorSize, manufacturerCode, description);
            else
                rid = PickAddFunctions.AddPickProjectInfo(projectName, memoryType, sectorSize, manufacturerCode, description);

            return rid;
        }


        public static void AddOrUpdatePickProjectInfo(string projectName, IProject project)
        {
            using (new ConnectionSwitcher())
            {
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);
                string projectXml = PALUtil.ToXml(project);

                if (string.IsNullOrEmpty(projectXml))
                    throw new ArgumentException(projectName + " has not project information to save.");

                if (String.IsNullOrEmpty(PickRetrieveFunctions.GetPickConfig(projectName)))
                {
                    Pick2DAOFactory.Pick2DAO().InsertPickConfig(projectHeader.Code, projectXml);
                }
                else
                {
                    Pick2DAOFactory.Pick2DAO().UpdatePickConfig(projectHeader.Code, projectXml);
                }
            }
        }

        public static void AddOrUpdatePickProjectLoad(string projectName, PickResultId.List idList)
        {
            using (new ConnectionSwitcher())
            {
                Dictionary<string, bool> loadLookup = CreateLoadLookup(projectName);
                PickResultId.List idListToAdd = new PickResultId.List();
                PickResultId.List idListToUpdate = new PickResultId.List();

                idList.ForEach(delegate(PickResultId id)
                {
                    if (loadLookup.ContainsKey(id.Id))
                        idListToUpdate.Add(id);
                    else
                        idListToAdd.Add(id);
                });
                PickAddFunctions.AddPickProjectLoad(projectName, idListToAdd);
                PickUpdateFunctions.UpdatePickProjectLoad(projectName, idListToUpdate);
            }
        }

        internal static Dictionary<string, bool> CreateLoadLookup(string projectName)
        {
            IDLoadCollection collectionBefore = ProductDBFunctions.GetIDLoadList(projectName);
            Dictionary<string, bool> loadLookup = new Dictionary<string, bool>();

            foreach (IDLoad load in collectionBefore)
                loadLookup[load.ID] = true;
            return loadLookup;
        }
    }
}
