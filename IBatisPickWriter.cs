using System;
using System.Text;
using BusinessObject;
using PickData;

namespace PickAccessLayer
{
    public class IBatisPickWriter : IPickWriter
    {
        public delegate void OnNotifyPickIdDelegate(string msg, int currentCount, int totalCount);
        public delegate bool IsCanceledDelegate();

        private readonly OnNotifyPickIdDelegate _onNotifyPickIdDelegate;
        private readonly IsCanceledDelegate _isCanceledCallback;

        private readonly string _connStr;

        public IBatisPickWriter(string connStr)
        {
            _onNotifyPickIdDelegate = null;
            _connStr = connStr;
        }

        public IBatisPickWriter(OnNotifyPickIdDelegate onNotify, string connStr)
        {
            _onNotifyPickIdDelegate = onNotify;
            _connStr = connStr;
        }

        public IBatisPickWriter(OnNotifyPickIdDelegate onNotify, string connStr,
                                IsCanceledDelegate isCanceledCallback)
        {
            _connStr = connStr;
            _onNotifyPickIdDelegate = onNotify;
            _isCanceledCallback = isCanceledCallback;
        }

        public void Write(string projectName, PickResultId.List pickResultIdList)
        {
            DAOFactory.ResetDBConnection(_connStr);

            int totalCount = pickResultIdList.Count;
            int currentCount = 0;

            AddOrUpdateProjectInfo(projectName);
            PickDBFunctions.AddOrUpdatePickProjectLoad(projectName, pickResultIdList);

            StringBuilder failedAdds = new StringBuilder();

            foreach (PickResultId pickResultId in pickResultIdList)
            {
                if (!PickAddFunctions.AddPickId(projectName, pickResultId, failedAdds))
                    failedAdds.AppendLine("Adding pick id failed: " + pickResultId.Id);
                else
                    currentCount++;

                SendNotification(pickResultId.Id, currentCount, totalCount);

                if (IsCanceled())
                    throw new UserCanceledException();
            }
            if (failedAdds.Length > 0)
            {
                failedAdds.AppendLine(string.Format("{0} ids added. But following ids had problems: ", currentCount));
                throw new ArgumentException(failedAdds.ToString());
            }
        }

        //public void Write(string projectName, PickResultId.List pickResultIdList)
        //{
        //    StringBuilder logBuilder = new StringBuilder("== Write Method ");
        //    logBuilder.AppendLine("");

        //    DateTime methodStartTime = DateTime.Now;

        //    DAOFactory.ResetDBConnection(_connStr);

        //    int totalCount = pickResultIdList.Count;
        //    int currentCount = 0;

        //    DateTime startTime = DateTime.Now;
        //    AddOrUpdateProjectInfo(projectName);
        //    DateTime endTime = DateTime.Now;
        //    TimeSpan AddOrUpdateProjectInfoSpan = endTime - startTime;

        //    startTime = DateTime.Now;
        //    PickDBFunctions.AddOrUpdatePickProjectLoad(projectName, pickResultIdList);
        //    endTime = DateTime.Now;
        //    TimeSpan AddOrUpdatePickProjectLoadOSpan = endTime - startTime;
        //    //PickDBFunctions.AddOrUpdatePickProjectLoad(projectName, pickResultIdList);

        //    StringBuilder failedAdds = new StringBuilder();

        //    startTime = DateTime.Now;
        //    foreach (PickResultId pickResultId in pickResultIdList)
        //    {
        //        if (!PickAddFunctions.AddPickId(projectName, pickResultId, failedAdds))
        //            failedAdds.AppendLine("Adding pick id failed: " + pickResultId.Id);
        //        else
        //            currentCount++;

        //        SendNotification(pickResultId.Id, currentCount, totalCount);

        //        if (IsCanceled())
        //            throw new UserCanceledException();
        //    }
        //    endTime = DateTime.Now;
        //    TimeSpan AddPickIdSpan = endTime - startTime;

        //    if (failedAdds.Length > 0)
        //    {
        //        failedAdds.AppendLine(string.Format("{0} ids added. But following ids had problems: ", currentCount));
        //        throw new ArgumentException(failedAdds.ToString());
        //    }

        //    TimeSpan totalTime = endTime - methodStartTime;

        //    logBuilder.AppendLine(String.Format("===> AddOrUpdateProjectInfo Method : {0} ", AddOrUpdateProjectInfoSpan.ToString()));
        //    logBuilder.AppendLine(String.Format("===> PickDBFunctions.AddOrUpdatePickProjectLoad Method : {0} ", AddOrUpdatePickProjectLoadOSpan.ToString()));
        //    logBuilder.AppendLine(String.Format("===> PickAddFunctions.AddPickId Method Loop: {0} ", AddPickIdSpan.ToString()));
        //    logBuilder.AppendLine(String.Format("===> Total taken by Write Method: {0} ", totalTime.ToString()));
        //    logBuilder.AppendLine("== Write Method End ");

        //    using (System.IO.StreamWriter sWriter = new System.IO.StreamWriter(@"D:\WorkingDirForPGS\LogsForT0251\IdLoadLog.txt", true))
        //    {
        //        sWriter.Write(logBuilder.ToString());
        //        sWriter.Close();
        //    }
        //}

        protected void SendNotification(string idName, int currentCount, int totalCount)
        {
            if (_onNotifyPickIdDelegate != null)
                _onNotifyPickIdDelegate(idName, currentCount, totalCount);
        }

        protected bool IsCanceled()
        {
            if (_isCanceledCallback != null)
                return _isCanceledCallback();

            return false;
        }

        //
        // this guesses default data. assuming user changes it later in other tool.
        //
        private void AddOrUpdateProjectInfo(string projectName)
        {
            
            string defaultManufacturerCode = "Q";
            string memoryType = "FDRA_GENII";
            int sectorSize = 0;
            string description = "";

            PickAccessLayer.PickProjectDAO _project = new PickProjectDAO ( );
            
            ProjectHeader _header=_project.SelectHeader ( projectName );

            if ( _header != null )
                {
                    if (_header.Name == "APAC 2013 Samsung VD Database")
                    {
                        memoryType = _header.MemoryType;
                        sectorSize = _header.SectorSize;
                        description = _header.Description;
                        defaultManufacturerCode = "X";
                    }
                else if ( _header.Name != null )
                    {
                    memoryType = _header.MemoryType;
                    sectorSize = _header.SectorSize;
                    description = _header.Description;
                    defaultManufacturerCode = _header.Manufacturer_Code;
                    }
                
                }

            PickDBFunctions.AddOrUpdateProjectInfo(projectName,
                                                   memoryType, sectorSize, defaultManufacturerCode, description);
        }

        public void WritePickConfig(string projectName, IProject config)
        {
            DAOFactory.ResetDBConnection(_connStr);
            AddOrUpdateProjectInfo(projectName);
            PickDBFunctions.AddOrUpdatePickProjectInfo(projectName, config);
        }
    }
}
