using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using BusinessObject;
using PickData;
using PickData.ConfigurationData;
using Project = BusinessObject.Project;

namespace PickAccessLayer.DataAccessObjects
{
    public class ProjectWithPick
    {
        private Project _project;
        private IProject _pickDataProject;

        public ProjectWithPick()
        {
            _project = new Project();
            _pickDataProject = null;
        }

        public ProjectHeader Header
        {
            get { return _project.Header; }
            set { _project.Header = value; }
        }

        public Firmware Firmware
        {
            get { return _project.Firmware; }
            set { _project.Firmware = value; }
        }

        public Project Project
        {
            get { return _project; }
            set { _project = value; }
        }

        public IProject PickProject
        {
            get { return _pickDataProject; }    
            set { _pickDataProject = value; }
        }

        public ConfigCollection ConfigList
        {
            get { return _project.ConfigList; }
            set { _project.ConfigList = value; }
        }

        public void Add()
        {
            DAOFactory.Project().AddPick2Related(_project);
        }

        public void Overwrite()
        {
            DAOFactory.Project().RemovePick2Related(_project);
            Add();
        }
    }
}
