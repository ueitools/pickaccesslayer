using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using BusinessObject;
using PickData;

namespace PickAccessLayer.DataAccessObjects
{
    [Serializable]
    public class PickIdDAO : DAO
    {
        private int _FK_ProjectCode;
        private string _FK_ID;
        private int _version;
        private string _userName;
        private string _comment;
        private int _exec;
        private char _isInverseData;
        private char _isHexFormat;
        private char _isInternalPick;
        private char _isTemporary;
        private char _isInversePrefix;
        private char _isExternalPrefix;
        private char _isFrequencyData;
        private DateTime _creationDate;
        private int _PK_PickID;
        private int _systemFlags;
        private int _FK_IDSnapshot;
        private string _rowguid;
        private BindingListEx<PickPrefixDAO> _prefixList;
        private BindingListEx<PickDataMapDAO> _dataMapList;
        private BindingListEx<PickIdSnapshotDAO> _snapshot;

        public PickIdDAO()
        {
            _prefixList = new BindingListEx<PickPrefixDAO>();
            _dataMapList = new BindingListEx<PickDataMapDAO>();
            _snapshot = new BindingListEx<PickIdSnapshotDAO>();
        }

        public int FK_Project_Code
        {
            get { return _FK_ProjectCode; }
            set { _FK_ProjectCode = value; }
        }

        public string FK_ID
        {
            get { return _FK_ID; }
            set { _FK_ID = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public int Exec
        {
            get { return _exec; }
            set { _exec = value; }
        }

        public char IsInverseData
        {
            get { return _isInverseData; }
            set { _isInverseData = value; }
        }

        public char IsHexFormat
        {
            get { return _isHexFormat; }
            set { _isHexFormat = value; }
        }

        public char IsInternalPick
        {
            get { return _isInternalPick; }
            set { _isInternalPick = value; }
        }

        public char IsTemporary
        {
            get { return _isTemporary; }
            set { _isTemporary = value; }
        }

        public char IsInversePrefix
        {
            get { return _isInversePrefix; }
            set { _isInversePrefix = value; }
        }

        public char IsExternalPrefix
        {
            get { return _isExternalPrefix; }
            set { _isExternalPrefix = value; }
        }

        public char IsFrequencyData
        {
            get { return _isFrequencyData; }
            set { _isFrequencyData = value; }
        }

        public int FK_IDSnapshot
        {
            get { return _FK_IDSnapshot; }
            set { _FK_IDSnapshot = value; }
        }

        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public int PK_PickID
        {
            get { return _PK_PickID; }
            set { _PK_PickID = value; }
        }

        public int SystemFlags
        {
            get { return _systemFlags; }
            set { _systemFlags = value; }
        }

        public string Rowguid
        {
            get { return _rowguid; }
            set { _rowguid = value; }
        }

        public BindingListEx<PickPrefixDAO> PrefixList
        {
            get { return _prefixList;  }
            set { _prefixList = value; }
        }

        public BindingListEx<PickDataMapDAO> DataMap
        {
            get { return _dataMapList; }
            set { _dataMapList = value; }
        }

        public BindingListEx<PickIdSnapshotDAO> Snapshot
        {
            get { return _snapshot;  }
            set { _snapshot = value; }
        }

        public List<PickData.PickId> GetSnapshotAsId()
        {
            List<PickData.PickId> listOfId = new List<PickId>();

            foreach (PickIdSnapshotDAO snapshot in _snapshot)
                listOfId.Add(PALUtil.FromPickIdXml(snapshot.OriginalId));

            return listOfId;
        }

        public PickId GetFirstSnapshotAsId()
        {
            return GetSnapshotAsId()[0];
        }

        public string FindFirstMatchingIdLabel(string data)
        {
            PickId pickId = GetFirstSnapshotAsId();
            string label = "Non functional";

            foreach (PickFunction function in pickId.FunctionList)
            {
                if (!String.IsNullOrEmpty(function.Data) &&
                    string.Compare(function.Data.Trim(), data.Trim()) == 0)
                {
                    label = function.Label;
                    break;
                }
            }
            return label;
        }

        public string FindFirstMatchingIdLabel(string data, PickId pickId)
        {
            string label = "Non functional";

            foreach (PickFunction function in pickId.FunctionList)
            {
                if (!String.IsNullOrEmpty(function.Data) &&
                    string.Compare(function.Data.Trim(), data.Trim()) == 0)
                {
                    label = function.Label;
                    break;
                }
            }
            return label;
        }

        public List<string> FindAllMatchingIdLabel(string data)
        {
            PickId pickId = GetFirstSnapshotAsId();
            List<string> labelList = new List<string>();

            foreach (PickFunction function in pickId.FunctionList)
            {
                if (!String.IsNullOrEmpty(function.Data) &&
                    string.Compare(function.Data.Trim(), data.Trim()) == 0 &&
                    !labelList.Contains(function.Label.Trim()))
                {
                    labelList.Add(function.Label.Trim());
                }
            }
            if (labelList.Count == 0)
                labelList.Add("Non functional");

            return labelList;
        }

        public bool IsDataPicked(string data)
        {
            bool found = false;

            string target = data.Trim();

            foreach (PickDataMapDAO dataMap in DataMap)
            {
                string current = dataMap.Data;
                found = !string.IsNullOrEmpty(current) && string.Compare(target, current.Trim()) == 0;
                if (found)
                    break;
            }
            return found;
        }

        public int CountDataPicked()
        {
            int count = 0;
            foreach (PickDataMapDAO dataMap in DataMap)
            {
                if (!string.IsNullOrEmpty(dataMap.Data.Trim()))
                    count++;
            }
            return count;
        }

        public static PickIdDAO GetFromDB(int projectCode, string id, int version)
        {
            Hashtable param = new Hashtable();

            param["ProjectCode"] = projectCode;
            param["FK_ID"] = id;
            param["Version"] = version;

            PickIdDAO pickId = Mapper().QueryForObject<PickIdDAO>("Pick2CopyPaste.SelectPickId", param);

            pickId.PrefixList = PickPrefixDAO.GetFromDB(pickId.PK_PickID);
            pickId.DataMap = PickDataMapDAO.GetFromDB(pickId.PK_PickID);
            pickId.Snapshot = PickIdSnapshotDAO.GetFromDB(pickId.FK_IDSnapshot);

            return pickId;
        }

        public void WriteDB()
        {
            WriteDB(false);
        }

        public void WriteDB(bool withCreationDate)
        {
            bool isAscending = false;

            _snapshot.Sort(isAscending, new string[] { "CreationDate" });
            PickIdSnapshotDAO snapshot = _snapshot[0];

            Pick2DAO dao = new Pick2DAO();
            int snapshotKey = dao.InsertIdSnapshot(_FK_ProjectCode, snapshot.OriginalId);
            _FK_IDSnapshot = snapshotKey;

            if (withCreationDate)
                Mapper().QueryForObject("Pick2CopyPaste.InsertPickIdWithDate", this);
            else
                Mapper().QueryForObject("Pick2CopyPaste.InsertPickIdWithVersion", this);

            foreach (PickPrefixDAO pickPrefix in PrefixList)
            {
                pickPrefix.FK_PickID = PK_PickID;
                Mapper().Insert("Pick2CopyPaste.InsertPickPrefix", pickPrefix);
            }
            foreach (PickDataMapDAO pickDataMap in DataMap)
            {
                pickDataMap.FK_PickId = PK_PickID;
                Mapper().Insert("Pick2CopyPaste.InsertPickDataMap", pickDataMap);
            }
        }

        public List<int> CollectPrefixes()
        {
            if (IsExternalPrefix == 'Y')
                throw new NotImplementedException("error: ID is no longer valid");

            List<int> prefixes = new List<int>();

            if (_prefixList == null || _prefixList.Count == 0)
                return prefixes;

            foreach (PickPrefixDAO prefix in _prefixList)
            {
                if (String.IsNullOrEmpty(prefix.Data))
                {
                    prefixes.Add(0);
                    continue;
                }
                prefixes.Add(Convert.ToInt32(prefix.Data, 2));
            }
            return prefixes;
        }
    }
}
