﻿using System;
using System.Collections;
using System.Collections.Generic;
using BusinessObject;
using PickData;


namespace PickAccessLayer
{
    public class CopyProjectDAO : DAO
    {        
        public void InsertIDLoad_Copy(int oldProjectCode, int newProjectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("oldProjectCode", oldProjectCode);
            paramList.Add("newProjectCode", newProjectCode);
            Mapper().Insert("Pick2.InsertIDLoad_CopyProject", paramList);
        }

        public void InsertOriginalIDSnapshot_Copy(int oldProjectCode, int newProjectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("oldProjectCode", oldProjectCode);
            paramList.Add("newProjectCode", newProjectCode);
            Mapper().Insert("Pick2.InsertOriginalIDSnapshot_CopyProject", paramList);
        }

        public void InsertPickID2_Copy(int oldProjectCode, int newProjectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("oldProjectCode", oldProjectCode);
            paramList.Add("newProjectCode", newProjectCode);
            Mapper().Insert("Pick2.InsertPickID2_CopyProject", paramList);
        }

        public void InsertPickPrefix2(int oldProjectCode, int newProjectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("oldProjectCode", oldProjectCode);
            paramList.Add("newProjectCode", newProjectCode);
            Mapper().Insert("Pick2.InsertPickPrefix2_CopyProject", paramList);
        }

        public void InsertPickDataMap2(int oldProjectCode, int newProjectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("oldProjectCode", oldProjectCode);
            paramList.Add("newProjectCode", newProjectCode);
            Mapper().Insert("Pick2.InsertPickDataMap2_CopyProject", paramList);
        }
        public void InsertOptionalTables(int oldProjectCode, int newProjectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("oldProjectCode", oldProjectCode);
            paramList.Add("newProjectCode", newProjectCode);

            Mapper().Insert("Pick2.InsertExecutorLoad_CopyProject", paramList);

            Mapper().Insert("Pick2.InsertReport_CopyProject", paramList);

            Mapper().Insert("Pick2.InsertIDElimination_CopyProject", paramList);

            Mapper().Insert("Pick2.InsertFirmware_CopyProject", paramList);

            Mapper().Insert("Pick2.InsertGoldenImage_CopyProject", paramList);

            Mapper().Insert("Pick2.InsertProjectchip_CopyProject", paramList);

        }     

    }
}
