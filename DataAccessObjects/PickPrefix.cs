using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace PickAccessLayer.DataAccessObjects
{
    [Serializable]
    public class PickPrefixDAO : DAO
    {
        private int _FK_PickID;
        private string _data;
        private string _comment;
        private int _order;
        private int _insertedRID;
        private DateTime _dateTime;
        private string _rowguid;
        private int _PK_PickPrefix;

        public int FK_PickID
        {
            get { return _FK_PickID; }
            set { _FK_PickID = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public int InsertedRid
        {
            get { return _insertedRID; }
            set { _insertedRID = value; }
        }

        public DateTime CreationDate
        {
            get { return _dateTime; }
            set { _dateTime = value; }
        }

        public string Rowguid
        {
            get { return _rowguid; }
            set { _rowguid = value; }
        }

        public int PK_PickPrefix
        {
            get { return _PK_PickPrefix; }
            set { _PK_PickPrefix = value; }
        }

        public static BindingListEx<PickPrefixDAO> GetFromDB(int PK_PickID)
        {
            Hashtable param = new Hashtable();

            param["FK_PickID"] = PK_PickID;
            BindingListEx<PickPrefixDAO> list =
                new BindingListEx<PickPrefixDAO>(
                    Mapper().QueryForList<PickPrefixDAO>("Pick2CopyPaste.SelectPickPrefix", param));

            return list;
        }

        public static BindingListEx<PickPrefixDAO> GetBulkFromDB(int projectCode)
        {
            Hashtable param = new Hashtable();

            param["FK_Project_Code"] = projectCode;
            BindingListEx<PickPrefixDAO> list =
                new BindingListEx<PickPrefixDAO>(
                    Mapper().QueryForList<PickPrefixDAO>("Pick2CopyPaste.BulkSelectPickPrefix", param));

            return list;
        }        

        public void Paste()
        {
            Mapper().Insert("Pick2CopyPaste.InsertPickPrefix", this);
        }
    }
}
