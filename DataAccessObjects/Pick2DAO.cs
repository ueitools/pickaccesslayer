using System;
using System.Collections;
using System.Collections.Generic;
using BusinessObject;
using PickData;

namespace PickAccessLayer
{
    public class Pick2DAO : DAO
    {
        public void UpdateFromId(string projectName, string id, string fromId)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectName", projectName);
            paramList.Add("ID", id);
            paramList.Add("FromId", fromId);

            Mapper().Update("Pick2.UpdateLoadWithFromId", paramList);
        }

        public void UpdatePickConfig(int projectCode, string configFile)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ConfigFile", configFile);

            Mapper().Update("Pick2.UpdatePickConfig", paramList);
        }

        public void InsertPickConfig(int projectCode, string configFile)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ConfigFile", configFile);

            Mapper().Insert("Pick2.InsertPickConfig", paramList);
        }

        public string GetPickConfig(int projectCode)
        {
            return Mapper().QueryForObject("Pick2.GetPickConfig", projectCode) as string;
        }

        public int InsertIdSnapshot(int projectCode, string idSnapshot)
        {
            Hashtable paramList = new Hashtable();
            const int dummy = 0;

            paramList.Add("OriginalID", idSnapshot);
            paramList.Add("InsertedRID", dummy);
            paramList.Add("FK_ProjectCode", projectCode);

            Mapper().QueryForObject("Pick2.InsertIdSnapshot", paramList);

            return (int)paramList["InsertedRID"];
        }

        public string GetIdSnapshot(string projectName, string id, int version)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ID", id);
            paramList.Add("ProjectName", projectName);
            paramList.Add("Version", version);
            return Mapper().QueryForObject("Pick2.GetIdSnapshot", paramList) as string;
        }

        public int GetIdLastSnapshotKey(string projectName, string id)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ID", id);
            paramList.Add("ProjectName", projectName);
            return Mapper().QueryForList<int>("Pick2.GetIdSnapshotKey", paramList)[0];
        }

        public Guid InsertPickPrefix(int idKey, string data, string comment, int order)
        {
            Hashtable paramList = new Hashtable();
            Guid dummy = Guid.NewGuid();

            paramList.Add("FK_PickID", idKey);
            paramList.Add("Data", data);
            paramList.Add("Comment", comment);
            paramList.Add("Order", order);
            paramList.Add("InsertedRID", dummy);

            Mapper().QueryForObject("Pick2.InsertPickPrefix", paramList);

            return (Guid)paramList["InsertedRID"];
        }

        public IList<PickPrefix> GetPickPrefix(string id)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ID", id);
            return Mapper().QueryForList<PickPrefix>("Pick2.GetPickPrefix", paramList);
        }

        public IList<PickPrefix> GetPickPrefixWithVersion(string projectName, string id, int version)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ID", id);
            paramList.Add("Version", version);
            paramList.Add("ProjectName", projectName);

            return Mapper().QueryForList<PickPrefix>("Pick2.GetPickPrefixWithVersion", paramList);
        }

        public string GetInversePrefixFlag(string ProjectName, string Id, int Version)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("FK_ID", Id);
            paramList.Add("Version", Version);
            paramList.Add("ProjectName", ProjectName);

            return (string)Mapper().QueryForObject("Pick2.GetInversePrefixFlag", paramList);
        }

        public IList<Hashtable> GetPickItems_Test(string projectName,string id,int version)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("Id", id);
            paramList.Add("Version", version);
            paramList.Add("ProjectName", projectName);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickItems_Test", paramList);
            
        }

        public IList<Hashtable> GetPickPrefixItems_Test(string ProjectName, string Id, int Version)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", ProjectName);
            paramList.Add("Id", Id);
            paramList.Add("Version", Version);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickPrefixItems_Test", paramList);
        }

        public Guid InsertPickDataMap(int idKey, string data, string idLabel, string firmwareLabel, string comment, string intron, string intronPriority, int keyNumber)
        {
            Hashtable paramList = new Hashtable();
            Guid dummy = Guid.NewGuid();

            paramList.Add("FK_PickID", idKey);
            paramList.Add("Data", data);
            paramList.Add("IDLabel", idLabel);
            paramList.Add("FirmwareLabel", firmwareLabel);
            paramList.Add("Comment", comment);
            paramList.Add("Intron", intron);
            paramList.Add("IntronPriority", intronPriority);
            paramList.Add("KeyNumber", keyNumber);
            paramList.Add("InsertedRID", dummy);

            Mapper().QueryForObject("Pick2.InsertPickDataMap", paramList);

            return (Guid)paramList["InsertedRID"];
        }

        public IList<Hashtable> GetPickDataMap(string id)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ID", id);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickDataMap", paramList);
        }

        public IList<Hashtable> GetPickDataMapWithVersion(string projectName, string id, int version)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ID", id);
            paramList.Add("Version", version);
            paramList.Add("ProjectName", projectName);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickDataMapWithVersion", paramList);
        }

        public IList<Hashtable> GetPickIdList(string projectname)
        {
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickIdListWithMaxVersion", projectname);
        }

        public IList<Hashtable> GetAllVersionsOfId(string ProjectName, string Id)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("Id", Id);
            paramList.Add("Projectname", ProjectName);
            return Mapper().QueryForList<Hashtable>("Pick2.GetAllVersionsOfId", paramList);
        }

        public IList<Hashtable> GetPickIdInfo(string ProjectName, string Id, int Version)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", ProjectName);
            paramList.Add("Id", Id);
            paramList.Add("Version", Version);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickIdInfo", paramList);
        }

        public IList<Hashtable> GetPickIdWithAlias(string projectname, ArrayList IDList)
        {
            //if (IDList.Count > 2000)
            //    return GetlargePickIdWithAlias(projectname, IDList);
            //return GetPickIdList(IDList, projectname);

            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
			if(IDList!=null)
		{
            string _idList = string.Empty;
            foreach (string id in IDList)
            {
                _idList += id + ",";
            }
            paramList.Add("IDList", _idList);
		}
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickIdWithAlias", paramList);
        }

        private IList<Hashtable> GetlargePickIdWithAlias(string projectname, ArrayList IDList)
        {
            const int MAXPARAMETERS = 2000;
            List<ArrayList> maxCollections = new List<ArrayList>();
            ArrayList current = new ArrayList();

            for (int i = 0; i < IDList.Count; i++)
            {
                if (i%MAXPARAMETERS == 0)
                {
                    current = new ArrayList();
                    maxCollections.Add(current);
                }
                current.Add(IDList[i]);
            }

            IList<Hashtable> result = new List<Hashtable>();
            foreach (ArrayList idSubList in maxCollections)
            {
                IList<Hashtable> temp = GetPickIdList(idSubList, projectname);
                foreach (Hashtable item in temp)
                    result.Add(item);
            }
            return result;
        }

        private IList<Hashtable> GetPickIdList(ArrayList IDList, string projectname)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            paramList.Add("IDList", IDList);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickIdWithAlias", paramList);
        }

        public IList<Hashtable> GetPickIdNames(int projectCode)
        {
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickIdList", projectCode);
        }

        public IList<Hashtable> GetPickProjectNames()
        {
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickProjectNames", "");
        }

        public IList<Hashtable> GetExecIdMap(string ProjectName, ArrayList idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", ProjectName);
            if ( idlist != null )
            {
                string _idList = string.Empty;
                foreach ( string id in idlist )
                {
                    _idList += id + ",";
                }
                paramList.Add ( "IDList", _idList );
            }
            return Mapper().QueryForList<Hashtable>("Pick2.GetExecIdMap", paramList);
        }

        //public IList<Hashtable> GetPrefixInfo ( string projectname, string Id, string version )
        //{
        //    Hashtable paramList = new Hashtable ( );
        //    paramList.Add ( "ProjectName", ProjectName );
        //    paramList.Add ( "Id", Id );
        //    paramList.Add ( "Version", version );
        //    if ( idlist != null )
        //    {
        //        string _idList = string.Empty;
        //        foreach ( string id in idlist )
        //        {
        //            _idList += id + ",";
        //        }
        //        paramList.Add ( "IDList", _idList );
        //    }
        //    return Mapper ( ).QueryForList<Hashtable> ( "Pick2.GetPrefixInfo", paramList );

        //}

        public int GetMaxVersion(string ProjectName, string Id)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", ProjectName);
            paramList.Add("Id", Id);
            object verObj = Mapper().QueryForObject("Pick2.GetMaxVersion", paramList);

            if (verObj == null)
            {
                return -1;
            }
            else
                return (int)verObj;
            //return (int)Mapper().QueryForObject("Pick2.GetMaxVersion", paramList);
        }

        public IList<Hashtable> GetFunctionSearchCount(string projectname, ArrayList idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            string _idList = string.Empty;
            foreach (string id in idlist)
            {
                _idList += id + ",";
            }
            paramList.Add("IDList", _idList);
            return Mapper().QueryForList<Hashtable>("Pick2.GetFunctionSearchCount", paramList);
        }

        public IList<Hashtable> SearchFunction(string projectname, ArrayList idlist, ArrayList functionlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            string _idList = string.Empty;
            string _fnList = string.Empty;
            foreach (string id in idlist)
            {
                _idList += id + ",";
            }

            foreach (string fn in functionlist)
            {
                _fnList += fn + ",";
            }

            paramList.Add("IDList", _idList);
            paramList.Add("FunctionFilter", 0);
            paramList.Add("FunctionList", _fnList);
            return Mapper().QueryForList<Hashtable>("Pick2.SearchFunction", paramList);
        }

        public IList<Hashtable> SearchUnPickedfunctions(string projectname, ArrayList idlist, ArrayList functionlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            string _idList = string.Empty;
            string _fnList = string.Empty;
            foreach (string id in idlist)
            {
                _idList += id + ",";
            }

            foreach (string fn in functionlist)
            {
                _fnList += fn + ",";
            }

            paramList.Add("IDList", _idList);
            paramList.Add("FunctionFilter", 1);
            paramList.Add("FunctionList", _fnList);
            return Mapper().QueryForList<Hashtable>("Pick2.SearchFunction", paramList);
        }

        public IList<Hashtable> GetEmptyPickedIds(string projectname, ArrayList idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            string _idList = string.Empty;
            string _fnList = string.Empty;
            foreach (string id in idlist)
            {
                _idList += id + ",";
            }

            if (_idList.Length == 0)
            {
               return Mapper().QueryForList<Hashtable>("Pick2.GetEmptyPickedIds", paramList);
            }
            else
            {
                paramList.Add("IDList", _idList);
                return Mapper().QueryForList<Hashtable>("Pick2.GetEmptyPickedIds", paramList);
            }

        }

        public IList<Hashtable> GetAllPickedKeyInfo(string projectname, ArrayList idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            paramList.Add("IDList", idlist);
            return Mapper().QueryForList<Hashtable>("Pick2.SearchFunction", paramList);
        }

        public IList<Hashtable> GetPickedKeyInfoWithoutPrefix(string projectname,ArrayList idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            paramList.Add("VerType", 0);
            string _idList = string.Empty;
            foreach (string id in idlist)
            {
                _idList += id + ",";
            }
            paramList.Add("IDList", _idList);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickedKeyInfoWithoutPrefix", paramList);
        }

        public IList<Hashtable> GetPickedKeyInfoWithoutPrefixV1(string projectname, ArrayList idlist,string version)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            paramList.Add("VerType", 1);
            string _idList = string.Empty;
            foreach (string id in idlist)
            {
                _idList += id + ",";
            }
            paramList.Add("IDList", _idList);
            paramList.Add("Ver", version);
            return Mapper().QueryForList<Hashtable>("Pick2.GetPickedKeyInfoWithoutPrefixV1", paramList);
        }

        public IList<Hashtable> ViewEliminatedIdList(string projectname)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            return Mapper().QueryForList<Hashtable>("Pick2.ViewEliminatedIds", paramList);
        }

        public int InsertPickId(int projectCode, string idName, string userName, string comment, int exec,
            string isInverseData, string isHexFormat, string isInternalPick, string isTemporary,
            string isInversePrefix, string isExternalPrefix, string isFrequencyData, int snapshotKey)
        {
            Hashtable paramList = new Hashtable();
            int dummy = 0;

            paramList.Add("FK_ProjectCode", projectCode);
            paramList.Add("FK_ID", idName);
            paramList.Add("UserName", userName);
            paramList.Add("Comment", comment);
            paramList.Add("Exec", exec);
            paramList.Add("IsInverseData", isInverseData);
            paramList.Add("IsHexFormat", isHexFormat);
            paramList.Add("IsInternalPick", isInternalPick);
            paramList.Add("IsTemporary", isTemporary);
            paramList.Add("InsertedRID", dummy);
            paramList.Add("IsInversePrefix", isInversePrefix);
            paramList.Add("IsExternalPrefix", isExternalPrefix);
            paramList.Add("IsFrequencyData", isFrequencyData);
            paramList.Add("FK_IDSnapshot", snapshotKey);

            Mapper().QueryForObject("Pick2.InsertPickId", paramList);

            return (int)paramList["InsertedRID"];
        }

        public IList<Hashtable> DeletePickIdAfterN(int projectCode, string id, int n)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("N", n);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteAfterN", paramList);
        }

        public IList<Hashtable> DeleteAfterDate(int projectCode, string id, DateTime afterTime)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("AfterTime", afterTime);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteAfterDate", paramList);
        }

        public IList<Hashtable> DeleteAfterN(int projectCode, string id, int n)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("N", n);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteAfterN", paramList);
        }

        public IList<Hashtable> DeleteBeforeDate(int projectCode, string id, DateTime dateTime)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("BeforeTime", dateTime);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteBeforeDate", paramList);
        }

        public IList<Hashtable> DeleteBeforeN(int projectCode, string id, int n)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("N", n);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteBeforeN", paramList);
        }

        public IList<Hashtable> DeleteExceptFirstAndLast(int projectCode, string id)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteExceptFirstAndLast", paramList);
        }

        public IList<Hashtable> DeleteExceptFirstN(int projectCode, string id, int n)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("N", n);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteExceptFirstN", paramList);
        }

        public IList<Hashtable> DeleteExceptLastN(int projectCode, string id, int n)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);
            paramList.Add("N", n);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteExceptLastN", paramList);
        }

        public IList<Hashtable> DeletePickIdVersion(int projectCode, string id, int version)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("Project_Code", projectCode);
            paramList.Add("ID", id);
            paramList.Add("Version", version);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeletePickIdVersion", paramList);            
        }

/// <summary>
        /// Delete All snapshot versions for a given Pick ID
        /// </summary>
        /// <param name="projectName">Project Name</param>
        /// <param name="id">Pick ID Name</param>
        /// <returns></returns>
        public IList<Hashtable> DeleteOriginalIDSnapshots(string projectName, string id)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectName", projectName);
            paramList.Add("ID", id);

            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteIDSnapshots", paramList);
        }

        public void DeleteSomeUnusedOriginalIDSnapshots()
        {
            Mapper().Delete("Pick2Purge.DeleteSomeUnusedSnapshots", null);
        }

        public PickIdHeader GetPickIdHeader(string projectName, string id, int version)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectName", projectName);
            paramList.Add("FK_ID", id);
            paramList.Add("Version", version);

            Hashtable uncheckedTable = Mapper().QueryForObject<Hashtable>("Pick2.GetPickId", paramList);
            HashtableWithCheck table = new HashtableWithCheck(uncheckedTable);

            PickIdHeader result = new PickIdHeader();

            result.ID = (string) table["ID"];
            result.ExecutorCode = (int)table["ExecutorCode"];
            result.IsInversedData = (string)table["IsInverseData"] == "Y";
            result.IsHexFormat = (string)table["IsHexFormat"] == "Y";
            result.Text = (string)table["Text"];
            result.IsInversedPrefix = (string)table["IsInversePrefix"] == "Y";
            result.IsExternalPrefix = (string)table["IsExternalPrefix"] == "Y";
            result.IsFrequencyData = (string) table["IsFrequencyData"] == "Y";

            return result;
        }

        public IList<OptomizedKeyInfo> GetOptimizedKeyInfoList(int projectCode, string modes)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ModeString", modes);

            return Mapper().QueryForList<OptomizedKeyInfo>("Pick2.CountPickedFunctions", paramList);
        }

        public IList<Hashtable> GetAllIdsWithDigitGroup(string projectname,string Firmwarelabel,int labelcount,int datalength,string idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            paramList.Add("FirmwareLabel", Firmwarelabel);
            paramList.Add("LabelCount",labelcount);
            paramList.Add("DataLength",datalength);
            paramList.Add("Ids",idlist);
            //paramList.Add("IDList", idlist);

            //if (isTwoByteData)
            //    return Mapper().QueryForList<Hashtable>("Pick2.GetAllIdsWithDigitGroup", paramList);
            
            return Mapper().QueryForList<Hashtable>("Pick2.GetDigitGroupWithNByteData", paramList);
        }

        public IList<Hashtable> GetIdsWithAllDigitKeysWithData(string firmwarLabel,string keyData,string projectName,int keyCount,char isInverseData,string idlist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("FirmwareLabel", firmwarLabel);
            paramList.Add("KeyData", keyData);
            paramList.Add("Projectname", projectName);
            paramList.Add("KeyCount", keyCount);
            paramList.Add("isInverseData", isInverseData.ToString());
            paramList.Add ( "ids", idlist );
            return Mapper().QueryForList<Hashtable>("Pick2.GetIDWithAllDigitKeys",paramList);
        }

        public void RenameOrInsertIdLoad(int projectCode, string oldIdName, string newIdName, string fromId, string doUpdate)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", oldIdName);
            paramList.Add("NewIDName", newIdName);
            paramList.Add("FromID", fromId);
            paramList.Add("DoUpdate", doUpdate);

            Mapper().QueryForObject("Pick2.UpdateInsertIDLoad", paramList);
        }

        public IList<Hashtable> GetIdenticalIdReport(string projectName, string deviceTypes, string modes)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectName);
            paramList.Add("DeviceTypes", deviceTypes);
            paramList.Add("Modes", modes);

            return Mapper().QueryForList<Hashtable>("Pick2.GetIdenticalIdReport", paramList);

        }

        public IList<Hashtable> StoreAndEliminateIdenticalIds(string ProjectName, string RetainedIDs, string EliminatedIds, string AliasIds, string LineNumbers, int saveStatus)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", ProjectName);
            paramList.Add("RetainedIDs", RetainedIDs);
            paramList.Add("EliminatedIds", EliminatedIds);
            paramList.Add("AliasIds", AliasIds);
            paramList.Add("LineNumbers", LineNumbers);
            paramList.Add("SaveStatus", saveStatus);

            return Mapper().QueryForList<Hashtable>("Pick2.StoreAndEliminateIdenticalIds", paramList);
        }

        //public IList<Hashtable> StoreAndEliminateIdenticalIds(string ProjectName, string RetainedIDs, string EliminatedIds,string LineNumbers,int saveStatus)
        //{
        //    Hashtable paramList = new Hashtable();
        //    paramList.Add("ProjectName", ProjectName);
        //    paramList.Add("RetainedIDs", RetainedIDs);
        //    paramList.Add("EliminatedIds", EliminatedIds);
        //    paramList.Add("LineNumbers", LineNumbers);
        //    paramList.Add("SaveStatus", saveStatus);

        //    return Mapper().QueryForList<Hashtable>("Pick2.StoreAndEliminateIdenticalIds", paramList);
        //}
		public IList<Hashtable> DeletePickIds(string projectname, ArrayList idModelist)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            string _idModeList = string.Empty;
            foreach (string idMode in idModelist)
            {
                if (idMode.Length >= 5)
                {
                    if (char.IsLetter(idMode[0]))
                        if (char.IsNumber(idMode[1]) && char.IsNumber(idMode[2]) && 
                            char.IsNumber(idMode[3]) && char.IsNumber(idMode[4]))
                            _idModeList += idMode.Substring(0, 5) + ",";
                }
                else if ( idMode.Length == 1 )
                {
                    if ( char.IsLetter ( idMode [0] ) )
                    {
                        _idModeList += idMode;
                    }
                }
            }
            if (string.IsNullOrEmpty(_idModeList))
                return null;

            paramList.Add("IDModeList", _idModeList);
            return Mapper().QueryForList<Hashtable>("Pick2Purge.DeletePickIds", paramList);
        }

        public int DeletePickIds(string projectname, ArrayList idModelist, int systemflag)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectname);
            string _idModeList = string.Empty;
            foreach(string idMode in idModelist)
            {
                if(idMode.Length >= 5)
                {
                    if(char.IsLetter(idMode[0]))
                        if(char.IsNumber(idMode[1]) && char.IsNumber(idMode[2]) &&
                            char.IsNumber(idMode[3]) && char.IsNumber(idMode[4]))
                            _idModeList += idMode.Substring(0, 5) + ",";
                }
            }

            if(string.IsNullOrEmpty(_idModeList))
                return 0;

            paramList.Add("IDModeList", _idModeList);
            paramList.Add("SystemFlags", systemflag);
            string _result = Mapper().QueryForObject("Pick2Purge.DeletePickIds_Test", paramList) as string;
            return Convert.ToInt16(_result);

        }
        public string IsDataInversed(string id, string projectname)
        {
            Hashtable ht = new Hashtable();
            string _status=string.Empty;
            ht.Add("Id", id);
            ht.Add("ProjectName", projectname);
            IList<Hashtable> _result = Mapper().QueryForList<Hashtable>("Pick2.GetInversedData", ht);
            foreach(Hashtable _ht in _result)
            {
                _status=_ht["IsInverseData"].ToString();
                break;
            }
            return _status;
        }

		public void DeleteDatabaseLoadIDs(string projectName, string id)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectName);
            paramList.Add("ID", id);
            Mapper().QueryForList<Hashtable>("Pick2Purge.DeleteLoadID", paramList);
        }

        public IList<int> GetExecListFromPickedIDs(int projectCode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectCode", projectCode);
            return Mapper().QueryForList<int>("Pick2.GetExecListFromPickedIDs", paramList);
        }

        public IList<Hashtable> GetAllProjectLocks ()
        {
            Hashtable ht = new Hashtable ( );
            IList<Hashtable> _result = Mapper ( ).QueryForList<Hashtable> ( "Pick2.GetAllProjectLocks", ht );
            return _result;
        }

        public void DeleteProjectLock (string projectname)
        {
            Hashtable ht = new Hashtable ( );
            ht.Add ( "ProjectName", projectname );
            Mapper ( ).QueryForList<Hashtable> ( "Pick2.DeleteProjectLock", ht );

        }

        public IList<Hashtable> GetAllProjectsInfo ()
        {
            Hashtable ht = new Hashtable ( );
            IList<Hashtable> _result = Mapper ( ).QueryForList<Hashtable> ( "Pick2.GetAllProjectsInfo", ht );
            return _result;
        }

        public IList<Hashtable> GetIdLoad (string projectname)
        {
            Hashtable ht = new Hashtable ( );
            ht.Add ( "ProjectName", projectname );
            IList<Hashtable> _result = Mapper ( ).QueryForList<Hashtable> ( "Pick2.GetIdLoad", ht );
            return _result;
        }


        public void DeleteIdenticalIdReport ( string projectname )
        {
            Hashtable ht = new Hashtable ( );
            ht.Add ( "ProjectName", projectname );
            Mapper ( ).QueryForList<Hashtable> ( "Pick2.DeleteIdenticalIdReport", ht );
            
        }           
}
}
