using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace PickAccessLayer.DataAccessObjects
{
    [Serializable]
    public class ReportDAO : DAO
    {
        int _PK_Report;
        int _FK_Project_Code;
        string _userName;
        string _report;
        DateTime _creationDate;
        int _FK_ReportType;

        public int PK_Report
        {
            get { return _PK_Report; }
            set { _PK_Report = value; }
        }

        public int FK_Project_Code
        {
            get { return _FK_Project_Code; }
            set { _FK_Project_Code = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string Report
        {
            get { return _report; }
            set { _report = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public int FK_ReportType
        {
            get { return _FK_ReportType; }
            set { _FK_ReportType = value; }
        }

        public static ReportDAOCollection GetFromDB(int FK_Project_Code)
        {
            Hashtable param = new Hashtable();

            param["FK_Project_Code"] = FK_Project_Code;
            ReportDAOCollection list = Mapper().QueryForList<ReportDAO>("Pick2CopyPaste.SelectReportDAO", param) as ReportDAOCollection;

            return list;
        }
    }

    public class ReportDAOCollection : BindingListEx<ReportDAO>
    {
    }
}
