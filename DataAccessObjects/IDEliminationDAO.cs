using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace PickAccessLayer.DataAccessObjects
{
    [Serializable]
    public class IDEliminationDAO : DAO
    {
        int _PK_IDElimination;
        int _FK_Report;
        string _ReplacementID;
        int _FK_Project_Code;
        string _FK_ID;
        DateTime _CreationDate;
        DateTime _ModificationDate;
        int _LineNumber;

        public int PK_IDElimination
        {
            get { return _PK_IDElimination; }
            set { _PK_IDElimination = value; }
        }

        public int FK_Report
        {
            get { return _FK_Report; }
            set { _FK_Report = value; }
        }

        public string ReplacementID
        {
            get { return _ReplacementID; }
            set { _ReplacementID= value; }
        }

        public int FK_Project_Code
        {
            get { return _FK_Project_Code; }
            set { _FK_Project_Code = value; }
        }

        public string FK_ID
        {
            get { return _FK_ID; }
            set { _FK_ID = value; }
        }

        public DateTime CreationDate
        {
            get { return _CreationDate; }
            set { _CreationDate = value; }
        }

        public DateTime ModificationDate
        {
            get { return _ModificationDate; }
            set { _ModificationDate = value; }
        }

        public int LineNumber
        {
            get { return _LineNumber; }
            set { _LineNumber = value; }
        }

        public static IDEliminationDAOCollection GetFromDB(int FK_Project_Code)
        {
            Hashtable param = new Hashtable();

            param["FK_Project_Code"] = FK_Project_Code;
            IDEliminationDAOCollection list = Mapper().QueryForList(
                "Pick2CopyPaste.SelectIdEliminationDAO", param) as IDEliminationDAOCollection;

            return list;
        }
    }

    [Serializable]
    public class IDEliminationDAOCollection : BindingListEx<IDEliminationDAO>
    {
    }
}
