using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace PickAccessLayer.DataAccessObjects
{
    [Serializable]
    public class PickDataMapDAO : DAO
    {
        private int _FK_PickID;
        private string _data;
        private string _idLabel;
        private string _firmwareLabel;
        private string _comment;
        private string _intron;
        private string _intronPriority;
        private int _keyNumber;
        private int _insertRID;
        private DateTime _creationDate;
        private string _PK_PickDataMap;
        private string _rowguid;

        public int FK_PickId
        {
            get { return _FK_PickID; }
            set { _FK_PickID = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string IdLabel
        {
            get { return _idLabel; }
            set { _idLabel = value; }
        }

        public string FirmwareLabel
        {
            get { return _firmwareLabel; }
            set { _firmwareLabel = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string IntronPriority
        {
            get { return _intronPriority; }
            set { _intronPriority = value; }
        }

        public int KeyNumber
        {
            get { return _keyNumber; }
            set { _keyNumber = value; }
        }

        public int InsertRid
        {
            get { return _insertRID; }
            set { _insertRID = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public string PK_PickDataMap
        {
            get { return _PK_PickDataMap; }
            set { _PK_PickDataMap = value; }
        }

        public string Rowguid
        {
            get { return _rowguid; }
            set { _rowguid = value; }
        }

        public static BindingListEx<PickDataMapDAO> GetFromDB(int PK_PickId)
        {
            Hashtable param = new Hashtable();

            param["FK_PickID"] = PK_PickId;
            BindingListEx<PickDataMapDAO> list = new BindingListEx<PickDataMapDAO>(
                Mapper().QueryForList<PickDataMapDAO>("Pick2CopyPaste.SelectPickDataMap", param));

            return list;
        }

        public static BindingListEx<PickDataMapDAO> GetBulkFromDB(int projectCode)
        {
            Hashtable param = new Hashtable();

            param["FK_Project_Code"] = projectCode;
            BindingListEx<PickDataMapDAO> list = new BindingListEx<PickDataMapDAO>(
                Mapper().QueryForList<PickDataMapDAO>("Pick2CopyPaste.BulkSelectPickDataMap", param));

            return list;
        }

        public void Paste()
        {
            Mapper().Insert("Pick2CopyPaste.InsertPickDataMap", this);
        }
    }
}
