using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace PickAccessLayer.DataAccessObjects
{
    public class PickIdSnapshotDAO : DAO
    {
        private int _PK_OriginalIDSnapshot;
        private int _projectCode;
        private string _OriginalID;
        private DateTime _CreationDate;
        private String _rowguid;

        public int PK_OriginalIDSnapshot
        {
            get { return _PK_OriginalIDSnapshot; }
            set { _PK_OriginalIDSnapshot = value; }
        }

        public string OriginalId
        {
            get { return _OriginalID; }
            set { _OriginalID = value; }
        }

        public DateTime CreationDate
        {
            get { return _CreationDate; }
            set { _CreationDate = value; }
        }

        public string Rowguid
        {
            get { return _rowguid; }
            set { _rowguid = value; }
        }

        public int ProjectCode
        {
            get { return _projectCode; }
            set { _projectCode = value; }
        }

        public static BindingListEx<PickIdSnapshotDAO> GetFromDB(int PK_snapshot)
        {
            Hashtable param = new Hashtable();

            param["PK_OriginalIDSnapshot"] = PK_snapshot;

            BindingListEx<PickIdSnapshotDAO> idSnapshotList =
                new BindingListEx<PickIdSnapshotDAO>(
                Mapper().QueryForList<PickIdSnapshotDAO>("Pick2CopyPaste.SelectPickIdSnapshot", param));

            return idSnapshotList;
        }

        public static BindingListEx<PickIdSnapshotDAO> GetBulkFromDB(int projectCode)
        {
            Hashtable param = new Hashtable();

            param["FK_Project_Code"] = projectCode;

            BindingListEx<PickIdSnapshotDAO> idSnapshotList =
                new BindingListEx<PickIdSnapshotDAO>(
                Mapper().QueryForList<PickIdSnapshotDAO>("Pick2CopyPaste.BulkSelectPickIdSnapshot", param));

            return idSnapshotList;
        }

        public void Paste()
        {
            Mapper().Insert("Pick2CopyPaste.InsertPickIdSnapshot", this);
        }
    }
}
