using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using IBatisNet.Common.Transaction;
using IBatisNet.Common.Utilities;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using BusinessObject;

namespace PickAccessLayer
{
    /// <summary>
    /// Singleton "controller" for Helper classes.
    /// </summary>
    public class Pick2DAOFactory
    {
        public static Pick2DAO Pick2DAO()
        {
            return new Pick2DAO();
        }
    }
}
