using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace PickAccessLayer
{
    class PickProjectDAO : DAO
    {
        /// <summary>
        /// for now have a 2100 limit for stored procedure parameters 
        /// http://msdn.microsoft.com/en-us/library/ms143432.aspx
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="allIdLoadCollection"></param>
        public void InsertIDLoad(int projectCode, IDLoadCollection allIdLoadCollection)
        {
            const int NUMPARAMETERS = 5;
            const int MAXPARAMETERS = 2100/NUMPARAMETERS;
            List<IDLoadCollection> maxCollections = new List<IDLoadCollection>();
            IDLoadCollection current = null;

            for (int i = 0; i < allIdLoadCollection.Count; i++)
            {
                if (i % MAXPARAMETERS == 0)
                {
                    current = new IDLoadCollection();
                    maxCollections.Add(current);
                }
                current.Add(allIdLoadCollection[i]);
            }
            foreach (IDLoadCollection idLoadCollection in maxCollections)
            {
                Hashtable paramList = new Hashtable();

                paramList.Add("IDLoad", idLoadCollection);

                Mapper().Insert("Pick2.InsertIDLoad", paramList);
            }
        }

        public ProjectHeader SelectHeader(string projectName)
        {
            return Mapper().QueryForObject(
                "Pick2.SelectHeader", projectName)
                as ProjectHeader;
        }

        public void SaveHeaderOnly(ProjectHeader projectHeader)
        {
            if (SelectHeader(projectHeader.Name) != null)
                throw new Exception("Duplicate project name.");
          
            Mapper().QueryForObject("Pick2.InsertProjectHeader", projectHeader);
        }

        public void InsertRegularProjectLock(int projectCode)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ProjectCode", projectCode);

            Mapper().QueryForObject("Pick2.InsertProjectLock", paramList);
        }

        public void ClearRegularProjectLock(int projectCode)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ProjectCode", projectCode);

            Mapper().QueryForObject("Pick2Purge.DeleteRegularProjectLocks", paramList);
        }

        public int CountProjectLocks(int projectCode)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("FK_ProjectCode", projectCode);
            paramList.Add("NumLocks", 0);

            Mapper().QueryForObject("Pick2.CheckForProjectLocks", paramList);

            return (int)paramList["NumLocks"];
        }

        public IList<ProjectLockUserInfo> GetWhoHasLocks(int projectCode)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ProjectCode", projectCode);

            return Mapper().QueryForList<ProjectLockUserInfo>("Pick2.GetWhoHasLocks", paramList);
        }
    }
}
