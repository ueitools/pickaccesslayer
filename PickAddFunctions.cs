using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using PickData;
using Project = PickData.Project;

namespace PickAccessLayer
{
    public static class PickAddFunctions
    {
        public static bool AddPickId(string projectName, PickResultId pickResultId, StringBuilder log)
        {
            using (new ConnectionSwitcher())
            {
                try
                {
                    ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);
                    PickId pickId = pickResultId.OriginalPickID;
                    Pick2DAO dao = new Pick2DAO();
                    int snapshotKey = dao.InsertIdSnapshot(projectHeader.Code, PALUtil.ToXml(pickId));
                    return AddPickId(projectName, pickResultId, snapshotKey);
                }
                catch(Exception ex)
                {
                    log.AppendLine(ex.Message);
                    return false;
                }
            }
        }

        public static bool AddPickId(string projectName, PickResultId pickResultId, int snapshotKey)
        {
            using (new ConnectionSwitcher())
            {
                try
                {
                    DAOFactory.BeginTransaction();

                    PickId pickId = pickResultId.OriginalPickID;
                    ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);

                    int projectCode = projectHeader.Code;
                    const string internalPickDefault = "Y";
                    const string tempPickDefault = "N";

                    Pick2DAO dao = new Pick2DAO();

                    int pickIdKey = dao.InsertPickId(projectCode, pickResultId.Id,
                                                     Environment.UserName,
                                                     pickId.Header.Text, pickId.Header.ExecutorCode,
                                                     PALUtil.ConvertToYesNo(pickId.Header.IsInversedData),
                                                     PALUtil.ConvertToYesNo(pickId.Header.IsHexFormat),
                                                     internalPickDefault,
                                                     tempPickDefault,
                                                     PALUtil.ConvertToYesNo(pickId.Header.IsInversedPrefix),
                                                     PALUtil.ConvertToYesNo(pickId.Header.IsExternalPrefix),
                                                     PALUtil.ConvertToYesNo(pickId.Header.IsFrequencyData),
                                                     snapshotKey);

                    for (int prefixNumber = 0; prefixNumber < pickId.Header.PrefixList.Count; prefixNumber++)
                    {
                        dao.InsertPickPrefix(pickIdKey,
                                             pickId.Header.PrefixList[prefixNumber].Data,
                                             pickId.Header.PrefixList[prefixNumber].Description, prefixNumber);
                    }

                    for (int keyNumber = 0; keyNumber < pickResultId.KeyList.Count; keyNumber++)
                    {
                        if (!String.IsNullOrEmpty(pickResultId.KeyList[keyNumber].DeviceKey.FirmwareLabel))
                            dao.InsertPickDataMap(pickIdKey, pickResultId.KeyList[keyNumber].Data,
                                                  pickResultId.KeyList[keyNumber].Label,
                                                  pickResultId.KeyList[keyNumber].DeviceKey.FirmwareLabel,
                                                  pickResultId.KeyList[keyNumber].Comment,
                                                  pickResultId.KeyList[keyNumber].Intron,
                                                  pickResultId.KeyList[keyNumber].IntronPriority,
                                                  pickResultId.KeyList[keyNumber].DeviceKey.KeyIndex);
                    }
                    DAOFactory.CommitTransaction();
                    return true;
                }
                catch (Exception ex)
                {
                    DAOFactory.RollBackTransaction();
                    throw ex;
                }
            }
        }

        public static void AddPickProjectLoad(string projectName, PickResultId.List idList)
        {
            if (idList.Count == 0)
                return;

            using (new ConnectionSwitcher())
            {
                IDLoadCollection idLoadCollection = CreateLoadCollection(projectName, idList);
                ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);
                PickProjectDAO dao = new PickProjectDAO();

                try
                {
                    DAOFactory.BeginTransaction();
                    dao.InsertIDLoad(projectHeader.Code, idLoadCollection);
                    DAOFactory.CommitTransaction();
                }
                catch
                {
                    DAOFactory.RollBackTransaction();
                    throw;
                }
            }
        }

        internal static IDLoadCollection CreateLoadCollection(string projectName, PickResultId.List idList)
        {
            ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(projectName);
            IDLoadCollection idLoadCollection = new IDLoadCollection();

            foreach (PickResultId id in idList)
            {
                IDLoad idLoad = new IDLoad();

                idLoad.Project_Code = projectHeader.Code;
                idLoad.ID = id.Id;
                idLoad.FromID = id.OriginalPickID.Header.ID;
                idLoad.DoUpdate = "N";
                idLoadCollection.Add(idLoad);
            }
            return idLoadCollection;
        }

        internal static int AddPickProjectInfo(string projectName,
            string memoryType, int sectorSize, string manufacturerCode,
            string description)
        {
            using (new ConnectionSwitcher())
            {
                PickProjectDAO pickProjectDao = new PickProjectDAO();

                if (pickProjectDao.SelectHeader(projectName) != null)
                    throw new ArgumentException("Project already exists. Perhaps project name is incorrect");

                ProjectHeader projectHeader = new ProjectHeader();

                projectHeader.Name = projectName;
                projectHeader.Description = description;
                projectHeader.Manufacturer_Code = manufacturerCode;
                projectHeader.MemoryType = memoryType;
                projectHeader.SectorSize = sectorSize;

                try
                {
                    DAOFactory.BeginTransaction();
                    pickProjectDao.SaveHeaderOnly(projectHeader);
                    DAOFactory.CommitTransaction();

                    if (pickProjectDao.SelectHeader(projectName).Code != projectHeader.Code)
                        throw new ArgumentException("Inconsistent write. Please try again.");
                }
                catch
                {
                    DAOFactory.RollBackTransaction();
                    throw;
                }
                return projectHeader.Code;
            }
        }

        public static void AddRegularProjectLock(string projectName)
        {
            using (new ConnectionSwitcher())
            {
                PickProjectDAO pickProjectDao = new PickProjectDAO();
                ProjectHeader projectHeader = pickProjectDao.SelectHeader(projectName);

                if (projectHeader == null)
                    throw new DBPickNotFoundException();

                DAOFactory.BeginTransaction();
                pickProjectDao.InsertRegularProjectLock(projectHeader.Code);
                DAOFactory.CommitTransaction();
            }
        }
    }
}
