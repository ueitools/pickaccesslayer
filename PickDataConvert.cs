using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using PickData;
using PickAccessLayer.DataAccessObjects;
using BusinessObject;

namespace PickAccessLayer
{
    public class PickResultIdConverter
    {
        private PickIdDAO _pickId;

        public PickResultIdConverter()
        {
            _pickId = new PickIdDAO();
        }

        public PickResultIdConverter(PickResultId pickResultId)
        {
            PickId pickId = pickResultId.OriginalPickID;

            _pickId = new PickIdDAO();
            _pickId.FK_ID = pickResultId.Id;
            _pickId.Comment = pickId.Header.Text;
            _pickId.Exec = pickId.Header.ExecutorCode;
            _pickId.IsInverseData = PALUtil.ConvertToYesNoChar(pickId.Header.IsInversedData);
            _pickId.IsHexFormat = PALUtil.ConvertToYesNoChar(pickId.Header.IsHexFormat);
            _pickId.IsInversePrefix = PALUtil.ConvertToYesNoChar(pickId.Header.IsInversedPrefix);
            _pickId.IsExternalPrefix = PALUtil.ConvertToYesNoChar(pickId.Header.IsExternalPrefix);
            _pickId.IsFrequencyData = PALUtil.ConvertToYesNoChar(pickId.Header.IsFrequencyData);

            BindingListEx<PickPrefixDAO> pickPrefixArray= new BindingListEx<PickPrefixDAO>();
            for (int prefixNumber = 0; prefixNumber < pickId.Header.PrefixList.Count; prefixNumber++)
            {
                pickPrefixArray.Add(new PickPrefixDAO());
                pickPrefixArray[prefixNumber].Data = pickId.Header.PrefixList[prefixNumber].Data;
                pickPrefixArray[prefixNumber].Comment = pickId.Header.PrefixList[prefixNumber].Description;
            }
            _pickId.PrefixList = pickPrefixArray;

            BindingListEx<PickDataMapDAO> pickDataMapArray = new BindingListEx<PickDataMapDAO>();
            for (int keyNumber = 0; keyNumber < pickResultId.KeyList.Count; keyNumber++)
            {
                pickDataMapArray.Add(new PickDataMapDAO());
                if (!String.IsNullOrEmpty(pickResultId.KeyList[keyNumber].DeviceKey.FirmwareLabel))
                {
                    pickDataMapArray[keyNumber].Data = pickResultId.KeyList[keyNumber].Data;
                    pickDataMapArray[keyNumber].IdLabel = pickResultId.KeyList[keyNumber].Label;
                    pickDataMapArray[keyNumber].FirmwareLabel = pickResultId.KeyList[keyNumber].DeviceKey.FirmwareLabel;
                    pickDataMapArray[keyNumber].Comment = pickResultId.KeyList[keyNumber].Comment;
                    pickDataMapArray[keyNumber].Intron = pickResultId.KeyList[keyNumber].Intron;
                    pickDataMapArray[keyNumber].IntronPriority = pickResultId.KeyList[keyNumber].IntronPriority;
                    pickDataMapArray[keyNumber].KeyNumber = pickResultId.KeyList[keyNumber].DeviceKey.KeyIndex;
                }
            }
            _pickId.DataMap = pickDataMapArray;

            _pickId.Snapshot = new BindingListEx<PickIdSnapshotDAO>();
            _pickId.Snapshot.Add(new PickIdSnapshotDAO());
            _pickId.Snapshot[0].CreationDate = DateTime.Now;
            _pickId.Snapshot[0].OriginalId = PALUtil.ToXml(pickId);
            _pickId.Snapshot[0].ProjectCode = 0;
            _pickId.Snapshot[0].Rowguid = string.Empty;
        }

        public PickIdDAO PickId
        {
            get { return _pickId; }
            set { _pickId = value; }
        }

        public void ToXml(string fileName)
        {
            using (XmlTrimmer xmlWriter = new XmlTrimmer(this))
            {
                xmlWriter.ToXml(fileName);
            }
        }

        public static void ToXml(PickIdDAO pickId, string fileName)
        {
            PickResultIdConverter convert = new PickResultIdConverter();
            convert.PickId = pickId;
            using (XmlTrimmer xmlWriter = new XmlTrimmer(convert))
            {
                xmlWriter.ToXml(fileName);
            }
        }

        public static PickResultIdConverter FromXml(string path)
        {
            XmlSerializer x = new XmlSerializer(typeof(PickResultIdConverter));
            Stream s = File.OpenRead(path);
            PickResultIdConverter result = x.Deserialize(s) as PickResultIdConverter;
            s.Close();

            return result;
        }

        private class XmlTrimmer : IDisposable
        {
            private PickDevice.List _deviceList;
            private PickResultIdConverter _converter;

            public XmlTrimmer(PickResultIdConverter converter)
            {
                _converter = converter;
                Trim();
            }

            public void ToXml(string fileName)
            {
                XmlSerializer x = new XmlSerializer(_converter.GetType());
                Stream s = File.Create(fileName);
                x.Serialize(s, _converter);
                s.Close();
            }

            public void Dispose()
            {
                Restore();
            }

            // plan to remove device list from original snapshot to save space
            private void Trim()
            {
            }

            // plan to restore device list from original snapshot to preserve functionality
            private void Restore()
            {
            }
        }
    }
}
