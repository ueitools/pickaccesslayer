USE [Product2]
/****** Object:  StoredProcedure [dbo].[sp_InsertProjectLock]    Script Date: 12/09/2010 15:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProjectLock]
	@FK_ProjectCode int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FK_LockStatus int
	
	SELECT @FK_LockStatus=PK_ProjectLockStatus 
	FROM ProjectLockStatus 
	WHERE ProjectLockStatus.LockType = 'Lock'
	
	INSERT INTO ProjectLock
	(FK_LockStatus, FK_ProjectCode, Username)
	VALUES (@FK_LockStatus, @FK_ProjectCode, (SELECT SYSTEM_USER))
	
END
GO
