USE [Product2]
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertPickDataMap]    Script Date: 08/31/2010 15:13:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickDataMap]
	@FK_PickID int, 
	@Data varchar(50),
	@IDLabel nvarchar(255),
	@FirmwareLabel nvarchar(50),
	@Comment text,
	@Intron varchar(255),
	@IntronPriority int,
	@KeyNumber int,
	@InsertedRID int OUTPUT
AS
BEGIN
    INSERT INTO PickDataMap
    (FK_PickID, Data, IDLabel, FirmwareLabel, Comment, Intron, IntronPriority, KeyNumber)
    VALUES (@FK_PickID, @Data, @IDLabel, @FirmwareLabel, @Comment, @Intron, @IntronPriority, @KeyNumber);
    SELECT @InsertedRID = @@IDENTITY;
END


GO


