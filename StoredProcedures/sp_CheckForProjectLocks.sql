USE [WorkingProduct2]
/****** Object:  StoredProcedure [dbo].[sp_CheckForProjectLocks]    Script Date: 12/09/2010 15:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckForProjectLocks]
	@FK_ProjectCode int,
	@NumLocks int OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @NumLocks=COUNT(*)
	FROM ProjectLockStatusView v
	WHERE v.ProjectCode = @FK_ProjectCode AND
	(v.LockType = 'Lock' 
	OR v.LockType = 'Published')
	
END
GO