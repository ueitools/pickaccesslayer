USE [Product2]
/****** Object:  StoredProcedure [dbo].[sp_PurgeRegularProjectLocks]    Script Date: 12/09/2010 15:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgeRegularProjectLocks]
	@FK_ProjectCode int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE 
	FROM ProjectLock 
	WHERE ProjectLock.Username = (SELECT SYSTEM_USER)
	AND ProjectLock.FK_ProjectCode = FK_ProjectCode	
	AND ProjectLock.FK_LockStatus = 
		(SELECT PK_ProjectLockStatus 
		FROM ProjectLockStatus 
		WHERE ProjectLockStatus.LockType = 'Lock');
		
END
GO