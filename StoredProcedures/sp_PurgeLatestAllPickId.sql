USE [PickTest]
GO

/****** Object:  StoredProcedure [dbo].[sp_PurgeLatestAllPickId]    Script Date: 09/02/2010 16:02:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgeLatestAllPickId]
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT
AS
BEGIN
  DELETE p1
  FROM [PickTest].[dbo].[PickID] as p1 
  WHERE [Version] = (SELECT MAX([Version])
  FROM [PickTest].[dbo].[PickID] p2
  WHERE p1.FK_ID = p2.FK_ID AND p1.FK_Project_Code = p2.FK_Project_Code);
  
  SELECT @NumRowsChanged = @@ROWCOUNT, @ErrorCode = @@ERROR
END

GO


