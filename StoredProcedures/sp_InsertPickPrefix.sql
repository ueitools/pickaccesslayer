USE [Product2]
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertPickPrefix]    Script Date: 08/31/2010 15:14:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickPrefix]
	@FK_PickID int, 
	@Data varchar(50),
	@Comment text,
	@Order int,
	@InsertedRID int OUTPUT
AS
BEGIN
    INSERT INTO PickPrefix
    (FK_PickID, Data, Comment, [Order])
    VALUES (@FK_PickID, @Data, @Comment, @Order);
    SELECT @InsertedRID = @@IDENTITY;
END


GO


