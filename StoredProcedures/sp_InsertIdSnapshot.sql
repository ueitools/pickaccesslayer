USE [Product2]
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertIdSnapshot]    Script Date: 08/31/2010 15:12:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertIdSnapshot]
	@FK_PickID int, 
	@OriginalID xml,
	@InsertedRID int OUTPUT
AS
BEGIN
    INSERT INTO OriginalIDSnapshot
    (FK_PickID, OriginalID)
    VALUES (@FK_PickID, @OriginalID);
    SELECT @InsertedRID = @@IDENTITY;
END


GO


