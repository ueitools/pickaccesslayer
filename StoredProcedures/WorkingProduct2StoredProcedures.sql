USE [WorkingProduct2]
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgeLatestPickId]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickIdWithVersion]
	@FK_ProjectCode int,
	@FK_ID char(5),
	@Version int,
	@UserName nvarchar(50),
	@Comment text,
	@Exec int,
	@IsInverseData char(1),
	@IsHexFormat char(1),
	@IsInternalPick char(1),
	@IsTemporary char(1),
	@IsInversePrefix char(1),
	@IsExternalPrefix char(1),		
	@FK_IDSnapshot int, 
	@InsertedRID int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @OutputRID table ( RID int );

    INSERT INTO PickID2
    (FK_Project_Code,FK_ID,[Version],UserName,Comment,[Exec],IsInverseData,IsHexFormat,IsInternalPick,IsTemporary, IsInversePrefix, IsExternalPrefix,FK_IDSnapshot)
    OUTPUT INSERTED.PK_PickID as RID INTO @OutputRID    
    VALUES (@FK_ProjectCode, @FK_ID, @Version, @UserName, @Comment, @Exec, @IsInverseData, @IsHexFormat, @IsInternalPick, @IsTemporary, @IsInversePrefix, @IsExternalPrefix, @FK_IDSnapshot);
    SELECT @InsertedRID=RID FROM @OutputRID;
END

GO

CREATE PROCEDURE [dbo].[sp_PurgeLatestPickId]
	@Id char(5),
	@Project_Code int,
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT	
AS
BEGIN
  DELETE FROM [PickTest].[dbo].[PickID2] 
  WHERE FK_ID = @Id AND FK_Project_Code= @Project_Code AND 
  [Version] = (SELECT MAX([Version]) 
  FROM [PickTest].[dbo].[PickID2] p2
  WHERE @Id = p2.FK_ID AND @Project_Code = p2.FK_Project_Code);
  
  SELECT @NumRowsChanged=@@ROWCOUNT, @ErrorCode=@@ERROR
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgeLatestAllPickId]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgeLatestAllPickId]
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT
AS
BEGIN
  DELETE p1
  FROM [PickTest].[dbo].[PickID2] as p1 
  WHERE [Version] = (SELECT MAX([Version])
  FROM [PickTest].[dbo].[PickID2] p2
  WHERE p1.FK_ID = p2.FK_ID AND p1.FK_Project_Code = p2.FK_Project_Code);
  
  SELECT @NumRowsChanged = @@ROWCOUNT, @ErrorCode = @@ERROR
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertIdSnapshot]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertIdSnapshot]
	@OriginalID xml,
	@InsertedRID int OUTPUT
AS
BEGIN
	DECLARE @OutputRID table ( RID int );
    INSERT INTO OriginalIDSnapshot
    (OriginalID)
	OUTPUT INSERTED.PK_OriginalIDSnapshot as RID INTO @OutputRID
    VALUES (@OriginalID);
    SELECT @InsertedRID=RID FROM @OutputRID;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteExceptLastN]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteExceptLastN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ReportOutput TABLE
	(
		ID char(5),
		[Version] int
	);
	
	DELETE FROM PICKID2 
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	INTO @ReportOutput			
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND [Version] NOT IN 
	(SELECT TOP (@N) [Version] 
		FROM pickID2 p2 
		WHERE @ID = p2.FK_ID AND FK_Project_Code = @Project_Code 
		ORDER BY [Version] DESC);		
	
	SELECT ID, [Version] FROM @ReportOutput;	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteExceptFirstN]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteExceptFirstN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ReportOutput TABLE
	(
		ID char(5),
		[Version] int
	);
	
	DELETE 
	FROM PICKID2 
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	INTO @ReportOutput		
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND [Version] NOT IN 
		(SELECT TOP (@N) [Version] 
		FROM pickID2 p2 
		WHERE @ID = p2.FK_ID AND FK_Project_Code = @Project_Code ORDER BY [Version])
	
	SELECT ID, [Version] FROM @ReportOutput;		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteExceptFirstAndLast]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteExceptFirstAndLast]
@Project_Code int,
@ID char(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ReportOutput TABLE
	(
		ID char(5),
		[Version] int
	);
	
	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	INTO @ReportOutput	
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND [Version] < 
	(SELECT MAX([Version]) FROM PICKID2 WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID) AND
	[Version] > (SELECT MIN([Version]) FROM PICKID2 WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID);

	SELECT ID, [Version] FROM @ReportOutput;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteBeforeN]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteBeforeN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ReportOutput TABLE
	(
		ID char(5),
		[Version] int
	);

	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	INTO @ReportOutput	
	WHERE FK_Project_Code = @Project_Code AND [Version] < @N AND FK_ID = @ID;
	
	SELECT ID, [Version] FROM @ReportOutput;	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteBeforeDate]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteBeforeDate]
					@Project_Code int,
					@ID char(5),
					@BeforeTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ReportOutput TABLE
	(
		ID char(5),
		[Version] int
	);

	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	INTO @ReportOutput
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND 
		[CreationDate] < @BeforeTime;
	
	SELECT ID, [Version] FROM @ReportOutput;	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAfterN]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteAfterN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ReportOutput TABLE
	(
		ID char(5),
		[Version] int
	);
	
	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version] INTO @ReportOutput
	WHERE FK_Project_Code = @Project_Code AND [Version] > @N AND FK_ID = @ID;
	
	SELECT ID, [Version] FROM @ReportOutput;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAfterDate]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteAfterDate]
					@Project_Code int,
					@ID char(5),
					@AfterTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ReportOutput TABLE
	(
		FK_ID char(5),
		[Version] int
	);

	DELETE FROM PICKID2	
	OUTPUT DELETED.FK_ID, DELETED.[Version] INTO @ReportOutput
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID 
	AND [CreationDate] > @AfterTime;
	
	SELECT FK_ID, [Version] FROM @ReportOutput;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgeRegularProjectLocks]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgeRegularProjectLocks]
	@FK_ProjectCode int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE 
	FROM ProjectLock 
	WHERE ProjectLock.Username = (SELECT SYSTEM_USER)
	AND ProjectLock.FK_ProjectCode = FK_ProjectCode	
	AND ProjectLock.FK_LockStatus = 
		(SELECT PK_ProjectLockStatus 
		FROM ProjectLockStatus 
		WHERE ProjectLockStatus.LockType = 'Lock');
		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgePickIdVersion]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgePickIdVersion]
	@Project_Code int,
	@ID char(5),
	@Version int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ReportOutput TABLE
	(
		FK_ID char(5),
		[Version] int
	);

	DELETE FROM [PickID2] 
	OUTPUT DELETED.FK_ID, DELETED.[Version] INTO @ReportOutput
	WHERE FK_ID = @ID AND FK_Project_Code= @Project_Code AND 
	[Version] = @Version;
  
	SELECT FK_ID, [Version] FROM @ReportOutput;  
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertPickId]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickId]
	@FK_ProjectCode int,
	@FK_ID char(5),
	@UserName nvarchar(50),
	@Comment text,
	@Exec int,
	@IsInverseData char(1),
	@IsHexFormat char(1),
	@IsInternalPick char(1),
	@IsTemporary char(1),
	@IsInversePrefix char(1),
	@IsExternalPrefix char(1),		
	@FK_IDSnapshot int, 
	@InsertedRID int OUTPUT
AS
BEGIN
	DECLARE @OutputRID table ( RID int );
	DECLARE @Version int;	
	SELECT @Version=ISNULL(MAX([Version])+1,0) FROM PickID2 
	WHERE FK_Project_Code = @FK_ProjectCode AND FK_ID = @FK_ID;	
    INSERT INTO PickID2
    (FK_Project_Code,FK_ID,[Version],UserName,Comment,[Exec],IsInverseData,IsHexFormat,IsInternalPick,IsTemporary, IsInversePrefix, IsExternalPrefix,FK_IDSnapshot)
    OUTPUT INSERTED.PK_PickID as RID INTO @OutputRID    
    VALUES (@FK_ProjectCode, @FK_ID, @Version, @UserName, @Comment, @Exec, @IsInverseData, @IsHexFormat, @IsInternalPick, @IsTemporary, @IsInversePrefix, @IsExternalPrefix, @FK_IDSnapshot);
    SELECT @InsertedRID=RID FROM @OutputRID;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProjectLock]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertProjectLock]
	@FK_ProjectCode int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FK_LockStatus int
	
	SELECT @FK_LockStatus=PK_ProjectLockStatus 
	FROM ProjectLockStatus 
	WHERE ProjectLockStatus.LockType = 'Lock'
	
	INSERT INTO ProjectLock
	(FK_LockStatus, FK_ProjectCode, Username)
	VALUES (@FK_LockStatus, @FK_ProjectCode, (SELECT SYSTEM_USER))
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertPickPrefix]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickPrefix]
	@FK_PickID int, 
	@Data varchar(50),
	@Comment text,
	@Order int,
	@InsertedRID int OUTPUT
AS
BEGIN
	DECLARE @OutputRID table ( RID int );
    INSERT INTO PickPrefix2
    (FK_PickID, Data, Comment, [Order])
    OUTPUT INSERTED.PK_PickPrefix as RID INTO @OutputRID
    VALUES (@FK_PickID, @Data, @Comment, @Order);
    SELECT @InsertedRID=RID FROM @OutputRID;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertPickDataMap]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickDataMap]
	@FK_PickID int, 
	@Data varchar(50),
	@IDLabel nvarchar(255),
	@FirmwareLabel nvarchar(50),
	@Comment text,
	@Intron varchar(255),
	@IntronPriority varchar(3),
	@KeyNumber int,
	@InsertedRID int OUTPUT
AS
BEGIN
	DECLARE @OutputRID table ( RID int );
    INSERT INTO PickDataMap2
    (FK_PickID, Data, IDLabel, FirmwareLabel, Comment, Intron, IntronPriority, KeyNumber)
	OUTPUT INSERTED.PK_PickDataMap INTO @OutputRID    
    VALUES (@FK_PickID, @Data, @IDLabel, @FirmwareLabel, @Comment, @Intron, @IntronPriority, @KeyNumber);
    SELECT @InsertedRID=RID FROM @OutputRID;
END
GO
/****** Object:  StoredProcedure [dbo].[Product_Sp_GetIDwithLabelData]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Product_Sp_GetIDwithLabelData] (
				 @nvcharFirmwareLabel   nvarchar(1000),
				 @nvcharInputData		nvarchar(1000),
				 @vcharProjectName		varchar(100),
				 @nKeyCount				int,
				 @nvcharIDs			    nvarchar(max)=NULL,
				 @charIsInverseData		nvarchar(1),
		 		 @Version				varchar='1.0.2', 
				 @xmlParameter			nvarchar='')
				 
--WITH ENCRYPTION

AS
BEGIN

	SET NOCOUNT ON

/*-- Create Local Temporary Table(s) ---------------------------------------*/
	
		--Temporary Table to Parse the Input IDs
		
		IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
			WHERE (NAME LIKE N'#TempIDList') 
			AND (TYPE = 'U'))
		DROP TABLE [#TempIDList];
	
		CREATE TABLE [#TempIDList]
		(
			RID int  IDENTITY(1,1) NOT NULL,
			InputID char(5)
			PRIMARY KEY (RID,InputID) 
		)

		--Temporary Table to Parse the Input Keys
		IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
			WHERE (NAME LIKE N'#TempKeyList') 
			AND (TYPE = 'U'))
		DROP TABLE [#TempKeyList];
				
		CREATE TABLE [#TempKeyList]
		(
			RID int  IDENTITY(1,1) NOT NULL,
			InputKey char(50)
			PRIMARY KEY (RID,InputKey) 
		)

		--Temporary Table to Parse the Input Data
		IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
			WHERE (NAME LIKE N'#TempDataList') 
			AND (TYPE = 'U'))
		DROP TABLE [#TempDataList];
				
		CREATE TABLE [#TempDataList]
		(
			RID int  IDENTITY(1,1) NOT NULL,
			Inputdata varchar(100)
			PRIMARY KEY (RID,Inputdata)  
		)
		
		--Temporary Table InputKey and Data 
		IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
			WHERE (NAME LIKE N'#TempData') 
			AND (TYPE = 'U'))
		DROP TABLE [#TempData];		
		
		CREATE TABLE [#TempData]
		(
			RID int  IDENTITY(1,1) NOT NULL,
			InputKey char(50),
			Inputdata varchar(100)
			PRIMARY KEY (RID,InputKey,Inputdata) 
		)		

		--Temporary Table for IDs with Max Version 
		IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
			WHERE (NAME LIKE N'#IDwithVersion') 
			AND (TYPE = 'U'))
		DROP TABLE [#IDwithVersion];	
				
		CREATE TABLE [#IDwithVersion]
		(
			RID int  IDENTITY(1,1) NOT NULL,
			Project_Code int,
			Project_Name varchar(100),
			ID			 char(5),
			MVersion	 int
			PRIMARY KEY(RID,Project_Code)
		)		

		--Temporary Table to store the output
		IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
			WHERE (NAME LIKE N'#TempOutput') 
			AND (TYPE = 'U'))
		DROP TABLE [#TempOutput];		
		
		CREATE TABLE [#TempOutput]
		(
			RID		int  IDENTITY(1,1) NOT NULL,
			IDList  char(5)
			PRIMARY KEY (RID,IDList) 
		)
				
/*-- Populate Temporary Table(s) --------------------------------------------*/
		INSERT [#TempKeyList]
		SELECT STR AS InputKey FROM iter_charlist_to_table(@nvcharFirmwareLabel,',')
		
		INSERT [#TempDataList]
		SELECT STR AS Inputdata FROM iter_charlist_to_table(@nvcharInputData,',')
		
		INSERT [#TempIDList]
		SELECT STR AS InputID FROM iter_charlist_to_table(@nvcharIDs,',')
		
		INSERT [#TempData]
		SELECT InputKey,#TempDataList.Inputdata
		FROM #TempKeyList
		Inner join #TempDataList ON #TempDataList.RID=#TempKeyList.RID
		
		--IDs with Max Version for the input project name
		INSERT [#IDwithVersion]
		SELECT PickID2.FK_Project_Code AS Project_Code, 
			   project.Name AS Project_Name, 
			   PickID2.FK_ID AS ID, 
			   MAX(PickID2.Version) AS MVersion
		FROM PickID2 INNER JOIN Project 
		ON PickID2.FK_Project_Code = project.Code
		WHERE project.Name = @vcharProjectName
		GROUP BY PickID2.FK_Project_Code,project.Name,PickID2.FK_ID 		

/*-- Generate Output -------------------------------------------------------*/

		--IF IDs(Input) is not specified, all IDs related to the Project to be considered 
		IF ((@nvcharIDs IS NULL) OR (@nvcharIDs = ''))

		BEGIN
			
			 SELECT IDList FROM
			 (SELECT FK_ID AS IDList,COUNT(FK_ID)AS IDCount FROM 
			 (SELECT PickID2.FK_ID, PickID2.[Exec], PickDataMap2.IDLabel, PickDataMap2.FirmwareLabel, 
					 PickDataMap2.Data,project.Name,Version
			 FROM PickDataMap2 INNER JOIN #TempData
			 ON PickDataMap2.FirmwareLabel=#TempData.InputKey
			 AND PickDataMap2.Data=#TempData.Inputdata INNER JOIN pickID2
			 ON PickDataMap2.FK_PickID = PickID2.PK_PickID INNER JOIN project 
			 ON PickID2.FK_Project_Code = project.Code INNER JOIN #IDwithVersion 
			 ON PickID2.FK_ID=#IDwithVersion.ID AND PickID2.Version=#IDwithVersion.MVersion
			 WHERE project.Name = @vcharProjectName AND PickID2.IsInverseData=@charIsInverseData)ID_Data
			 GROUP BY FK_ID
			 HAVING COUNT(FK_ID)=@nKeyCount)IDs
			 ORDER BY IDList
		
		END
		
		--IF IDs(Input) is specified, only Input IDs related to the Project to be considered 
		ELSE IF ((@nvcharIDs IS NOT NULL) OR (@nvcharIDs != ''))
		
		BEGIN
				
			SELECT IDList FROM
			(SELECT FK_ID AS IDList,COUNT(FK_ID)AS IDCount FROM 
			(SELECT PickID2.FK_ID, PickID2.[Exec], PickDataMap2.IDLabel, PickDataMap2.FirmwareLabel, 
				    PickDataMap2.Data,project.Name,Version
			FROM PickDataMap2 INNER JOIN #TempData
			ON PickDataMap2.FirmwareLabel=#TempData.InputKey
			AND PickDataMap2.Data=#TempData.Inputdata INNER JOIN pickID2
			ON PickDataMap2.FK_PickID = PickID2.PK_PickID INNER JOIN project 
			ON PickID2.FK_Project_Code = project.Code INNER JOIN #TempIDList 
			ON #TempIDList.InputID=PickID2.FK_ID INNER JOIN #IDwithVersion 
			ON PickID2.FK_ID=#IDwithVersion.ID AND PickID2.Version=#IDwithVersion.MVersion
			WHERE project.Name = @vcharProjectName AND PickID2.IsInverseData=@charIsInverseData)ID_Data
			GROUP BY FK_ID
			HAVING COUNT(FK_ID)=@nKeyCount)IDs
			ORDER BY IDList		
		
		END
		
/*-- Drop Temporary tables -------------------------------------------------------*/
		
		DROP TABLE [#TempDataList]
		DROP TABLE [#TempKeyList]
		DROP TABLE [#TempData]
		DROP TABLE [#TempIDList]
		DROP TABLE [#IDwithVersion]
		
END

GO

/****** Object:  StoredProcedure [dbo].[sp_CountPickedFunctions]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CountPickedFunctions]
	@ProjectCode int, 
	@ModeString nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT FirmwareLabel, COUNT(*) as OptomizedOrder
	FROM [PickID2] pid2
	INNER JOIN PickDataMap2 pdm ON pid2.PK_PickID = pdm.FK_PickID
	WHERE FK_Project_Code = @ProjectCode 
		AND FK_ID LIKE ('[' + @ModeString + ']' + '%') 
		AND Data IS NOT NULL AND Data <> ''
		AND pid2.[Version] = (
			SELECT MAX([VERSION]) FROM [PickID2] innerPid2
			WHERE innerPid2.FK_Project_Code = @ProjectCode 
			AND innerPid2.FK_ID = pid2.FK_ID)
	GROUP BY FirmwareLabel
	ORDER BY COUNT(*) DESC, FirmwareLabel		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckForProjectLocks]    Script Date: 01/13/2011 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CheckForProjectLocks]
	@FK_ProjectCode int,
	@NumLocks int OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @NumLocks=COUNT(*)
	FROM ProjectLockStatusView v
	WHERE v.ProjectCode = @FK_ProjectCode AND
	(v.LockType = 'Lock' 
	OR v.LockType = 'Published')
	
END
GO
