USE [WorkingProduct2]
GO

/****** Object:  StoredProcedure [dbo].[sp_CountPickedFunctions]    Script Date: 11/15/2010 16:46:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CountPickedFunctions]
	@ProjectCode int, 
	@ModeString nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT FirmwareLabel, COUNT(*) as OptomizedOrder
	FROM [PickID2] pid2
	INNER JOIN PickDataMap2 pdm ON pid2.PK_PickID = pdm.FK_PickID
	WHERE FK_Project_Code = @ProjectCode 
		AND FK_ID LIKE ('[' + @ModeString + ']' + '%') 
		AND Data IS NOT NULL AND Data <> ''
		AND pid2.[Version] = (
			SELECT MAX([VERSION]) FROM [PickID2] innerPid2
			WHERE innerPid2.FK_Project_Code = @ProjectCode 
			AND innerPid2.FK_ID = pid2.FK_ID)
	GROUP BY FirmwareLabel
	ORDER BY COUNT(*) DESC, FirmwareLabel		
END

GO


