USE [PickTest]
GO

/****** Object:  StoredProcedure [dbo].[sp_PurgePickIdVersion]    Script Date: 09/02/2010 16:10:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgePickIdVersion]
	@Id char(5),
	@Project_Code int,
	@Version int, 
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT	
AS
BEGIN
  DELETE FROM [PickTest].[dbo].[PickID] 
  WHERE FK_ID = @Id AND FK_Project_Code= @Project_Code AND 
  [Version] = @Version;
  
  SELECT @NumRowsChanged=@@ROWCOUNT, @ErrorCode=@@ERROR
END


GO


