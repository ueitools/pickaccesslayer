USE [PickTest]
GO

/****** Object:  StoredProcedure [dbo].[sp_PurgeLatestPickId]    Script Date: 09/02/2010 16:03:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgeLatestPickId]
	@Id char(5),
	@Project_Code int,
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT	
AS
BEGIN
  DELETE FROM [PickTest].[dbo].[PickID] 
  WHERE FK_ID = @Id AND FK_Project_Code= @Project_Code AND 
  [Version] = (SELECT MAX([Version]) 
  FROM [PickTest].[dbo].[PickID] p2
  WHERE @Id = p2.FK_ID AND @Project_Code = p2.FK_Project_Code);
  
  SELECT @NumRowsChanged=@@ROWCOUNT, @ErrorCode=@@ERROR
END

GO


