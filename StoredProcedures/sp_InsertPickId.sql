USE [Product2]
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertPickId]    Script Date: 08/31/2010 15:14:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickId]
	@FK_ProjectCode int,
	@FK_ID char(5),
	@UserName nvarchar(50),
	@Comment text,
	@Exec int,
	@FromID char(5),
	@IsInverseData char(1),
	@IsHexFormat char(1),
	@IsInternalPick char(1),
	@IsTemporary char(1),
	@InsertedRID int OUTPUT
AS
BEGIN
	DECLARE @Version int;	
	SELECT @Version=ISNULL(MAX([Version])+1,0) FROM PickID 
	WHERE FK_Project_Code = @FK_ProjectCode AND FK_ID = @FK_ID;	
    INSERT INTO PickID
    (FK_Project_Code,FK_ID,[Version],UserName,Comment,[Exec],FromID,IsInverseData,IsHexFormat,IsInternalPick,IsTemporary)
    VALUES (@FK_ProjectCode, @FK_ID, @Version, @UserName, @Comment, @Exec, @FromID, @IsInverseData, @IsHexFormat, @IsInternalPick, @IsTemporary);
    SELECT @InsertedRID=@@Identity;
END
GO


