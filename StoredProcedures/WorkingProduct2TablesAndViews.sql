USE [WorkingProduct2]
GO
/****** Object:  Table [dbo].[PunchThroughTemporary]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PunchThroughTemporary](
	[Label] [varchar](50) NOT NULL,
	[RID] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReportType]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportType](
	[PK_ReportType] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[reportType] [char](50) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_ReportType] PRIMARY KEY CLUSTERED 
(
	[PK_ReportType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[manufacturer]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[manufacturer](
	[Code] [char](1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_manufacturer] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[keyType_ref]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keyType_ref](
	[FK_KeyTypeCode] [int] NOT NULL,
	[SRIKeyGroupName] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[keyType]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keyType](
	[Code] [int] NOT NULL,
	[Name] [varchar](255) NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_keyType] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[chip]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chip](
	[Code] [int] NOT NULL,
	[Number] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_chip] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDSnapshot]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OriginalIDSnapshot](
	[OriginalID] [xml] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_OriginalIDSnapshot] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_OriginalIDSnapshot] PRIMARY KEY CLUSTERED 
(
	[PK_OriginalIDSnapshot] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PickID]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickID](
	[ID] [char](5) NOT NULL,
	[Version] [int] NOT NULL,
	[Time] [datetime] NULL,
	[Name] [char](10) NULL,
	[Comment] [text] NULL,
	[Project_Code] [int] NULL,
	[Exec] [int] NOT NULL,
	[isinverseddata] [char](1) NULL,
	[isexternalprefix] [char](1) NULL,
	[isfrequencydata] [char](1) NULL,
	[isinversedprefix] [char](1) NULL,
	[ishexformat] [char](1) NULL,
	[pickFromID] [char](10) NULL,
	[PickRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[isinternalpick] [char](1) NULL,
	[istemporary] [char](1) NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickID] PRIMARY KEY CLUSTERED 
(
	[PickRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProjectLockStatus]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectLockStatus](
	[PK_ProjectLockStatus] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LockType] [varchar](50) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_ProjectLockStatus] PRIMARY KEY CLUSTERED 
(
	[PK_ProjectLockStatus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[project]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[project](
	[Code] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[MemoryType] [varchar](255) NOT NULL,
	[SectorSize] [int] NOT NULL,
	[Manufacturer_Code] [char](1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_project] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[product]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[product](
	[URC] [int] NULL,
	[Revision] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[Chip_Code] [int] NOT NULL,
	[ReplicationID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[ReplicationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [IX_product] UNIQUE NONCLUSTERED 
(
	[URC] ASC,
	[Revision] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'product', @level2type=N'COLUMN',@level2name=N'Revision'
GO
/****** Object:  Table [dbo].[PickPrefix]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickPrefix](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Data] [varchar](255) NULL,
	[Comment] [text] NULL,
	[Order] [int] NULL,
	[PickRID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickPrefix] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDLog]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDLog](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[tn] [int] NULL,
	[time] [datetime] NULL,
	[operator] [varchar](255) NULL,
	[content] [char](255) NULL,
	[PickRID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_OriginalIDLog] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDFunction]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDFunction](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[synth] [char](10) NULL,
	[Data] [varchar](255) NULL,
	[Label] [varchar](255) NULL,
	[Intron] [varchar](255) NULL,
	[IntronPriority] [char](10) NULL,
	[Comment] [varchar](255) NULL,
	[PickRID] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_OriginalIDFunction] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDDevice]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDDevice](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[tn] [int] NULL,
	[branch] [char](2) NULL,
	[ispartnumber] [char](1) NULL,
	[isretail] [char](1) NULL,
	[PickRID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_OriginalIDDevice] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMap]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMap](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Data] [varchar](255) NULL,
	[Synth] [char](10) NULL,
	[Outron] [char](10) NULL,
	[KeyLabel] [varchar](255) NULL,
	[OutronGroup] [int] NULL,
	[Intron] [varchar](255) NULL,
	[IntronPriority] [int] NULL,
	[Position] [int] NULL,
	[Comment] [text] NULL,
	[PickRID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickDataMap] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[key]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[key](
	[RID] [int] NOT NULL,
	[Project_Code] [int] NOT NULL,
	[Position] [int] NULL,
	[Label] [varchar](255) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Scan_Code] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_key] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickConfig]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PickConfig](
	[FK_ProjectCode] [int] NOT NULL,
	[ConfigFile] [xml] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_PickConfig] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickConfig] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickConfig] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[idLoad]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[idLoad](
	[Project_Code] [int] NOT NULL,
	[ID] [char](5) NOT NULL,
	[DoUpdate] [char](1) NOT NULL,
	[FromID] [char](5) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_idLoad] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[goldenImage]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goldenImage](
	[Project_Code] [int] NOT NULL,
	[Image] [image] NOT NULL,
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Date] [datetime] NULL,
	[Description] [varchar](255) NULL,
	[Attribute] [varchar](63) NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_goldenImage] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[firmware]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[firmware](
	[Project_Code] [int] NOT NULL,
	[MaxExecutorSize] [int] NOT NULL,
	[IsCheckSum] [char](1) NOT NULL,
	[IsTwoByteExecutorCode] [char](1) NOT NULL,
	[IncludePath] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_firmware_1] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[executorLoad]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[executorLoad](
	[Project_Code] [int] NOT NULL,
	[Executor_Code] [int] NOT NULL,
	[DoUpdate] [char](1) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_ExecutorLoad] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Executor_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[digitGroup]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[digitGroup](
	[Project_Code] [int] NOT NULL,
	[Code] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Data] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_DigitGroup] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Code] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AlternateCode]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AlternateCode](
	[Project_Code] [int] NOT NULL,
	[ID] [char](5) NOT NULL,
	[ExternalID] [char](5) NOT NULL,
	[NumCriticalOutron] [int] NOT NULL,
	[MatchCount] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[address]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address](
	[Project_Code] [int] NOT NULL,
	[Version] [varchar](255) NOT NULL,
	[Type] [int] NOT NULL,
	[VersionAddress] [varchar](255) NOT NULL,
	[BeginAddress] [varchar](255) NOT NULL,
	[EndAddress] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_firmware] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Version] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Library ''L'', Software ''S''' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'address', @level2type=N'COLUMN',@level2name=N'Type'
GO
/****** Object:  Table [dbo].[config]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[config](
	[Project_Code] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Value] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProjectLock]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectLock](
	[PK_ProjectLock] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Username] [varchar](128) NULL,
	[DomainName] [varchar](50) NULL,
	[TimeStamp] [datetime] NULL,
	[FK_LockStatus] [int] NOT NULL,
	[FK_ProjectCode] [int] NOT NULL,
	[Comment] [nvarchar](255) NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_ProjectLock] PRIMARY KEY CLUSTERED 
(
	[PK_ProjectLock] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[project_chip]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[project_chip](
	[Project_Code] [int] NOT NULL,
	[Chip_Code] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_project_chip] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Chip_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PickID2]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickID2](
	[FK_Project_Code] [int] NOT NULL,
	[FK_ID] [char](5) NOT NULL,
	[Version] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Comment] [text] NULL,
	[Exec] [int] NOT NULL,
	[IsInverseData] [char](1) NOT NULL,
	[IsHexFormat] [char](1) NOT NULL,
	[IsInternalPick] [char](1) NOT NULL,
	[IsTemporary] [char](1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_PickID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SystemFlags] [int] NULL,
	[IsInversePrefix] [char](1) NULL,
	[IsExternalPrefix] [char](1) NULL,
	[FK_IDSnapshot] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickID2_1] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMapPDLIntron]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMapPDLIntron](
	[IntronRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Intron] [varchar](255) NULL,
	[pickDataMap_RID] [int] NOT NULL,
	[Order] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickDataMapPDLIntron] PRIMARY KEY NONCLUSTERED 
(
	[IntronRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMapLabel]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMapLabel](
	[LabelRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Label] [varchar](255) NULL,
	[PickDataMap_RID] [int] NOT NULL,
	[Order] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickDataMapLabel] PRIMARY KEY NONCLUSTERED 
(
	[LabelRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Report]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report](
	[PK_Report] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FK_Project_Code] [int] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Report] [text] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[FK_ReportType] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[PK_Report] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OriginalIDTNCaptureList]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDTNCaptureList](
	[CaptureRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Label] [varchar](255) NULL,
	[TN] [int] NULL,
	[Filename] [varchar](255) NULL,
	[LegacyLabel] [varchar](255) NULL,
	[FunctionRID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_OriginalIDTNCaptureList] PRIMARY KEY NONCLUSTERED 
(
	[CaptureRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ProjectLockStatusView]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ProjectLockStatusView]
AS
SELECT     dbo.ProjectLock.Username, dbo.ProjectLock.DomainName, dbo.ProjectLock.TimeStamp, dbo.ProjectLockStatus.LockType, 
                      dbo.project.Name AS ProjectName, dbo.project.Code AS ProjectCode
FROM         dbo.ProjectLockStatus INNER JOIN
                      dbo.ProjectLock ON dbo.ProjectLockStatus.PK_ProjectLockStatus = dbo.ProjectLock.FK_LockStatus INNER JOIN
                      dbo.project ON dbo.ProjectLock.FK_ProjectCode = dbo.project.Code
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProjectLockStatus"
            Begin Extent = 
               Top = 70
               Left = 279
               Bottom = 155
               Right = 464
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProjectLock"
            Begin Extent = 
               Top = 9
               Left = 14
               Bottom = 164
               Right = 172
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "project"
            Begin Extent = 
               Top = 172
               Left = 202
               Bottom = 287
               Right = 379
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProjectLockStatusView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ProjectLockStatusView'
GO
/****** Object:  Table [dbo].[keyGroup]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[keyGroup](
	[Device_RID] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Key_RID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_keyGroup] PRIMARY KEY CLUSTERED 
(
	[Device_RID] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PickDataMap2]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMap2](
	[Data] [varchar](50) NOT NULL,
	[IDLabel] [varchar](50) NOT NULL,
	[FirmwareLabel] [varchar](50) NOT NULL,
	[Comment] [text] NULL,
	[Intron] [nchar](10) NOT NULL,
	[IntronPriority] [varchar](3) NOT NULL,
	[KeyNumber] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_PickDataMap] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FK_PickID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickDataMap2] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickDataMap] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickPrefix2]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickPrefix2](
	[Data] [varchar](50) NOT NULL,
	[Order] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Comment] [text] NULL,
	[PK_PickPrefix] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FK_PickID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_PickPrefix2_1] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickPrefix] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[device]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[device](
	[Key_RID] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[ModeList] [varchar](255) NOT NULL,
	[DefaultID] [char](5) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_deviceDescription] PRIMARY KEY CLUSTERED 
(
	[Key_RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[chipNumber]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[chipNumber]
AS
SELECT     dbo.project_chip.Project_Code, dbo.project.Name AS ProjectName, dbo.chip.Number
FROM         dbo.project_chip INNER JOIN
                      dbo.chip ON dbo.project_chip.Chip_Code = dbo.chip.Code INNER JOIN
                      dbo.project ON dbo.project_chip.Project_Code = dbo.project.Code
GO
/****** Object:  Table [dbo].[keyProperty]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[keyProperty](
	[Type_Code] [int] NOT NULL,
	[Key_RID] [int] NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_keyProperty] PRIMARY KEY CLUSTERED 
(
	[Type_Code] ASC,
	[Key_RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[keyOutron]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keyOutron](
	[RID] [int] NOT NULL,
	[Key_RID] [int] NULL,
	[Mode] [char](1) NOT NULL,
	[Outron] [char](3) NOT NULL,
	[Group] [int] NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_outronGroup] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[keyInfo]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[keyInfo]
AS
SELECT     dbo.[key].RID, dbo.project.Name AS ProjectName, dbo.[key].[Position], dbo.[key].Label, dbo.[key].Description, dbo.device.[Order] AS DeviceNumber, 
                      dbo.device.ModeList, dbo.device.DefaultID, dbo.[key].Scan_Code, dbo.keyProperty.Type_Code, dbo.keyType.Name AS Type_Name
FROM         dbo.project INNER JOIN
                      dbo.[key] ON dbo.project.Code = dbo.[key].Project_Code LEFT OUTER JOIN
                      dbo.device ON dbo.[key].RID = dbo.device.Key_RID LEFT OUTER JOIN
                      dbo.keyProperty ON dbo.[key].RID = dbo.keyProperty.Key_RID LEFT OUTER JOIN
                      dbo.keyType ON dbo.keyProperty.Type_Code = dbo.keyType.Code
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1 [56] 4 [18] 2))"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "project"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "key"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 121
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "device"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyProperty"
            Begin Extent = 
               Top = 6
               Left = 443
               Bottom = 91
               Right = 595
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyType"
            Begin Extent = 
               Top = 6
               Left = 633
               Bottom = 91
               Right = 785
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      RowHeights = 220
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'keyInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'keyInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'keyInfo'
GO
/****** Object:  View [dbo].[deviceInfo]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[deviceInfo]
AS
SELECT     dbo.project.Name AS ProjectName, d.Key_RID AS DeviceKey_RID, d.[Order] AS DeviceNumber, d.ModeList, d.DefaultID, k.RID AS Key_RID, 
                      k.[Position] AS Key_Position, k.Label AS Key_Label, k.Description AS Key_Description, kg.[Order] AS Key_Order, k.Scan_Code AS Key_Scan_Code, 
                      dbo.keyProperty.Type_Code AS Key_Type_Code, dbo.keyType.Name AS Key_Type_Name
FROM         dbo.[key] k INNER JOIN
                      dbo.keyGroup kg ON k.RID = kg.Key_RID INNER JOIN
                      dbo.device d ON kg.Device_RID = d.Key_RID INNER JOIN
                      dbo.project ON k.Project_Code = dbo.project.Code LEFT OUTER JOIN
                      dbo.keyProperty ON k.RID = dbo.keyProperty.Key_RID LEFT OUTER JOIN
                      dbo.keyType ON dbo.keyProperty.Type_Code = dbo.keyType.Code
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1 [56] 4 [18] 2))"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "k"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "kg"
            Begin Extent = 
               Top = 6
               Left = 228
               Bottom = 106
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 108
               Left = 228
               Bottom = 223
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "project"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyProperty"
            Begin Extent = 
               Top = 6
               Left = 418
               Bottom = 91
               Right = 570
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyType"
            Begin Extent = 
               Top = 6
               Left = 608
               Bottom = 91
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      RowHeights = 220
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'deviceInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'deviceInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'deviceInfo'
GO
/****** Object:  Table [dbo].[outronIntron]    Script Date: 01/13/2011 16:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[outronIntron](
	[KeyOutron_RID] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Intron] [varchar](255) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_outronToIntron] PRIMARY KEY CLUSTERED 
(
	[KeyOutron_RID] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[outronInfo]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[outronInfo]
AS
SELECT     DISTINCT di.ProjectName, ko.Mode, ko.Outron, di.Key_Position as Position, di.Key_Description AS Description, ko.[Group], di.Key_Order AS [Order], ko.RID, ko.Key_RID
FROM         dbo.deviceInfo di INNER JOIN
                      dbo.keyOutron ko ON di.Key_RID = ko.Key_RID
WHERE     (di.ModeList LIKE '%' + ko.Mode + '%')
GO
/****** Object:  View [dbo].[intronInfo]    Script Date: 01/13/2011 16:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[intronInfo]
AS
SELECT     di.ProjectName, ko.Mode, ko.Outron, di.Key_Order AS OutronOrder, oi.Intron, oi.[Order] AS IntronOrder, ko.Key_RID, oi.KeyOutron_RID
FROM         dbo.deviceInfo di INNER JOIN
                      dbo.keyOutron ko ON di.Key_RID = ko.Key_RID INNER JOIN
                      dbo.outronIntron oi ON ko.RID = oi.KeyOutron_RID
WHERE     (di.ModeList LIKE '%' + ko.Mode + '%')
GO
/****** Object:  Default [MSmerge_df_rowguid_D0D541C2F0B54B4595A9B4B55AF17EEC]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[address] ADD  CONSTRAINT [MSmerge_df_rowguid_D0D541C2F0B54B4595A9B4B55AF17EEC]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_A408A229C1A74D62A9ADAA7DE6BA3806]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[AlternateCode] ADD  CONSTRAINT [MSmerge_df_rowguid_A408A229C1A74D62A9ADAA7DE6BA3806]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_B7FE91F988184A7BBB6CA264E3F1F3E5]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[chip] ADD  CONSTRAINT [MSmerge_df_rowguid_B7FE91F988184A7BBB6CA264E3F1F3E5]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_517749A5670B4783A19894F88FB20AFE]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [MSmerge_df_rowguid_517749A5670B4783A19894F88FB20AFE]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_deviceInfo_Order]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[device] ADD  CONSTRAINT [DF_deviceInfo_Order]  DEFAULT ((-1)) FOR [Order]
GO
/****** Object:  Default [DF_deviceDescription_DefaultID]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[device] ADD  CONSTRAINT [DF_deviceDescription_DefaultID]  DEFAULT ('') FOR [DefaultID]
GO
/****** Object:  Default [MSmerge_df_rowguid_A0944858993B4900872F0FBF9D0DDEFF]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[device] ADD  CONSTRAINT [MSmerge_df_rowguid_A0944858993B4900872F0FBF9D0DDEFF]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_49821E59DF0848A69EB9BFBDE2A62F3E]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[digitGroup] ADD  CONSTRAINT [MSmerge_df_rowguid_49821E59DF0848A69EB9BFBDE2A62F3E]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_executorLoad_DoUpdate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[executorLoad] ADD  CONSTRAINT [DF_executorLoad_DoUpdate]  DEFAULT ('N') FOR [DoUpdate]
GO
/****** Object:  Default [MSmerge_df_rowguid_0BCD004108704253977A7429E251BB73]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[executorLoad] ADD  CONSTRAINT [MSmerge_df_rowguid_0BCD004108704253977A7429E251BB73]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_1D6C77DF3D764538BA6367766301D4F0]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[firmware] ADD  CONSTRAINT [MSmerge_df_rowguid_1D6C77DF3D764538BA6367766301D4F0]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_DEF3D7BA1B2845DEBEE3134235697A0D]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[goldenImage] ADD  CONSTRAINT [MSmerge_df_rowguid_DEF3D7BA1B2845DEBEE3134235697A0D]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_idLoad_DoUpdate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[idLoad] ADD  CONSTRAINT [DF_idLoad_DoUpdate]  DEFAULT ('N') FOR [DoUpdate]
GO
/****** Object:  Default [MSmerge_df_rowguid_2CEFCAF92B8642608EEEC68B864CEBC7]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[idLoad] ADD  CONSTRAINT [MSmerge_df_rowguid_2CEFCAF92B8642608EEEC68B864CEBC7]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_4D7ADAFF966343B1B18EA3AEC39FC62F]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[key] ADD  CONSTRAINT [MSmerge_df_rowguid_4D7ADAFF966343B1B18EA3AEC39FC62F]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_14D7A746658A4F3B99A843CFEE044200]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyGroup] ADD  CONSTRAINT [MSmerge_df_rowguid_14D7A746658A4F3B99A843CFEE044200]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_9EA2227600EB42D18DE40A56AE34D972]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyOutron] ADD  CONSTRAINT [MSmerge_df_rowguid_9EA2227600EB42D18DE40A56AE34D972]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_FD79D0867CCA493FB11339E1D3D99D48]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyProperty] ADD  CONSTRAINT [MSmerge_df_rowguid_FD79D0867CCA493FB11339E1D3D99D48]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_F986084E23794D1880892A2FE3CD9569]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyType] ADD  CONSTRAINT [MSmerge_df_rowguid_F986084E23794D1880892A2FE3CD9569]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_08F7F653E19C4787A145ED9948593744]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyType_ref] ADD  CONSTRAINT [MSmerge_df_rowguid_08F7F653E19C4787A145ED9948593744]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_EEEDFD2015574CBCA951C10B4423E6EB]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[manufacturer] ADD  CONSTRAINT [MSmerge_df_rowguid_EEEDFD2015574CBCA951C10B4423E6EB]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_2D387F502E43431796ED1C986CC5411D]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDDevice] ADD  CONSTRAINT [MSmerge_df_rowguid_2D387F502E43431796ED1C986CC5411D]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_7F974D5B8A504FD8AD966449B31A3D52]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDFunction] ADD  CONSTRAINT [MSmerge_df_rowguid_7F974D5B8A504FD8AD966449B31A3D52]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_9555CC9AF527452CB30DB07CCDB5100E]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDLog] ADD  CONSTRAINT [MSmerge_df_rowguid_9555CC9AF527452CB30DB07CCDB5100E]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_OriginalIDSnapshot_CreationDate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDSnapshot] ADD  CONSTRAINT [DF_OriginalIDSnapshot_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [MSmerge_df_rowguid_E0128C142DA341F787CD1DC59CA5D8D8]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDSnapshot] ADD  CONSTRAINT [MSmerge_df_rowguid_E0128C142DA341F787CD1DC59CA5D8D8]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_5AA120C166B34075A47FEF5FDC6D15A0]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDTNCaptureList] ADD  CONSTRAINT [MSmerge_df_rowguid_5AA120C166B34075A47FEF5FDC6D15A0]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_2B9C063058D2415086EF3D05BBD03509]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[outronIntron] ADD  CONSTRAINT [MSmerge_df_rowguid_2B9C063058D2415086EF3D05BBD03509]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_PickConfig_CreationDate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickConfig] ADD  CONSTRAINT [DF_PickConfig_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [MSmerge_df_rowguid_803FBC54AC5748FF9692D71B0DC3B05D]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickConfig] ADD  CONSTRAINT [MSmerge_df_rowguid_803FBC54AC5748FF9692D71B0DC3B05D]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_647FB30E2C3447BBA62C146C9114EC6E]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap] ADD  CONSTRAINT [MSmerge_df_rowguid_647FB30E2C3447BBA62C146C9114EC6E]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_PickDataMap2_CreationDate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap2] ADD  CONSTRAINT [DF_PickDataMap2_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [MSmerge_df_rowguid_58EE702DC7FD42E1A748D0D9BA52F458]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap2] ADD  CONSTRAINT [MSmerge_df_rowguid_58EE702DC7FD42E1A748D0D9BA52F458]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_2F34957E74344C9B8C1F4C2CD4E17ACF]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMapLabel] ADD  CONSTRAINT [MSmerge_df_rowguid_2F34957E74344C9B8C1F4C2CD4E17ACF]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_8443EEF32C2340C6ADB69D4B952EE933]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMapPDLIntron] ADD  CONSTRAINT [MSmerge_df_rowguid_8443EEF32C2340C6ADB69D4B952EE933]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_9AC6EA94B19E45268A1B68D0F572D51A]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID] ADD  CONSTRAINT [MSmerge_df_rowguid_9AC6EA94B19E45268A1B68D0F572D51A]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_PickID2_CreationDate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID2] ADD  CONSTRAINT [DF_PickID2_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [MSmerge_df_rowguid_AA1E4F3AE15943ACB7E9A24AA6D1031C]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID2] ADD  CONSTRAINT [MSmerge_df_rowguid_AA1E4F3AE15943ACB7E9A24AA6D1031C]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_7BCED402EFD64BDD9239C6A87C04F3BD]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix] ADD  CONSTRAINT [MSmerge_df_rowguid_7BCED402EFD64BDD9239C6A87C04F3BD]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_PickPrefix2_CreationDate]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix2] ADD  CONSTRAINT [DF_PickPrefix2_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [MSmerge_df_rowguid_19FEA186852A45BF89FABE7B69439C51]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix2] ADD  CONSTRAINT [MSmerge_df_rowguid_19FEA186852A45BF89FABE7B69439C51]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_5AF4AF7D3FF14119A73CFA68C89275EA]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [MSmerge_df_rowguid_5AF4AF7D3FF14119A73CFA68C89275EA]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_product_IncludePath]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[project] ADD  CONSTRAINT [DF_product_IncludePath]  DEFAULT ('') FOR [Description]
GO
/****** Object:  Default [MSmerge_df_rowguid_EBAEC581C3C747A98D8D3996186C0645]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[project] ADD  CONSTRAINT [MSmerge_df_rowguid_EBAEC581C3C747A98D8D3996186C0645]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_8EF57C7E2F9B4D3CAD17C51FF28794BB]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[project_chip] ADD  CONSTRAINT [MSmerge_df_rowguid_8EF57C7E2F9B4D3CAD17C51FF28794BB]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_ProjectLock_Username]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLock] ADD  CONSTRAINT [DF_ProjectLock_Username]  DEFAULT ('Unknown') FOR [Username]
GO
/****** Object:  Default [DF_ProjectLock_TimeStamp]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLock] ADD  CONSTRAINT [DF_ProjectLock_TimeStamp]  DEFAULT (getutcdate()) FOR [TimeStamp]
GO
/****** Object:  Default [MSmerge_df_rowguid_439B1088557C4BA7B1D9DE701F2EAA6C]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLock] ADD  CONSTRAINT [MSmerge_df_rowguid_439B1088557C4BA7B1D9DE701F2EAA6C]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_F436AB5A506646C49BF255934A6A294B]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLockStatus] ADD  CONSTRAINT [MSmerge_df_rowguid_F436AB5A506646C49BF255934A6A294B]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_187187E16BCF4849A21AB699350BD367]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PunchThroughTemporary] ADD  CONSTRAINT [MSmerge_df_rowguid_187187E16BCF4849A21AB699350BD367]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [DF_Report_CreationDate]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[Report] ADD  CONSTRAINT [DF_Report_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [MSmerge_df_rowguid_FFA641F2C409444D852275298C7C8DAC]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[Report] ADD  CONSTRAINT [MSmerge_df_rowguid_FFA641F2C409444D852275298C7C8DAC]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Default [MSmerge_df_rowguid_51088EF1F8DB4E6382C3E8AC99A6A540]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[ReportType] ADD  CONSTRAINT [MSmerge_df_rowguid_51088EF1F8DB4E6382C3E8AC99A6A540]  DEFAULT (newsequentialid()) FOR [rowguid]
GO
/****** Object:  Check [repl_identity_range_E4D862FE_74AB_4356_A684_EFD40E393B94]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[goldenImage]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_E4D862FE_74AB_4356_A684_EFD40E393B94] CHECK NOT FOR REPLICATION (([RID]>(30) AND [RID]<=(1030) OR [RID]>(1030) AND [RID]<=(2030)))
GO
ALTER TABLE [dbo].[goldenImage] CHECK CONSTRAINT [repl_identity_range_E4D862FE_74AB_4356_A684_EFD40E393B94]
GO
/****** Object:  Check [repl_identity_range_905F9B44_386F_487F_B786_9FAEF92087A7]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDDevice]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_905F9B44_386F_487F_B786_9FAEF92087A7] CHECK NOT FOR REPLICATION (([RID]>=(1) AND [RID]<=(1001) OR [RID]>(1001) AND [RID]<=(2001)))
GO
ALTER TABLE [dbo].[OriginalIDDevice] CHECK CONSTRAINT [repl_identity_range_905F9B44_386F_487F_B786_9FAEF92087A7]
GO
/****** Object:  Check [repl_identity_range_B8EBF6E9_131B_4F33_8971_969C9DBA87D7]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDFunction]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_B8EBF6E9_131B_4F33_8971_969C9DBA87D7] CHECK NOT FOR REPLICATION (([RID]>=(1) AND [RID]<=(1001) OR [RID]>(1001) AND [RID]<=(2001)))
GO
ALTER TABLE [dbo].[OriginalIDFunction] CHECK CONSTRAINT [repl_identity_range_B8EBF6E9_131B_4F33_8971_969C9DBA87D7]
GO
/****** Object:  Check [repl_identity_range_1ED2CDA0_F24E_4DD9_84D1_754DE1129963]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDLog]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_1ED2CDA0_F24E_4DD9_84D1_754DE1129963] CHECK NOT FOR REPLICATION (([RID]>=(1) AND [RID]<=(1001) OR [RID]>(1001) AND [RID]<=(2001)))
GO
ALTER TABLE [dbo].[OriginalIDLog] CHECK CONSTRAINT [repl_identity_range_1ED2CDA0_F24E_4DD9_84D1_754DE1129963]
GO
/****** Object:  Check [repl_identity_range_95F113F9_1D67_40BD_B09D_049F9A72E8F5]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDSnapshot]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_95F113F9_1D67_40BD_B09D_049F9A72E8F5] CHECK NOT FOR REPLICATION (([PK_OriginalIDSnapshot]>(118193) AND [PK_OriginalIDSnapshot]<=(119193) OR [PK_OriginalIDSnapshot]>(119193) AND [PK_OriginalIDSnapshot]<=(120193)))
GO
ALTER TABLE [dbo].[OriginalIDSnapshot] CHECK CONSTRAINT [repl_identity_range_95F113F9_1D67_40BD_B09D_049F9A72E8F5]
GO
/****** Object:  Check [repl_identity_range_E91F9385_6302_4D7D_A6E2_36619008C0C8]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDTNCaptureList]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_E91F9385_6302_4D7D_A6E2_36619008C0C8] CHECK NOT FOR REPLICATION (([CaptureRID]>=(1) AND [CaptureRID]<=(1001) OR [CaptureRID]>(1001) AND [CaptureRID]<=(2001)))
GO
ALTER TABLE [dbo].[OriginalIDTNCaptureList] CHECK CONSTRAINT [repl_identity_range_E91F9385_6302_4D7D_A6E2_36619008C0C8]
GO
/****** Object:  Check [repl_identity_range_3DB3A06D_1C0E_43E6_9DEB_AE23F991F2B0]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickConfig]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_3DB3A06D_1C0E_43E6_9DEB_AE23F991F2B0] CHECK NOT FOR REPLICATION (([PK_PickConfig]>(57) AND [PK_PickConfig]<=(1057) OR [PK_PickConfig]>(1057) AND [PK_PickConfig]<=(2057)))
GO
ALTER TABLE [dbo].[PickConfig] CHECK CONSTRAINT [repl_identity_range_3DB3A06D_1C0E_43E6_9DEB_AE23F991F2B0]
GO
/****** Object:  Check [repl_identity_range_72B7575F_A2D2_4767_948B_3539062EC72F]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_72B7575F_A2D2_4767_948B_3539062EC72F] CHECK NOT FOR REPLICATION (([RID]>(70971) AND [RID]<=(71971) OR [RID]>(71971) AND [RID]<=(72971)))
GO
ALTER TABLE [dbo].[PickDataMap] CHECK CONSTRAINT [repl_identity_range_72B7575F_A2D2_4767_948B_3539062EC72F]
GO
/****** Object:  Check [repl_identity_range_A473BCA5_9159_433C_BFA6_3D55F5F491E3]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap2]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_A473BCA5_9159_433C_BFA6_3D55F5F491E3] CHECK NOT FOR REPLICATION (([PK_PickDataMap]>(2575344) AND [PK_PickDataMap]<=(2576344) OR [PK_PickDataMap]>(2576344) AND [PK_PickDataMap]<=(2577344)))
GO
ALTER TABLE [dbo].[PickDataMap2] CHECK CONSTRAINT [repl_identity_range_A473BCA5_9159_433C_BFA6_3D55F5F491E3]
GO
/****** Object:  Check [repl_identity_range_22D1610C_CC87_4A5A_8A8B_AE964EF66DFA]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMapLabel]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_22D1610C_CC87_4A5A_8A8B_AE964EF66DFA] CHECK NOT FOR REPLICATION (([LabelRID]>=(1) AND [LabelRID]<=(1001) OR [LabelRID]>(1001) AND [LabelRID]<=(2001)))
GO
ALTER TABLE [dbo].[PickDataMapLabel] CHECK CONSTRAINT [repl_identity_range_22D1610C_CC87_4A5A_8A8B_AE964EF66DFA]
GO
/****** Object:  Check [repl_identity_range_791C9A9F_31EA_4C7E_95B9_96F46DD35550]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMapPDLIntron]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_791C9A9F_31EA_4C7E_95B9_96F46DD35550] CHECK NOT FOR REPLICATION (([IntronRID]>=(1) AND [IntronRID]<=(1001) OR [IntronRID]>(1001) AND [IntronRID]<=(2001)))
GO
ALTER TABLE [dbo].[PickDataMapPDLIntron] CHECK CONSTRAINT [repl_identity_range_791C9A9F_31EA_4C7E_95B9_96F46DD35550]
GO
/****** Object:  Check [repl_identity_range_002FBC4D_F838_4713_B18B_0CE171E3225A]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_002FBC4D_F838_4713_B18B_0CE171E3225A] CHECK NOT FOR REPLICATION (([PickRID]>(514) AND [PickRID]<=(1514) OR [PickRID]>(1514) AND [PickRID]<=(2514)))
GO
ALTER TABLE [dbo].[PickID] CHECK CONSTRAINT [repl_identity_range_002FBC4D_F838_4713_B18B_0CE171E3225A]
GO
/****** Object:  Check [repl_identity_range_DE70FA66_8CC2_405F_9E43_DF64652AAA12]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID2]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_DE70FA66_8CC2_405F_9E43_DF64652AAA12] CHECK NOT FOR REPLICATION (([PK_PickID]>(118233) AND [PK_PickID]<=(119233) OR [PK_PickID]>(119233) AND [PK_PickID]<=(120233)))
GO
ALTER TABLE [dbo].[PickID2] CHECK CONSTRAINT [repl_identity_range_DE70FA66_8CC2_405F_9E43_DF64652AAA12]
GO
/****** Object:  Check [repl_identity_range_C31A646E_D307_42A3_8588_B1A18EB41D10]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_C31A646E_D307_42A3_8588_B1A18EB41D10] CHECK NOT FOR REPLICATION (([RID]>(1625) AND [RID]<=(2625) OR [RID]>(2625) AND [RID]<=(3625)))
GO
ALTER TABLE [dbo].[PickPrefix] CHECK CONSTRAINT [repl_identity_range_C31A646E_D307_42A3_8588_B1A18EB41D10]
GO
/****** Object:  Check [repl_identity_range_FA557AE0_CE6B_4A20_98E9_EC48C876EC19]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix2]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_FA557AE0_CE6B_4A20_98E9_EC48C876EC19] CHECK NOT FOR REPLICATION (([PK_PickPrefix]>(249938) AND [PK_PickPrefix]<=(250938) OR [PK_PickPrefix]>(250938) AND [PK_PickPrefix]<=(251938)))
GO
ALTER TABLE [dbo].[PickPrefix2] CHECK CONSTRAINT [repl_identity_range_FA557AE0_CE6B_4A20_98E9_EC48C876EC19]
GO
/****** Object:  Check [repl_identity_range_95E17961_5BCB_4278_B43B_1589960B529A]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[product]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_95E17961_5BCB_4278_B43B_1589960B529A] CHECK NOT FOR REPLICATION (([ReplicationID]>(24006) AND [ReplicationID]<=(25006) OR [ReplicationID]>(25006) AND [ReplicationID]<=(26006)))
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [repl_identity_range_95E17961_5BCB_4278_B43B_1589960B529A]
GO
/****** Object:  Check [repl_identity_range_3CC48C9A_60E8_4E35_81A0_0BBAC7AFCF00]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLock]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_3CC48C9A_60E8_4E35_81A0_0BBAC7AFCF00] CHECK NOT FOR REPLICATION (([PK_ProjectLock]>(92) AND [PK_ProjectLock]<=(1092) OR [PK_ProjectLock]>(1092) AND [PK_ProjectLock]<=(2092)))
GO
ALTER TABLE [dbo].[ProjectLock] CHECK CONSTRAINT [repl_identity_range_3CC48C9A_60E8_4E35_81A0_0BBAC7AFCF00]
GO
/****** Object:  Check [repl_identity_range_78F77954_F41F_45F4_B0DA_02C65804101D]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLockStatus]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_78F77954_F41F_45F4_B0DA_02C65804101D] CHECK NOT FOR REPLICATION (([PK_ProjectLockStatus]>(4) AND [PK_ProjectLockStatus]<=(1004) OR [PK_ProjectLockStatus]>(1004) AND [PK_ProjectLockStatus]<=(2004)))
GO
ALTER TABLE [dbo].[ProjectLockStatus] CHECK CONSTRAINT [repl_identity_range_78F77954_F41F_45F4_B0DA_02C65804101D]
GO
/****** Object:  Check [repl_identity_range_B77ADA74_9A8B_46F6_9876_5E0AD8AB87D4]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[Report]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_B77ADA74_9A8B_46F6_9876_5E0AD8AB87D4] CHECK NOT FOR REPLICATION (([PK_Report]>=(1) AND [PK_Report]<=(1001) OR [PK_Report]>(1001) AND [PK_Report]<=(2001)))
GO
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [repl_identity_range_B77ADA74_9A8B_46F6_9876_5E0AD8AB87D4]
GO
/****** Object:  Check [repl_identity_range_5550C392_F935_48F4_A555_F8BE5D609056]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[ReportType]  WITH NOCHECK ADD  CONSTRAINT [repl_identity_range_5550C392_F935_48F4_A555_F8BE5D609056] CHECK NOT FOR REPLICATION (([PK_ReportType]>=(1) AND [PK_ReportType]<=(1001) OR [PK_ReportType]>(1001) AND [PK_ReportType]<=(2001)))
GO
ALTER TABLE [dbo].[ReportType] CHECK CONSTRAINT [repl_identity_range_5550C392_F935_48F4_A555_F8BE5D609056]
GO
/****** Object:  ForeignKey [FK_address_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[address]  WITH CHECK ADD  CONSTRAINT [FK_address_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[address] CHECK CONSTRAINT [FK_address_project]
GO
/****** Object:  ForeignKey [FK_AlternateCode_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[AlternateCode]  WITH CHECK ADD  CONSTRAINT [FK_AlternateCode_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AlternateCode] CHECK CONSTRAINT [FK_AlternateCode_project]
GO
/****** Object:  ForeignKey [FK_config_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[config]  WITH CHECK ADD  CONSTRAINT [FK_config_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[config] CHECK CONSTRAINT [FK_config_project]
GO
/****** Object:  ForeignKey [FK_deviceInfo_key]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[device]  WITH NOCHECK ADD  CONSTRAINT [FK_deviceInfo_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[device] CHECK CONSTRAINT [FK_deviceInfo_key]
GO
/****** Object:  ForeignKey [FK_digitGroup_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[digitGroup]  WITH CHECK ADD  CONSTRAINT [FK_digitGroup_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[digitGroup] CHECK CONSTRAINT [FK_digitGroup_project]
GO
/****** Object:  ForeignKey [FK_executorLoad_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[executorLoad]  WITH CHECK ADD  CONSTRAINT [FK_executorLoad_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[executorLoad] CHECK CONSTRAINT [FK_executorLoad_project]
GO
/****** Object:  ForeignKey [FK_firmware_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[firmware]  WITH CHECK ADD  CONSTRAINT [FK_firmware_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[firmware] CHECK CONSTRAINT [FK_firmware_project]
GO
/****** Object:  ForeignKey [FK_goldenImage_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[goldenImage]  WITH CHECK ADD  CONSTRAINT [FK_goldenImage_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[goldenImage] CHECK CONSTRAINT [FK_goldenImage_project]
GO
/****** Object:  ForeignKey [FK_idLoad_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[idLoad]  WITH NOCHECK ADD  CONSTRAINT [FK_idLoad_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[idLoad] CHECK CONSTRAINT [FK_idLoad_project]
GO
/****** Object:  ForeignKey [FK_key_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[key]  WITH CHECK ADD  CONSTRAINT [FK_key_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[key] CHECK CONSTRAINT [FK_key_project]
GO
/****** Object:  ForeignKey [FK_keyGroup_device]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_keyGroup_device] FOREIGN KEY([Device_RID])
REFERENCES [dbo].[key] ([RID])
GO
ALTER TABLE [dbo].[keyGroup] CHECK CONSTRAINT [FK_keyGroup_device]
GO
/****** Object:  ForeignKey [FK_keyGroup_key]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_keyGroup_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[keyGroup] CHECK CONSTRAINT [FK_keyGroup_key]
GO
/****** Object:  ForeignKey [FK_keyOutron_key]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyOutron]  WITH NOCHECK ADD  CONSTRAINT [FK_keyOutron_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[keyOutron] CHECK CONSTRAINT [FK_keyOutron_key]
GO
/****** Object:  ForeignKey [FK_keyProperty_key]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyProperty]  WITH CHECK ADD  CONSTRAINT [FK_keyProperty_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[keyProperty] CHECK CONSTRAINT [FK_keyProperty_key]
GO
/****** Object:  ForeignKey [FK_keyProperty_keyType]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[keyProperty]  WITH CHECK ADD  CONSTRAINT [FK_keyProperty_keyType] FOREIGN KEY([Type_Code])
REFERENCES [dbo].[keyType] ([Code])
GO
ALTER TABLE [dbo].[keyProperty] CHECK CONSTRAINT [FK_keyProperty_keyType]
GO
/****** Object:  ForeignKey [FK_OriginalIDDevice_PickID]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDDevice]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDDevice_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDDevice] CHECK CONSTRAINT [FK_OriginalIDDevice_PickID]
GO
/****** Object:  ForeignKey [FK_OriginalIDFunction_PickID]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDFunction]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDFunction_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDFunction] CHECK CONSTRAINT [FK_OriginalIDFunction_PickID]
GO
/****** Object:  ForeignKey [FK_OriginalIDLog_PickID]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDLog]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDLog_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDLog] CHECK CONSTRAINT [FK_OriginalIDLog_PickID]
GO
/****** Object:  ForeignKey [FK_OriginalIDTNCaptureList_OriginalIDFunction]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[OriginalIDTNCaptureList]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDTNCaptureList_OriginalIDFunction] FOREIGN KEY([FunctionRID])
REFERENCES [dbo].[OriginalIDFunction] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDTNCaptureList] CHECK CONSTRAINT [FK_OriginalIDTNCaptureList_OriginalIDFunction]
GO
/****** Object:  ForeignKey [FK_OutronToIntron_outronGroup]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[outronIntron]  WITH NOCHECK ADD  CONSTRAINT [FK_OutronToIntron_outronGroup] FOREIGN KEY([KeyOutron_RID])
REFERENCES [dbo].[keyOutron] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[outronIntron] CHECK CONSTRAINT [FK_OutronToIntron_outronGroup]
GO
/****** Object:  ForeignKey [FK_PickConfig_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickConfig]  WITH CHECK ADD  CONSTRAINT [FK_PickConfig_project] FOREIGN KEY([FK_ProjectCode])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickConfig] CHECK CONSTRAINT [FK_PickConfig_project]
GO
/****** Object:  ForeignKey [FK_PickDataMap_PickID]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap]  WITH NOCHECK ADD  CONSTRAINT [FK_PickDataMap_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMap] CHECK CONSTRAINT [FK_PickDataMap_PickID]
GO
/****** Object:  ForeignKey [FK_PickDataMap2_PickID2]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMap2]  WITH CHECK ADD  CONSTRAINT [FK_PickDataMap2_PickID2] FOREIGN KEY([FK_PickID])
REFERENCES [dbo].[PickID2] ([PK_PickID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMap2] CHECK CONSTRAINT [FK_PickDataMap2_PickID2]
GO
/****** Object:  ForeignKey [FK_PickDataMapLabel_PickDataMap]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMapLabel]  WITH NOCHECK ADD  CONSTRAINT [FK_PickDataMapLabel_PickDataMap] FOREIGN KEY([PickDataMap_RID])
REFERENCES [dbo].[PickDataMap] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMapLabel] CHECK CONSTRAINT [FK_PickDataMapLabel_PickDataMap]
GO
/****** Object:  ForeignKey [FK_PickDataMapPDLIntron_PickDataMap]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickDataMapPDLIntron]  WITH NOCHECK ADD  CONSTRAINT [FK_PickDataMapPDLIntron_PickDataMap] FOREIGN KEY([pickDataMap_RID])
REFERENCES [dbo].[PickDataMap] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMapPDLIntron] CHECK CONSTRAINT [FK_PickDataMapPDLIntron_PickDataMap]
GO
/****** Object:  ForeignKey [FK_PickID2_OriginalIDSnapshot]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID2]  WITH CHECK ADD  CONSTRAINT [FK_PickID2_OriginalIDSnapshot] FOREIGN KEY([FK_IDSnapshot])
REFERENCES [dbo].[OriginalIDSnapshot] ([PK_OriginalIDSnapshot])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PickID2] CHECK CONSTRAINT [FK_PickID2_OriginalIDSnapshot]
GO
/****** Object:  ForeignKey [FK_PickID2_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickID2]  WITH CHECK ADD  CONSTRAINT [FK_PickID2_project] FOREIGN KEY([FK_Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickID2] CHECK CONSTRAINT [FK_PickID2_project]
GO
/****** Object:  ForeignKey [FK_PickPrefix_PickID]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix]  WITH NOCHECK ADD  CONSTRAINT [FK_PickPrefix_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickPrefix] CHECK CONSTRAINT [FK_PickPrefix_PickID]
GO
/****** Object:  ForeignKey [FK_PickPrefix2_PickID2]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[PickPrefix2]  WITH CHECK ADD  CONSTRAINT [FK_PickPrefix2_PickID2] FOREIGN KEY([FK_PickID])
REFERENCES [dbo].[PickID2] ([PK_PickID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickPrefix2] CHECK CONSTRAINT [FK_PickPrefix2_PickID2]
GO
/****** Object:  ForeignKey [FK_product_chip]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[product]  WITH NOCHECK ADD  CONSTRAINT [FK_product_chip] FOREIGN KEY([Chip_Code])
REFERENCES [dbo].[chip] ([Code])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_chip]
GO
/****** Object:  ForeignKey [FK_project_manufacturer]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[project]  WITH NOCHECK ADD  CONSTRAINT [FK_project_manufacturer] FOREIGN KEY([Manufacturer_Code])
REFERENCES [dbo].[manufacturer] ([Code])
GO
ALTER TABLE [dbo].[project] CHECK CONSTRAINT [FK_project_manufacturer]
GO
/****** Object:  ForeignKey [FK_project_chip_chip]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[project_chip]  WITH CHECK ADD  CONSTRAINT [FK_project_chip_chip] FOREIGN KEY([Chip_Code])
REFERENCES [dbo].[chip] ([Code])
GO
ALTER TABLE [dbo].[project_chip] CHECK CONSTRAINT [FK_project_chip_chip]
GO
/****** Object:  ForeignKey [FK_project_chip_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[project_chip]  WITH CHECK ADD  CONSTRAINT [FK_project_chip_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[project_chip] CHECK CONSTRAINT [FK_project_chip_project]
GO
/****** Object:  ForeignKey [FK_ProjectLock_project]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLock]  WITH CHECK ADD  CONSTRAINT [FK_ProjectLock_project] FOREIGN KEY([FK_ProjectCode])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProjectLock] CHECK CONSTRAINT [FK_ProjectLock_project]
GO
/****** Object:  ForeignKey [FK_ProjectLock_ProjectLockStatus]    Script Date: 01/13/2011 16:13:55 ******/
ALTER TABLE [dbo].[ProjectLock]  WITH CHECK ADD  CONSTRAINT [FK_ProjectLock_ProjectLockStatus] FOREIGN KEY([FK_LockStatus])
REFERENCES [dbo].[ProjectLockStatus] ([PK_ProjectLockStatus])
GO
ALTER TABLE [dbo].[ProjectLock] CHECK CONSTRAINT [FK_ProjectLock_ProjectLockStatus]
GO
/****** Object:  ForeignKey [FK_Report_project]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_project] FOREIGN KEY([FK_Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_project]
GO
/****** Object:  ForeignKey [FK_Report_ReportType]    Script Date: 01/13/2011 16:13:56 ******/
ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_ReportType] FOREIGN KEY([FK_ReportType])
REFERENCES [dbo].[ReportType] ([PK_ReportType])
GO
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_ReportType]
GO
