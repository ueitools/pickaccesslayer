USE [Product2]
GO
/****** Object:  User [amunoz]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [amunoz] FOR LOGIN [UEIC\amunoz] WITH DEFAULT_SCHEMA=[amunoz]
GO
/****** Object:  User [amuratov]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [amuratov] FOR LOGIN [UEIC\amuratov] WITH DEFAULT_SCHEMA=[amuratov]
GO
/****** Object:  User [APAC\bmuralidharkini]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [APAC\bmuralidharkini] FOR LOGIN [APAC\bmuralidharkini] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [APAC\gmonica]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [APAC\gmonica] FOR LOGIN [APAC\gmonica] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [APAC\mpchandrika]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [APAC\mpchandrika] FOR LOGIN [APAC\mpchandrika] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [bcampbell]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [bcampbell] FOR LOGIN [UEIC\bcampbell] WITH DEFAULT_SCHEMA=[bcampbell]
GO
/****** Object:  User [BSinohui]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [BSinohui] FOR LOGIN [UEIC\bsinohui] WITH DEFAULT_SCHEMA=[BSinohui]
GO
/****** Object:  User [hplasman]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [hplasman] FOR LOGIN [UEIC\hplasman] WITH DEFAULT_SCHEMA=[hplasman]
GO
/****** Object:  User [IV&V]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [IV&V] FOR LOGIN [UEIC\IV&V]
GO
/****** Object:  User [JLim]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [JLim] FOR LOGIN [UEIC\jlim] WITH DEFAULT_SCHEMA=[JLim]
GO
/****** Object:  User [jpan]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [jpan] FOR LOGIN [UEIC\jpan] WITH DEFAULT_SCHEMA=[jpan]
GO
/****** Object:  User [knguyen]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [knguyen] FOR LOGIN [UEIC\knguyen] WITH DEFAULT_SCHEMA=[knguyen]
GO
/****** Object:  User [NETWORK SERVICE]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [NETWORK SERVICE] FOR LOGIN [NT AUTHORITY\NETWORK SERVICE]
GO
/****** Object:  User [sadulratananu]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [sadulratananu] FOR LOGIN [UEIC\sadulratananu] WITH DEFAULT_SCHEMA=[sadulratananu]
GO
/****** Object:  User [UEIC\acheng]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\acheng] FOR LOGIN [UEIC\acheng] WITH DEFAULT_SCHEMA=[UEIC\acheng]
GO
/****** Object:  User [UEIC\ayuh]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\ayuh] FOR LOGIN [UEIC\ayuh] WITH DEFAULT_SCHEMA=[UEIC\ayuh]
GO
/****** Object:  User [UEIC\bkim]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\bkim] FOR LOGIN [UEIC\bkim] WITH DEFAULT_SCHEMA=[UEIC\bkim]
GO
/****** Object:  User [UEIC\jetter]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\jetter] FOR LOGIN [UEIC\jetter] WITH DEFAULT_SCHEMA=[UEIC\jetter]
GO
/****** Object:  User [UEIC\jhawes]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\jhawes] FOR LOGIN [UEIC\jhawes] WITH DEFAULT_SCHEMA=[UEIC\jhawes]
GO
/****** Object:  User [UEIC\jpableo]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\jpableo] FOR LOGIN [UEIC\jpableo] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UEIC\jperez]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\jperez] FOR LOGIN [UEIC\jperez] WITH DEFAULT_SCHEMA=[UEIC\jperez]
GO
/****** Object:  User [UEIC\Library_Capture]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\Library_Capture] FOR LOGIN [UEIC\Library_Capture]
GO
/****** Object:  User [UEIC\rkim]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\rkim] FOR LOGIN [UEIC\rkim] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UEIC\shuang]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\shuang] FOR LOGIN [UEIC\shuang] WITH DEFAULT_SCHEMA=[UEIC\shuang]
GO
/****** Object:  User [UEIC\SW Enjgineering]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\SW Enjgineering] FOR LOGIN [UEIC\SW Enjgineering]
GO
/****** Object:  User [UEIC\thowell]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\thowell] FOR LOGIN [UEIC\thowell] WITH DEFAULT_SCHEMA=[UEIC\thowell]
GO
/****** Object:  User [UEIC\wwang]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [UEIC\wwang] FOR LOGIN [UEIC\wwang] WITH DEFAULT_SCHEMA=[UEIC\wwang]
GO
/****** Object:  User [VHong]    Script Date: 10/21/2010 18:55:54 ******/
CREATE USER [VHong] FOR LOGIN [UEIC\VHong] WITH DEFAULT_SCHEMA=[VHong]
GO
/****** Object:  Schema [amuratov]    Script Date: 10/21/2010 18:55:45 ******/
CREATE SCHEMA [amuratov] AUTHORIZATION [amuratov]
GO
/****** Object:  Schema [bcampbell]    Script Date: 10/21/2010 18:55:45 ******/
CREATE SCHEMA [bcampbell] AUTHORIZATION [bcampbell]
GO
/****** Object:  Schema [JLim]    Script Date: 10/21/2010 18:55:45 ******/
CREATE SCHEMA [JLim] AUTHORIZATION [JLim]
GO
/****** Object:  Schema [UEIC\acheng]    Script Date: 10/21/2010 18:55:45 ******/
CREATE SCHEMA [UEIC\acheng] AUTHORIZATION [UEIC\acheng]
GO
/****** Object:  Schema [UEIC\shuang]    Script Date: 10/21/2010 18:55:45 ******/
CREATE SCHEMA [UEIC\shuang] AUTHORIZATION [UEIC\shuang]
GO
/****** Object:  Table [dbo].[manufacturer]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[manufacturer](
	[Code] [char](1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_manufacturer] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[keyType_ref]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keyType_ref](
	[FK_KeyTypeCode] [int] NOT NULL,
	[SRIKeyGroupName] [varchar](255) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[keyType]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keyType](
	[Code] [int] NOT NULL,
	[Name] [varchar](255) NULL,
 CONSTRAINT [PK_keyType] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[chip]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chip](
	[Code] [int] NOT NULL,
	[Number] [varchar](255) NOT NULL,
 CONSTRAINT [PK_chip] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickID2]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickID2](
	[FK_Project_Code] [int] NOT NULL,
	[FK_ID] [char](5) NOT NULL,
	[Version] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Comment] [text] NULL,
	[Exec] [int] NOT NULL,
	[FromID] [char](5) NOT NULL,
	[IsInverseData] [char](1) NOT NULL,
	[IsHexFormat] [char](1) NOT NULL,
	[IsInternalPick] [char](1) NOT NULL,
	[IsTemporary] [char](1) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_PickID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SystemFlags] [int] NULL,
	[IsInversePrefix] [char](1) NULL,
	[IsExternalPrefix] [char](1) NULL,
	[FK_IDSnapshot] [int] NULL,
 CONSTRAINT [PK_PickID2_1] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickID]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickID](
	[ID] [char](5) NOT NULL,
	[Version] [int] NOT NULL,
	[Time] [datetime] NULL,
	[Name] [char](10) NULL,
	[Comment] [text] NULL,
	[Project_Code] [int] NULL,
	[Exec] [int] NOT NULL,
	[isinverseddata] [char](1) NULL,
	[isexternalprefix] [char](1) NULL,
	[isfrequencydata] [char](1) NULL,
	[isinversedprefix] [char](1) NULL,
	[ishexformat] [char](1) NULL,
	[pickFromID] [char](10) NULL,
	[PickRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[isinternalpick] [char](1) NULL,
	[istemporary] [char](1) NULL,
 CONSTRAINT [PK_PickID] PRIMARY KEY CLUSTERED 
(
	[PickRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReportType]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportType](
	[PK_ReportType] [int] NOT NULL,
	[reportType] [char](50) NOT NULL,
 CONSTRAINT [PK_ReportType] PRIMARY KEY CLUSTERED 
(
	[PK_ReportType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PunchThroughTemporary]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PunchThroughTemporary](
	[Label] [varchar](50) NOT NULL,
	[RID] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDSnapshot]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OriginalIDSnapshot](
	[OriginalID] [xml] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_OriginalIDSnapshot] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 CONSTRAINT [PK_OriginalIDSnapshot] PRIMARY KEY CLUSTERED 
(
	[PK_OriginalIDSnapshot] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OriginalIDLog]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDLog](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[tn] [int] NULL,
	[time] [datetime] NULL,
	[operator] [varchar](255) NULL,
	[content] [char](255) NULL,
	[PickRID] [int] NOT NULL,
 CONSTRAINT [PK_OriginalIDLog] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDFunction]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDFunction](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[synth] [char](10) NULL,
	[Data] [varchar](255) NULL,
	[Label] [varchar](255) NULL,
	[Intron] [varchar](255) NULL,
	[IntronPriority] [char](10) NULL,
	[Comment] [varchar](255) NULL,
	[PickRID] [int] NULL,
 CONSTRAINT [PK_OriginalIDFunction] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDDevice]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDDevice](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[tn] [int] NULL,
	[branch] [char](2) NULL,
	[ispartnumber] [char](1) NULL,
	[isretail] [char](1) NULL,
	[PickRID] [int] NOT NULL,
 CONSTRAINT [PK_OriginalIDDevice] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMap2]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMap2](
	[Data] [varchar](50) NOT NULL,
	[IDLabel] [varchar](50) NOT NULL,
	[FirmwareLabel] [varchar](50) NOT NULL,
	[Comment] [text] NULL,
	[Intron] [nchar](10) NOT NULL,
	[IntronPriority] [varchar](3) NOT NULL,
	[KeyNumber] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_PickDataMap] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FK_PickID] [int] NOT NULL,
 CONSTRAINT [PK_PickDataMap2] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickDataMap] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMap]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMap](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Data] [varchar](255) NULL,
	[Synth] [char](10) NULL,
	[Outron] [char](10) NULL,
	[KeyLabel] [varchar](255) NULL,
	[OutronGroup] [int] NULL,
	[Intron] [varchar](255) NULL,
	[IntronPriority] [int] NULL,
	[Position] [int] NULL,
	[Comment] [text] NULL,
	[PickRID] [int] NOT NULL,
 CONSTRAINT [PK_PickDataMap] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertIdSnapshot]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertIdSnapshot]
	@OriginalID xml,
	@InsertedRID int OUTPUT
AS
BEGIN
    INSERT INTO OriginalIDSnapshot
    (OriginalID)
    VALUES (@OriginalID);
    SELECT @InsertedRID = @@IDENTITY;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteExceptLastN]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteExceptLastN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM PICKID2 
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND [Version] NOT IN 
	(SELECT TOP (@N) [Version] 
		FROM pickID2 p2 
		WHERE @ID = p2.FK_ID AND FK_Project_Code = @Project_Code 
		ORDER BY [Version] DESC);		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteExceptFirstN]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteExceptFirstN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE 
	FROM PICKID2 
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND [Version] NOT IN 
		(SELECT TOP (@N) [Version] 
		FROM pickID2 p2 
		WHERE @ID = p2.FK_ID AND FK_Project_Code = @Project_Code ORDER BY [Version])
		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteExceptFirstAndLast]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteExceptFirstAndLast]
@Project_Code int,
@ID char(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND [Version] < 
	(SELECT MAX([Version]) FROM PICKID2 WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID) AND
	[Version] > (SELECT MIN([Version]) FROM PICKID2 WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID);

END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteBeforeN]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteBeforeN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	
	WHERE FK_Project_Code = @Project_Code AND [Version] < @N AND FK_ID = @ID;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteBeforeDate]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteBeforeDate]
					@Project_Code int,
					@ID char(5),
					@BeforeTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]	
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID AND 
		[CreationDate] < @BeforeTime;
		
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAfterN]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteAfterN]
					@Project_Code int,
					@ID char(5),
					@N int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM PICKID2
	OUTPUT DELETED.FK_ID as [ID], DELETED.[Version]
	WHERE FK_Project_Code = @Project_Code AND [Version] > @N AND FK_ID = @ID;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAfterDate]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteAfterDate]
					@Project_Code int,
					@ID char(5),
					@AfterTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM PICKID2	
	OUTPUT DELETED.FK_ID as ID, DELETED.[Version]
	WHERE FK_Project_Code = @Project_Code AND FK_ID = @ID 
	AND [CreationDate] > @AfterTime;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertPickId]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickId]
	@FK_ProjectCode int,
	@FK_ID char(5),
	@UserName nvarchar(50),
	@Comment text,
	@Exec int,
	@FromID char(5),
	@IsInverseData char(1),
	@IsHexFormat char(1),
	@IsInternalPick char(1),
	@IsTemporary char(1),
	@IsInversePrefix char(1),
	@IsExternalPrefix char(1),		
	@FK_IDSnapshot int, 
	@InsertedRID int OUTPUT
AS
BEGIN
	DECLARE @Version int;	
	SELECT @Version=ISNULL(MAX([Version])+1,0) FROM PickID2 
	WHERE FK_Project_Code = @FK_ProjectCode AND FK_ID = @FK_ID;	
    INSERT INTO PickID2
    (FK_Project_Code,FK_ID,[Version],UserName,Comment,[Exec],FromID,IsInverseData,IsHexFormat,IsInternalPick,IsTemporary, IsInversePrefix, IsExternalPrefix,FK_IDSnapshot)
    VALUES (@FK_ProjectCode, @FK_ID, @Version, @UserName, @Comment, @Exec, @FromID, @IsInverseData, @IsHexFormat, @IsInternalPick, @IsTemporary, @IsInversePrefix, @IsExternalPrefix, @FK_IDSnapshot);
    SELECT @InsertedRID=@@Identity;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgePickIdVersion]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgePickIdVersion]
	@Id char(5),
	@Project_Code int,
	@Version int, 
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT	
AS
BEGIN
  DELETE FROM [PickTest].[dbo].[PickID2] 
  WHERE FK_ID = @Id AND FK_Project_Code= @Project_Code AND 
  [Version] = @Version;
  
  SELECT @NumRowsChanged=@@ROWCOUNT, @ErrorCode=@@ERROR
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgeLatestPickId]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PurgeLatestPickId]
	@Id char(5),
	@Project_Code int,
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT	
AS
BEGIN
  DELETE FROM [PickTest].[dbo].[PickID2] 
  WHERE FK_ID = @Id AND FK_Project_Code= @Project_Code AND 
  [Version] = (SELECT MAX([Version]) 
  FROM [PickTest].[dbo].[PickID2] p2
  WHERE @Id = p2.FK_ID AND @Project_Code = p2.FK_Project_Code);
  
  SELECT @NumRowsChanged=@@ROWCOUNT, @ErrorCode=@@ERROR
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PurgeLatestAllPickId]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_PurgeLatestAllPickId]
	@NumRowsChanged int OUTPUT, 
	@ErrorCode int OUTPUT
AS
BEGIN
  DELETE p1
  FROM [PickTest].[dbo].[PickID2] as p1 
  WHERE [Version] = (SELECT MAX([Version])
  FROM [PickTest].[dbo].[PickID2] p2
  WHERE p1.FK_ID = p2.FK_ID AND p1.FK_Project_Code = p2.FK_Project_Code);
  
  SELECT @NumRowsChanged = @@ROWCOUNT, @ErrorCode = @@ERROR
END
GO
/****** Object:  Table [dbo].[project]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[project](
	[Code] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[MemoryType] [varchar](255) NOT NULL,
	[SectorSize] [int] NOT NULL,
	[Manufacturer_Code] [char](1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_project] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[product]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[product](
	[URC] [int] NULL,
	[Revision] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[Chip_Code] [int] NOT NULL,
	[ReplicationID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[ReplicationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [IX_product] UNIQUE NONCLUSTERED 
(
	[URC] ASC,
	[Revision] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'product', @level2type=N'COLUMN',@level2name=N'Revision'
GO
/****** Object:  Table [dbo].[PickPrefix2]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickPrefix2](
	[Data] [varchar](50) NOT NULL,
	[Order] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Comment] [text] NULL,
	[PK_PickPrefix] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FK_PickID] [int] NOT NULL,
 CONSTRAINT [PK_PickPrefix2_1] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickPrefix] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickPrefix]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickPrefix](
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Data] [varchar](255) NULL,
	[Comment] [text] NULL,
	[Order] [int] NULL,
	[PickRID] [int] NOT NULL,
 CONSTRAINT [PK_PickPrefix] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMapPDLIntron]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMapPDLIntron](
	[IntronRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Intron] [varchar](255) NULL,
	[pickDataMap_RID] [int] NOT NULL,
	[Order] [int] NULL,
 CONSTRAINT [PK_PickDataMapPDLIntron] PRIMARY KEY NONCLUSTERED 
(
	[IntronRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PickDataMapLabel]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PickDataMapLabel](
	[LabelRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Label] [varchar](255) NULL,
	[PickDataMap_RID] [int] NOT NULL,
	[Order] [int] NULL,
 CONSTRAINT [PK_PickDataMapLabel] PRIMARY KEY NONCLUSTERED 
(
	[LabelRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[idLoad]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[idLoad](
	[Project_Code] [int] NOT NULL,
	[ID] [char](5) NOT NULL,
	[DoUpdate] [char](1) NOT NULL,
	[FromID] [char](5) NOT NULL,
 CONSTRAINT [PK_idLoad] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[goldenImage]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[goldenImage](
	[Project_Code] [int] NOT NULL,
	[Image] [image] NOT NULL,
	[RID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Date] [datetime] NULL,
	[Description] [varchar](255) NULL,
	[Attribute] [varchar](63) NULL,
 CONSTRAINT [PK_goldenImage] PRIMARY KEY NONCLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[firmware]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[firmware](
	[Project_Code] [int] NOT NULL,
	[MaxExecutorSize] [int] NOT NULL,
	[IsCheckSum] [char](1) NOT NULL,
	[IsTwoByteExecutorCode] [char](1) NOT NULL,
	[IncludePath] [varchar](255) NOT NULL,
 CONSTRAINT [PK_firmware_1] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[executorLoad]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[executorLoad](
	[Project_Code] [int] NOT NULL,
	[Executor_Code] [int] NOT NULL,
	[DoUpdate] [char](1) NOT NULL,
 CONSTRAINT [PK_ExecutorLoad] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Executor_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[digitGroup]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[digitGroup](
	[Project_Code] [int] NOT NULL,
	[Code] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Data] [varchar](255) NOT NULL,
 CONSTRAINT [PK_DigitGroup] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Code] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AlternateCode]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AlternateCode](
	[Project_Code] [int] NOT NULL,
	[ID] [char](5) NOT NULL,
	[ExternalID] [char](5) NOT NULL,
	[NumCriticalOutron] [int] NOT NULL,
	[MatchCount] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[address]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address](
	[Project_Code] [int] NOT NULL,
	[Version] [varchar](255) NOT NULL,
	[Type] [int] NOT NULL,
	[VersionAddress] [varchar](255) NOT NULL,
	[BeginAddress] [varchar](255) NOT NULL,
	[EndAddress] [varchar](255) NOT NULL,
 CONSTRAINT [PK_firmware] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Version] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Library ''L'', Software ''S''' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'address', @level2type=N'COLUMN',@level2name=N'Type'
GO
/****** Object:  Table [dbo].[config]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[config](
	[Project_Code] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Value] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[key]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[key](
	[RID] [int] NOT NULL,
	[Project_Code] [int] NOT NULL,
	[Position] [int] NULL,
	[Label] [varchar](255) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Scan_Code] [int] NULL,
 CONSTRAINT [PK_key] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OriginalIDTNCaptureList]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OriginalIDTNCaptureList](
	[CaptureRID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Label] [varchar](255) NULL,
	[TN] [int] NULL,
	[Filename] [varchar](255) NULL,
	[LegacyLabel] [varchar](255) NULL,
	[FunctionRID] [int] NOT NULL,
 CONSTRAINT [PK_OriginalIDTNCaptureList] PRIMARY KEY NONCLUSTERED 
(
	[CaptureRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertPickPrefix]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickPrefix]
	@FK_PickID int, 
	@Data varchar(50),
	@Comment text,
	@Order int,
	@InsertedRID int OUTPUT
AS
BEGIN
    INSERT INTO PickPrefix2
    (FK_PickID, Data, Comment, [Order])
    VALUES (@FK_PickID, @Data, @Comment, @Order);
    SELECT @InsertedRID = @@IDENTITY;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertPickDataMap]    Script Date: 10/21/2010 18:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jlim
-- Create date: 8/4/2010
-- Description:	insert id snapshot with guid return
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertPickDataMap]
	@FK_PickID int, 
	@Data varchar(50),
	@IDLabel nvarchar(255),
	@FirmwareLabel nvarchar(50),
	@Comment text,
	@Intron varchar(255),
	@IntronPriority int,
	@KeyNumber int,
	@InsertedRID int OUTPUT
AS
BEGIN
    INSERT INTO PickDataMap2
    (FK_PickID, Data, IDLabel, FirmwareLabel, Comment, Intron, IntronPriority, KeyNumber)
    VALUES (@FK_PickID, @Data, @IDLabel, @FirmwareLabel, @Comment, @Intron, @IntronPriority, @KeyNumber);
    SELECT @InsertedRID = @@IDENTITY;
END
GO
/****** Object:  Table [dbo].[Report]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report](
	[PK_Report] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FK_Project_Code] [int] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Report] [text] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[FK_ReportType] [int] NOT NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[PK_Report] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PickConfig]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PickConfig](
	[FK_ProjectCode] [int] NOT NULL,
	[ConfigFile] [xml] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[PK_PickConfig] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 CONSTRAINT [PK_PickConfig] PRIMARY KEY NONCLUSTERED 
(
	[PK_PickConfig] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[project_chip]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[project_chip](
	[Project_Code] [int] NOT NULL,
	[Chip_Code] [int] NOT NULL,
 CONSTRAINT [PK_project_chip] PRIMARY KEY CLUSTERED 
(
	[Project_Code] ASC,
	[Chip_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[keyGroup]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[keyGroup](
	[Device_RID] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Key_RID] [int] NOT NULL,
 CONSTRAINT [PK_keyGroup] PRIMARY KEY CLUSTERED 
(
	[Device_RID] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[keyProperty]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[keyProperty](
	[Type_Code] [int] NOT NULL,
	[Key_RID] [int] NOT NULL,
 CONSTRAINT [PK_keyProperty] PRIMARY KEY CLUSTERED 
(
	[Type_Code] ASC,
	[Key_RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[keyOutron]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keyOutron](
	[RID] [int] NOT NULL,
	[Key_RID] [int] NULL,
	[Mode] [char](1) NOT NULL,
	[Outron] [char](3) NOT NULL,
	[Group] [int] NULL,
 CONSTRAINT [PK_outronGroup] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[device]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[device](
	[Key_RID] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[ModeList] [varchar](255) NOT NULL,
	[DefaultID] [char](5) NOT NULL,
 CONSTRAINT [PK_deviceDescription] PRIMARY KEY CLUSTERED 
(
	[Key_RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[chipNumber]    Script Date: 10/21/2010 18:55:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[chipNumber]
AS
SELECT     dbo.project_chip.Project_Code, dbo.project.Name AS ProjectName, dbo.chip.Number
FROM         dbo.project_chip INNER JOIN
                      dbo.chip ON dbo.project_chip.Chip_Code = dbo.chip.Code INNER JOIN
                      dbo.project ON dbo.project_chip.Project_Code = dbo.project.Code
GO
/****** Object:  View [dbo].[deviceInfo]    Script Date: 10/21/2010 18:55:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[deviceInfo]
AS
SELECT     dbo.project.Name AS ProjectName, d.Key_RID AS DeviceKey_RID, d.[Order] AS DeviceNumber, d.ModeList, d.DefaultID, k.RID AS Key_RID, 
                      k.[Position] AS Key_Position, k.Label AS Key_Label, k.Description AS Key_Description, kg.[Order] AS Key_Order, k.Scan_Code AS Key_Scan_Code, 
                      dbo.keyProperty.Type_Code AS Key_Type_Code, dbo.keyType.Name AS Key_Type_Name
FROM         dbo.[key] k INNER JOIN
                      dbo.keyGroup kg ON k.RID = kg.Key_RID INNER JOIN
                      dbo.device d ON kg.Device_RID = d.Key_RID INNER JOIN
                      dbo.project ON k.Project_Code = dbo.project.Code LEFT OUTER JOIN
                      dbo.keyProperty ON k.RID = dbo.keyProperty.Key_RID LEFT OUTER JOIN
                      dbo.keyType ON dbo.keyProperty.Type_Code = dbo.keyType.Code
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1 [56] 4 [18] 2))"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "k"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "kg"
            Begin Extent = 
               Top = 6
               Left = 228
               Bottom = 106
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 108
               Left = 228
               Bottom = 223
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "project"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyProperty"
            Begin Extent = 
               Top = 6
               Left = 418
               Bottom = 91
               Right = 570
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyType"
            Begin Extent = 
               Top = 6
               Left = 608
               Bottom = 91
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      RowHeights = 220
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'deviceInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'deviceInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'deviceInfo'
GO
/****** Object:  View [dbo].[keyInfo]    Script Date: 10/21/2010 18:55:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[keyInfo]
AS
SELECT     dbo.[key].RID, dbo.project.Name AS ProjectName, dbo.[key].[Position], dbo.[key].Label, dbo.[key].Description, dbo.device.[Order] AS DeviceNumber, 
                      dbo.device.ModeList, dbo.device.DefaultID, dbo.[key].Scan_Code, dbo.keyProperty.Type_Code, dbo.keyType.Name AS Type_Name
FROM         dbo.project INNER JOIN
                      dbo.[key] ON dbo.project.Code = dbo.[key].Project_Code LEFT OUTER JOIN
                      dbo.device ON dbo.[key].RID = dbo.device.Key_RID LEFT OUTER JOIN
                      dbo.keyProperty ON dbo.[key].RID = dbo.keyProperty.Key_RID LEFT OUTER JOIN
                      dbo.keyType ON dbo.keyProperty.Type_Code = dbo.keyType.Code
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1 [56] 4 [18] 2))"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "project"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "key"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 121
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "device"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyProperty"
            Begin Extent = 
               Top = 6
               Left = 443
               Bottom = 91
               Right = 595
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "keyType"
            Begin Extent = 
               Top = 6
               Left = 633
               Bottom = 91
               Right = 785
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      RowHeights = 220
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'keyInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'keyInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'keyInfo'
GO
/****** Object:  Table [dbo].[outronIntron]    Script Date: 10/21/2010 18:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[outronIntron](
	[KeyOutron_RID] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[Intron] [varchar](255) NOT NULL,
 CONSTRAINT [PK_outronToIntron] PRIMARY KEY CLUSTERED 
(
	[KeyOutron_RID] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[outronInfo]    Script Date: 10/21/2010 18:55:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[outronInfo]
AS
SELECT     DISTINCT di.ProjectName, ko.Mode, ko.Outron, di.Key_Position as Position, di.Key_Description AS Description, ko.[Group], di.Key_Order AS [Order], ko.RID, ko.Key_RID
FROM         dbo.deviceInfo di INNER JOIN
                      dbo.keyOutron ko ON di.Key_RID = ko.Key_RID
WHERE     (di.ModeList LIKE '%' + ko.Mode + '%')
GO
/****** Object:  View [dbo].[intronInfo]    Script Date: 10/21/2010 18:55:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[intronInfo]
AS
SELECT     di.ProjectName, ko.Mode, ko.Outron, di.Key_Order AS OutronOrder, oi.Intron, oi.[Order] AS IntronOrder, ko.Key_RID, oi.KeyOutron_RID
FROM         dbo.deviceInfo di INNER JOIN
                      dbo.keyOutron ko ON di.Key_RID = ko.Key_RID INNER JOIN
                      dbo.outronIntron oi ON ko.RID = oi.KeyOutron_RID
WHERE     (di.ModeList LIKE '%' + ko.Mode + '%')
GO
/****** Object:  Default [DF_deviceInfo_Order]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[device] ADD  CONSTRAINT [DF_deviceInfo_Order]  DEFAULT ((-1)) FOR [Order]
GO
/****** Object:  Default [DF_deviceDescription_DefaultID]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[device] ADD  CONSTRAINT [DF_deviceDescription_DefaultID]  DEFAULT ('') FOR [DefaultID]
GO
/****** Object:  Default [DF_executorLoad_DoUpdate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[executorLoad] ADD  CONSTRAINT [DF_executorLoad_DoUpdate]  DEFAULT ('N') FOR [DoUpdate]
GO
/****** Object:  Default [DF_idLoad_DoUpdate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[idLoad] ADD  CONSTRAINT [DF_idLoad_DoUpdate]  DEFAULT ('N') FOR [DoUpdate]
GO
/****** Object:  Default [DF_OriginalIDSnapshot_CreationDate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[OriginalIDSnapshot] ADD  CONSTRAINT [DF_OriginalIDSnapshot_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [DF_PickConfig_CreationDate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickConfig] ADD  CONSTRAINT [DF_PickConfig_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [DF_PickDataMap2_CreationDate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickDataMap2] ADD  CONSTRAINT [DF_PickDataMap2_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [DF_PickID2_CreationDate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickID2] ADD  CONSTRAINT [DF_PickID2_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [DF_PickPrefix2_CreationDate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickPrefix2] ADD  CONSTRAINT [DF_PickPrefix2_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  Default [DF_product_IncludePath]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[project] ADD  CONSTRAINT [DF_product_IncludePath]  DEFAULT ('') FOR [Description]
GO
/****** Object:  Default [DF_Report_CreationDate]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[Report] ADD  CONSTRAINT [DF_Report_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
/****** Object:  ForeignKey [FK_address_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[address]  WITH CHECK ADD  CONSTRAINT [FK_address_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[address] CHECK CONSTRAINT [FK_address_project]
GO
/****** Object:  ForeignKey [FK_AlternateCode_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[AlternateCode]  WITH CHECK ADD  CONSTRAINT [FK_AlternateCode_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AlternateCode] CHECK CONSTRAINT [FK_AlternateCode_project]
GO
/****** Object:  ForeignKey [FK_config_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[config]  WITH CHECK ADD  CONSTRAINT [FK_config_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[config] CHECK CONSTRAINT [FK_config_project]
GO
/****** Object:  ForeignKey [FK_deviceInfo_key]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[device]  WITH NOCHECK ADD  CONSTRAINT [FK_deviceInfo_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[device] CHECK CONSTRAINT [FK_deviceInfo_key]
GO
/****** Object:  ForeignKey [FK_digitGroup_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[digitGroup]  WITH CHECK ADD  CONSTRAINT [FK_digitGroup_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[digitGroup] CHECK CONSTRAINT [FK_digitGroup_project]
GO
/****** Object:  ForeignKey [FK_executorLoad_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[executorLoad]  WITH CHECK ADD  CONSTRAINT [FK_executorLoad_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[executorLoad] CHECK CONSTRAINT [FK_executorLoad_project]
GO
/****** Object:  ForeignKey [FK_firmware_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[firmware]  WITH CHECK ADD  CONSTRAINT [FK_firmware_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[firmware] CHECK CONSTRAINT [FK_firmware_project]
GO
/****** Object:  ForeignKey [FK_goldenImage_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[goldenImage]  WITH CHECK ADD  CONSTRAINT [FK_goldenImage_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[goldenImage] CHECK CONSTRAINT [FK_goldenImage_project]
GO
/****** Object:  ForeignKey [FK_idLoad_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[idLoad]  WITH NOCHECK ADD  CONSTRAINT [FK_idLoad_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[idLoad] CHECK CONSTRAINT [FK_idLoad_project]
GO
/****** Object:  ForeignKey [FK_key_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[key]  WITH CHECK ADD  CONSTRAINT [FK_key_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[key] CHECK CONSTRAINT [FK_key_project]
GO
/****** Object:  ForeignKey [FK_keyGroup_device]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[keyGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_keyGroup_device] FOREIGN KEY([Device_RID])
REFERENCES [dbo].[key] ([RID])
GO
ALTER TABLE [dbo].[keyGroup] CHECK CONSTRAINT [FK_keyGroup_device]
GO
/****** Object:  ForeignKey [FK_keyGroup_key]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[keyGroup]  WITH NOCHECK ADD  CONSTRAINT [FK_keyGroup_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[keyGroup] CHECK CONSTRAINT [FK_keyGroup_key]
GO
/****** Object:  ForeignKey [FK_keyOutron_key]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[keyOutron]  WITH NOCHECK ADD  CONSTRAINT [FK_keyOutron_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[keyOutron] CHECK CONSTRAINT [FK_keyOutron_key]
GO
/****** Object:  ForeignKey [FK_keyProperty_key]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[keyProperty]  WITH CHECK ADD  CONSTRAINT [FK_keyProperty_key] FOREIGN KEY([Key_RID])
REFERENCES [dbo].[key] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[keyProperty] CHECK CONSTRAINT [FK_keyProperty_key]
GO
/****** Object:  ForeignKey [FK_keyProperty_keyType]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[keyProperty]  WITH CHECK ADD  CONSTRAINT [FK_keyProperty_keyType] FOREIGN KEY([Type_Code])
REFERENCES [dbo].[keyType] ([Code])
GO
ALTER TABLE [dbo].[keyProperty] CHECK CONSTRAINT [FK_keyProperty_keyType]
GO
/****** Object:  ForeignKey [FK_OriginalIDDevice_PickID]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[OriginalIDDevice]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDDevice_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDDevice] CHECK CONSTRAINT [FK_OriginalIDDevice_PickID]
GO
/****** Object:  ForeignKey [FK_OriginalIDFunction_PickID]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[OriginalIDFunction]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDFunction_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDFunction] CHECK CONSTRAINT [FK_OriginalIDFunction_PickID]
GO
/****** Object:  ForeignKey [FK_OriginalIDLog_PickID]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[OriginalIDLog]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDLog_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDLog] CHECK CONSTRAINT [FK_OriginalIDLog_PickID]
GO
/****** Object:  ForeignKey [FK_OriginalIDTNCaptureList_OriginalIDFunction]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[OriginalIDTNCaptureList]  WITH NOCHECK ADD  CONSTRAINT [FK_OriginalIDTNCaptureList_OriginalIDFunction] FOREIGN KEY([FunctionRID])
REFERENCES [dbo].[OriginalIDFunction] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OriginalIDTNCaptureList] CHECK CONSTRAINT [FK_OriginalIDTNCaptureList_OriginalIDFunction]
GO
/****** Object:  ForeignKey [FK_OutronToIntron_outronGroup]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[outronIntron]  WITH NOCHECK ADD  CONSTRAINT [FK_OutronToIntron_outronGroup] FOREIGN KEY([KeyOutron_RID])
REFERENCES [dbo].[keyOutron] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[outronIntron] CHECK CONSTRAINT [FK_OutronToIntron_outronGroup]
GO
/****** Object:  ForeignKey [FK_PickConfig_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickConfig]  WITH CHECK ADD  CONSTRAINT [FK_PickConfig_project] FOREIGN KEY([FK_ProjectCode])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickConfig] CHECK CONSTRAINT [FK_PickConfig_project]
GO
/****** Object:  ForeignKey [FK_PickDataMap_PickID]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickDataMap]  WITH NOCHECK ADD  CONSTRAINT [FK_PickDataMap_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMap] CHECK CONSTRAINT [FK_PickDataMap_PickID]
GO
/****** Object:  ForeignKey [FK_PickDataMap2_PickID2]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickDataMap2]  WITH CHECK ADD  CONSTRAINT [FK_PickDataMap2_PickID2] FOREIGN KEY([FK_PickID])
REFERENCES [dbo].[PickID2] ([PK_PickID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMap2] CHECK CONSTRAINT [FK_PickDataMap2_PickID2]
GO
/****** Object:  ForeignKey [FK_PickDataMapLabel_PickDataMap]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickDataMapLabel]  WITH NOCHECK ADD  CONSTRAINT [FK_PickDataMapLabel_PickDataMap] FOREIGN KEY([PickDataMap_RID])
REFERENCES [dbo].[PickDataMap] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMapLabel] CHECK CONSTRAINT [FK_PickDataMapLabel_PickDataMap]
GO
/****** Object:  ForeignKey [FK_PickDataMapPDLIntron_PickDataMap]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickDataMapPDLIntron]  WITH NOCHECK ADD  CONSTRAINT [FK_PickDataMapPDLIntron_PickDataMap] FOREIGN KEY([pickDataMap_RID])
REFERENCES [dbo].[PickDataMap] ([RID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickDataMapPDLIntron] CHECK CONSTRAINT [FK_PickDataMapPDLIntron_PickDataMap]
GO
/****** Object:  ForeignKey [FK_PickPrefix_PickID]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickPrefix]  WITH NOCHECK ADD  CONSTRAINT [FK_PickPrefix_PickID] FOREIGN KEY([PickRID])
REFERENCES [dbo].[PickID] ([PickRID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickPrefix] CHECK CONSTRAINT [FK_PickPrefix_PickID]
GO
/****** Object:  ForeignKey [FK_PickPrefix2_PickI?D2]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[PickPrefix2]  WITH CHECK ADD  CONSTRAINT [FK_PickPrefix2_PickID2] FOREIGN KEY([FK_PickID])
REFERENCES [dbo].[PickID2] ([PK_PickID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PickPrefix2] CHECK CONSTRAINT [FK_PickPrefix2_PickID2]
GO
/****** Object:  ForeignKey [FK_product_chip]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[product]  WITH NOCHECK ADD  CONSTRAINT [FK_product_chip] FOREIGN KEY([Chip_Code])
REFERENCES [dbo].[chip] ([Code])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_chip]
GO
/****** Object:  ForeignKey [FK_project_manufacturer]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[project]  WITH NOCHECK ADD  CONSTRAINT [FK_project_manufacturer] FOREIGN KEY([Manufacturer_Code])
REFERENCES [dbo].[manufacturer] ([Code])
GO
ALTER TABLE [dbo].[project] CHECK CONSTRAINT [FK_project_manufacturer]
GO
/****** Object:  ForeignKey [FK_project_chip_chip]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[project_chip]  WITH CHECK ADD  CONSTRAINT [FK_project_chip_chip] FOREIGN KEY([Chip_Code])
REFERENCES [dbo].[chip] ([Code])
GO
ALTER TABLE [dbo].[project_chip] CHECK CONSTRAINT [FK_project_chip_chip]
GO
/****** Object:  ForeignKey [FK_project_chip_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[project_chip]  WITH CHECK ADD  CONSTRAINT [FK_project_chip_project] FOREIGN KEY([Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[project_chip] CHECK CONSTRAINT [FK_project_chip_project]
GO
/****** Object:  ForeignKey [FK_Report_project]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_project] FOREIGN KEY([FK_Project_Code])
REFERENCES [dbo].[project] ([Code])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_project]
GO
/****** Object:  ForeignKey [FK_Report_ReportType]    Script Date: 10/21/2010 18:55:54 ******/
ALTER TABLE [dbo].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Report_ReportType] FOREIGN KEY([FK_ReportType])
REFERENCES [dbo].[ReportType] ([PK_ReportType])
GO
ALTER TABLE [dbo].[Report] CHECK CONSTRAINT [FK_Report_ReportType]
GO
