USE [WorkingProduct2]
GO
/****** Object:  Role [MSmerge_39812635D3E846FD83D6B806C4D86FDA]    Script Date: 12/09/2010 15:32:10 ******/
CREATE ROLE [MSmerge_39812635D3E846FD83D6B806C4D86FDA] AUTHORIZATION [dbo]
GO
/****** Object:  Role [MSmerge_PAL_role]    Script Date: 12/09/2010 15:32:10 ******/
CREATE ROLE [MSmerge_PAL_role] AUTHORIZATION [dbo]
GO
/****** Object:  User [amunoz]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [amunoz] FOR LOGIN [UEIC\amunoz] WITH DEFAULT_SCHEMA=[amunoz]
GO
/****** Object:  User [APAC\bmuralidharkini]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [APAC\bmuralidharkini] FOR LOGIN [APAC\bmuralidharkini] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [APAC\gmonica]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [APAC\gmonica] FOR LOGIN [APAC\gmonica] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [APAC\mpchandrika]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [APAC\mpchandrika] FOR LOGIN [APAC\mpchandrika] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [bcampbell]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [bcampbell] FOR LOGIN [UEIC\bcampbell] WITH DEFAULT_SCHEMA=[bcampbell]
GO
/****** Object:  User [BSinohui]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [BSinohui] FOR LOGIN [UEIC\bsinohui] WITH DEFAULT_SCHEMA=[BSinohui]
GO
/****** Object:  User [hplasman]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [hplasman] FOR LOGIN [UEIC\hplasman] WITH DEFAULT_SCHEMA=[hplasman]
GO
/****** Object:  User [IV&V]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [IV&V] FOR LOGIN [UEIC\IV&V]
GO
/****** Object:  User [JLim]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [JLim] FOR LOGIN [UEIC\jlim] WITH DEFAULT_SCHEMA=[JLim]
GO
/****** Object:  User [jpan]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [jpan] FOR LOGIN [UEIC\jpan] WITH DEFAULT_SCHEMA=[jpan]
GO
/****** Object:  User [knguyen]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [knguyen] FOR LOGIN [UEIC\knguyen] WITH DEFAULT_SCHEMA=[knguyen]
GO
/****** Object:  User [NETWORK SERVICE]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [NETWORK SERVICE] FOR LOGIN [NT AUTHORITY\NETWORK SERVICE]
GO
/****** Object:  User [sadulratananu]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [sadulratananu] FOR LOGIN [UEIC\sadulratananu] WITH DEFAULT_SCHEMA=[sadulratananu]
GO
/****** Object:  User [UEIC\acheng]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\acheng] FOR LOGIN [UEIC\acheng] WITH DEFAULT_SCHEMA=[UEIC\acheng]
GO
/****** Object:  User [UEIC\amuratov]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\amuratov] FOR LOGIN [UEIC\amuratov] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UEIC\ayuh]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\ayuh] FOR LOGIN [UEIC\ayuh] WITH DEFAULT_SCHEMA=[UEIC\ayuh]
GO
/****** Object:  User [UEIC\bkim]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\bkim] FOR LOGIN [UEIC\bkim] WITH DEFAULT_SCHEMA=[UEIC\bkim]
GO
/****** Object:  User [UEIC\cwalters]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\cwalters] FOR LOGIN [UEIC\cwalters] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UEIC\jetter]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\jetter] FOR LOGIN [UEIC\jetter] WITH DEFAULT_SCHEMA=[UEIC\jetter]
GO
/****** Object:  User [UEIC\jhawes]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\jhawes] FOR LOGIN [UEIC\jhawes] WITH DEFAULT_SCHEMA=[UEIC\jhawes]
GO
/****** Object:  User [UEIC\jpableo]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\jpableo] FOR LOGIN [UEIC\jpableo] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UEIC\jperez]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\jperez] FOR LOGIN [UEIC\jperez] WITH DEFAULT_SCHEMA=[UEIC\jperez]
GO
/****** Object:  User [UEIC\Library_Capture]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\Library_Capture] FOR LOGIN [UEIC\Library_Capture]
GO
/****** Object:  User [UEIC\rkim]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\rkim] FOR LOGIN [UEIC\rkim] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UEIC\shuang]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\shuang] FOR LOGIN [UEIC\shuang] WITH DEFAULT_SCHEMA=[UEIC\shuang]
GO
/****** Object:  User [UEIC\SW Enjgineering]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\SW Enjgineering] FOR LOGIN [UEIC\SW Enjgineering]
GO
/****** Object:  User [UEIC\thowell]    Script Date: 12/09/2010 15:32:09 ******/
CREATE USER [UEIC\thowell] FOR LOGIN [UEIC\thowell] WITH DEFAULT_SCHEMA=[UEIC\thowell]
GO
/****** Object:  User [UEIC\wwang]    Script Date: 12/09/2010 15:32:10 ******/
CREATE USER [UEIC\wwang] FOR LOGIN [UEIC\wwang] WITH DEFAULT_SCHEMA=[UEIC\wwang]
GO
/****** Object:  User [VHong]    Script Date: 12/09/2010 15:32:10 ******/
CREATE USER [VHong] FOR LOGIN [UEIC\VHong] WITH DEFAULT_SCHEMA=[VHong]
GO
/****** Object:  Schema [amunoz]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [amunoz] AUTHORIZATION [amunoz]
GO
/****** Object:  Schema [amuratov]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [amuratov] AUTHORIZATION [UEIC\amuratov]
GO
/****** Object:  Schema [bcampbell]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [bcampbell] AUTHORIZATION [bcampbell]
GO
/****** Object:  Schema [BSinohui]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [BSinohui] AUTHORIZATION [BSinohui]
GO
/****** Object:  Schema [hplasman]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [hplasman] AUTHORIZATION [hplasman]
GO
/****** Object:  Schema [IV&V]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [IV&V] AUTHORIZATION [IV&V]
GO
/****** Object:  Schema [JLim]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [JLim] AUTHORIZATION [JLim]
GO
/****** Object:  Schema [jpan]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [jpan] AUTHORIZATION [jpan]
GO
/****** Object:  Schema [knguyen]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [knguyen] AUTHORIZATION [knguyen]
GO
/****** Object:  Schema [MSmerge_PAL_role]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [MSmerge_PAL_role] AUTHORIZATION [MSmerge_PAL_role]
GO
/****** Object:  Schema [NETWORK SERVICE]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [NETWORK SERVICE] AUTHORIZATION [NETWORK SERVICE]
GO
/****** Object:  Schema [sadulratananu]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [sadulratananu] AUTHORIZATION [sadulratananu]
GO
/****** Object:  Schema [UEIC\acheng]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\acheng] AUTHORIZATION [UEIC\acheng]
GO
/****** Object:  Schema [UEIC\ayuh]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\ayuh] AUTHORIZATION [UEIC\ayuh]
GO
/****** Object:  Schema [UEIC\bkim]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\bkim] AUTHORIZATION [UEIC\bkim]
GO
/****** Object:  Schema [UEIC\jetter]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\jetter] AUTHORIZATION [UEIC\jetter]
GO
/****** Object:  Schema [UEIC\jhawes]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\jhawes] AUTHORIZATION [UEIC\jhawes]
GO
/****** Object:  Schema [UEIC\jperez]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\jperez] AUTHORIZATION [UEIC\jperez]
GO
/****** Object:  Schema [UEIC\Library_Capture]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\Library_Capture] AUTHORIZATION [UEIC\Library_Capture]
GO
/****** Object:  Schema [UEIC\shuang]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\shuang] AUTHORIZATION [UEIC\shuang]
GO
/****** Object:  Schema [UEIC\SW Enjgineering]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\SW Enjgineering] AUTHORIZATION [UEIC\SW Enjgineering]
GO
/****** Object:  Schema [UEIC\thowell]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\thowell] AUTHORIZATION [UEIC\thowell]
GO
/****** Object:  Schema [UEIC\wwang]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [UEIC\wwang] AUTHORIZATION [UEIC\wwang]
GO
/****** Object:  Schema [VHong]    Script Date: 12/09/2010 15:32:09 ******/
CREATE SCHEMA [VHong] AUTHORIZATION [VHong]
GO
