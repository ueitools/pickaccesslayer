using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using BusinessObject;
using PickData;

namespace PickAccessLayer
{
    static class PALUtil
    {
        public static char ConvertToYesNoChar(bool b)
        {
            return ConvertToYesNo(b)[0];
        }
        public static string ConvertToYesNo(bool b)
        {
            return (b ? BooleanFlag.YES : BooleanFlag.NO);
        }
        public static string ConvertNullToDefault(string s, string onNull)
        {
            return (s == null ? onNull : s);
        }
        internal static string ToXml(PickId pickId)
        {
            StringWriter stringWriter = new StringWriter();
            XmlSerializer x = new XmlSerializer(pickId.GetType());
            x.Serialize(stringWriter, pickId);
            stringWriter.Close();

            return stringWriter.ToString();
        }

        internal static string ToXml(IProject project)
        {
            StringWriter stringWriter = new StringWriter();
            XmlSerializer x = new XmlSerializer(project.GetType());
            x.Serialize(stringWriter, project);
            stringWriter.Close();

            return stringWriter.ToString();
        }

        internal static PickData.IProject FromProjectXml(string xml)
        {
            PickData.Project project = new PickData.Project();
            MemoryStream memoryStream = new MemoryStream(Encoding.Default.GetBytes(xml));
            project.FromXml(memoryStream, ref project);

            return project;
        }

        internal static PickId FromPickIdXml(string xml)
        {
            xml = xml.Replace("&#x13;", "");

            StringReader stringReader = new StringReader(xml);
            PickId pickId = new PickData.PickId();
            XmlSerializer x = new XmlSerializer(pickId.GetType());
            pickId = x.Deserialize(stringReader) as PickId;
            stringReader.Close();

            return pickId;
        }
    }
    /// <summary>
    /// maybe in the future this will be changed if pick database gets separated to its own file
    /// perhaps in that case this would actually do nothing
    /// </summary>
    internal class ConnectionSwitcher : IDisposable
    {
        private string _lastConn;

        public ConnectionSwitcher()
        {
            _lastConn = DAOFactory.GetDBConnectionString();
            if (_lastConn != DBConnectionString.PRODUCT &&
                _lastConn != DBConnectionString.WORKPRODUCT &&
                _lastConn != DBConnectionString.LOCALPRODUCT)
                DAOFactory.ResetDBConnection(DBConnectionString.WORKPRODUCT);
        }
        public void Dispose()
        {
            DAOFactory.ResetDBConnection(_lastConn);
        }
    }

    internal class HashtableWithCheck
    {
        Hashtable _hashTable;

        public HashtableWithCheck(Hashtable hashTable)
        {
            _hashTable = hashTable;
        }

        public object this[object key]
        {
            get
            {
                if (_hashTable.ContainsKey(key))
                    return _hashTable[key];
                else
                    throw new ArgumentException("hash table does not have key");
            }
            set { _hashTable[key] = value; }
        }
    }

    public class IDVersionPair
    {
        private string id;
        private int version;
        private DateTime creationDate;

        public IDVersionPair(string id, int version, DateTime date)
        {
            this.id = id;
            this.version = version;
            this.creationDate = date;
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public int Version
        {
            get { return version; }
            set { version = value; }
        }

        public DateTime CreationDate
        {
            get { return creationDate.ToLocalTime(); }
            set { creationDate = value; }
        }

        public override string ToString()
        {
            return string.Format("{0} (v.{1})", id, version);
        }
    }
    public class IdListCollection
    {
        private string _Id;

        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        private string _FromId;

        public string FromId
        {
            get { return _FromId; }
            set { _FromId = value; }
        }
    }
    public class LabelCount
    {
        private string _firmwareLabel;
        private int _total;

        public string FirmwareLabel
        {
            get { return _firmwareLabel; }
            set { _firmwareLabel = value; }
        }

        public int Total
        {
            get { return _total; }
            set { _total = value; }
        }
    }
    public class ProjectLockUserInfo
    {
        private string _username;
        private DateTime _timeStamp;
        private string _lockType;

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set { _timeStamp = value; }
        }

        public string LockType
        {
            get { return _lockType; }
            set { _lockType = value; }
        }
    }
    // Constructors needed for serialization
    // http://msdn.microsoft.com/en-us/library/ms173163.aspx
    [Serializable()]
    public class DBPickNotFoundException : Exception
    {
        public DBPickNotFoundException () : base() { }
        public DBPickNotFoundException (string message) : base(message) { }
        public DBPickNotFoundException (string message, System.Exception inner) : base(message, inner) { }
        protected DBPickNotFoundException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }
    }
}
